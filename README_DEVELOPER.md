# Updating or editing the application

This chapter helps you get a base understanding of how the application is structured and how data is moved from A to B within the application.

## Tooling

- The map system is built using [MapLibre](https://maplibre.org/)
- See the [API specification](https://maplibre.org/maplibre-gl-js/docs/) for the working of Maplibre.
- See the [Styling specification](https://maplibre.org/maplibre-style-spec/) for the data & styling rules of map layers in Maplibre.
  
We render the data through vector tiles and several API's . 
Also some layers are from external resources, like the klimaat effect atlas. 


## The App structure

A general overview of the application and the corresponding files: 

![image](public/images/viewer_documentation.png)

1. Menu Panel `/src/components/menu-panel/` containing all the possible Tabs and DataLayers from the configuration. 
2. The Map - where all the style layers/map layers are rendered `src/components/map/`
3. The map configuration - or context style layers
4. Location Search bar, based on the PDOK location server. `/src/components/menu-panel/location-nav-bar/`
5. Tab `/scr/components/menu-panel/tabs/`
6. a DataLayer `/scr/components/menu-panel/layer/` - Relevant style layers
7. the Footer `/scr/components/menu-panel/footer/`
8. the legend - showing per style layer the matching legend. `/scr/components/legend/`
9. Modal- a information panel containing details about a clicked feature or a analysis on a drawn area. `/scr/components/modal/`
10. Popup in the map. 

In the simplest way, the application exists out of `DataLayers` (input by user) and a `Map` (output). A user picks a `DataLayer` to view from the left side panel which is then rendered on top of the `Map`. Next to the map, a `Legend` is also rendered as output of the chosen `DataLayer`.

A `DataLayer` is structured under various `Tabs`, and contain titles, descriptions and other metadata. The `DataLayer` is defined in the file `src/data/data-layers.ts` and the `Tabs` are defined in the configurations files like `app.dego.json`, `app.dook.json` and `app.json`. Together these files are used to configure the data content of the applications.

We define our own syntax for the layers. Imagine it like this:

```js
Tabs
    Tab
        DataLayers [
            DataLayer {...}
            DataLayer {...}
            DataLayer {
                StyleLayers [
                    StyleLayer {}
                    StyleLayer {}
                    StyleLayer {}
                ]
            }
            DataLayer {
                sublayers [
                    DataLayer {
                        StyleLayers [
                            StyleLayer {}
                            StyleLayer {}
                            StyleLayer {}
                        ]
                    }
                    DataLayer {
                        StyleLayers [
                            StyleLayer {}
                            StyleLayer {}
                        ]
                    }
                ]
            }
        ]
```

### Relevant vs Context

The DataLayers and the visual output on the Map we call the Relevant information. The DataLayers are defined in multiple styleLayers and these are transformed into the MapLayers which can be rendered on the map. The `DataLayers` are rendered in `MapRelevantLayers`. 

Next to the relevant information we need context information. The background of the map is regarded as the context. There fore the  `map-configuration` layers are rendered in `MapContextLayers` and the layers can be found in `/src/data/context-style-layers.ts`.

This is purely a naming convention of our own. For Maplibre these are exactly the same map style layers. Technologically seen there is no difference between them. 

### DataLayer
How to define a DataLayer. The DataLayer is for the user to be seen. The DataLayer is shown to the user as a single entity. Therefore we start with defining a single  name and description. But it can consist out of multiple StyleLayers (to MapLayers) and multiple functionalities.
In `src/data/data-layers.ts` you can see the properties in `DataLayerProps`.  Here we explain what the configuration settings are. When a property end with a `?` the property is optional. Otherwise it is required. 


| property                                                          | what does it do                                                                                                                         |
| ----------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| id: string                                                        | The unique layer id - shown in the url of the application                                                                               |
| authLocked?: boolean                                              | Does the layer need authorisation or not                                                                                                |
| authRoles: AuthRole[]                                             | The Role under which the layer can be viewed once logged in.                                                                            |
| tab: TabsDook \| TabsDego \| TabsDook[] \| TabsDego[]             | The theme/tab which the `DataLayer` belongs to.                                                                                         |
| name: string                                                      | Name/title to be shown in front-end                                                                                                     |
| desc: string                                                      | The description shown in the front-end `menu-panel` > `Layer`                                                                           |
| note: string                                                      | Developer note, not shown or used anywhere                                                                                              |
| lastUpdate: string                                                | Metadata: when the layer is last edited, re-facotred or checked by te developers.                                                       |
| sourceDescription: MetaData                                       | The metadata of the data source used. Defined in a separte file (should become API)                                                     |
| layerType: '' \| 'sbi-codes'                                      | Special tag for the kvk layer                                                                                                           |
| styleLayers: Partial<StyleLayer>[]                                | The `styleLayers` that form the input for the `MapLayers` . A `DataLayer` can exist out of multiple `styleLayers`                       |
| subLayers?: Partial<DataLayerProps>[]                             | A `subLayer` is a `DataLayer` in a `DataLayer`.                                                                                         |
| drawType: 'count'                                                 | This enables the drawing functionality for aggregate statistics on the data. This needs a count API.                                    |

When the user picks a `DataLayer` they start the process of potentially loading styleLayers and their corresponding `mapLayers`


A chosen DataLayer becomes : `activeDataLayer` with a `activeDataLayerKey` throughout the application.

### StyleLayers & MapLayers

A `styleLayer` is our custom configuration and a simplification of the Maplibre styling syntax. Every `styleLayer` gets transformed into a corresponding `MapLayer`. This is done through the `src/data/make-map-style-layer.ts` file.

A `MapLayer` can be rendered on the `Map` where we configure the paint, layout updates and adding or removing them.

There can be multiple styleLayers and so multiple mapLayers rendered on the Map when a Datalayer is chosen. 

The `StyleLayer` properties can be found in `src/data/data-layers.ts`. Here we explain what the configuration settings are. When a property end with a `?` the property is optional. Otherwise it is required. 

| property                                                                       | what does it do?                                                                                                 |
| ------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------- |
| tileSource: Partial<SourceSettingsProps>                                       | The tile source to use                                                                                           |
| 'source-layer': string                                                         | The `source-layer` in the tile source when not the default defined in `tile-sources.ts`                          |
| attrName: string                                                               | The attribute name of the data to be styled (and filtered) on                                                    |
| geomType?: 'fill' \| 'line' \| 'circle' \| 'raster' \| 'symbol'                | What geometry type the styling is rendered in.                                                                                                                 |
| color?: string                                                                 | The default color when only one color is used                                                                    |
| colorScale: ColorScale                                                         | The color scale definition to use                                                                                |
| expressionType?: 'interpolate' \| 'step' \| 'match' \| 'case' | The expression type of the data distribution to color scale distribution                                         |
| extrusionAttr?: string                                                         | The extrusion attribute (height) when available                                                                  |
| hasExtrusion?: boolean                                                         | If extrusion is enabled or disabled. default : enabled                                                                                                                |
| customPaint: any                                                               | a custom paint to put on the mapLayer - overwrites what is defined in `make-map-style-layer.ts`                  |
| customFilter?: FilterSpecification                           | a custom filter to put on the mapLayer - overrides what is defined in `make-map-style-layer.ts`                 |
| caseArray: string[]                                                            | real values instead of legendStops. only for case                                                                |
| noDataValue?: number[] \| string[]                                             | No data value in the data, for example -99999 will not be shown in the visualisation.                            |
| noLegend: boolean                                                              | Do not show the Legend in the front-end                                                                          |
| offset?: number[]                                                              | Translate offset for circle placement. same as https://maplibre.org/maplibre-style-spec/layers/#circle-translate |
| opacity?: number                                                               | Layer opacity for fill layers                                                                                    |
| popup: ({ properties }: PopupProps) => Popup                                   | The popup content                                                                                                |
| detailModal: ({ properties, id }: PopupProps) => { }                           | The content of the `DetailModal`                                                                                 |
| featureRequest?: string                                                        | Only for raster sources - the GetFeatureInfoRequest                                                              |
| image?: string \| string[]                                                     | Only for raster - the wms legend image url                                                                       |
| insert?:                                                                       |                                                                                                                  |
| isRelevantLayer?: boolean                                                      |                                                                                                                  |
| isHighlightLayer?: boolean                                                     |                                                                                                                  |
| linkedStyleLayerId: string                                                     |                                                                                                                  |
| firstLabel?: string                                                            | Used for legend, last label if different from legendStops.                                                       |
| lastLabel?: string                                                             | Used for legend, last label if different from legendStops.                                                       |
| legendStops: number[] \| string[]                                              | Used for Legend, the stops used to shown in the front-end legend (partly used in the data distribution)          |
| average?: number                                                  | Used for legend - the average number                                                              |
| scale: string                                                     | The scale of the data, shown in the legend.                                                            |
| unit: string                                                      | The unit of the data, shown in `Legend` .                                                              |
| filterAttrb?: string                                              | Used for legend - which attribute to filter on                                                   |
| filterNames?: string[]                                            | Used for legend - which attribute names to show in the legend (not the properties of the real data!)  |
| filterValues?: number[] \| string[] \| [string, string[], string] | Used for legend - which attribute names to filter on of the real data                                |
| id?: string                                                                    | Is added automatically. Not to be used here                                                                                                                |
| maxzoom: number                                                                | Is added automatically from the source. Not to be configured manually here                                       |

## The Map

We use [MapLibre](https://maplibre.org/) as viewer for our maps. 

- The map system is built using [MapLibre](https://maplibre.org/)
- See the [API specification](https://maplibre.org/maplibre-gl-js/docs/) for the working of Maplibre.
- See the [Styling specification](https://maplibre.org/maplibre-style-spec/) for the data & styling rules of map layers in Maplibre.

### 1. Map initiated

In `scr/components/map/Map.tsx` the map is initiated. We add the standard background map form `/src/data/mapstyle-osm-grey-v2.json` based on our own hosted tiles made with the OpenMaptiles project. 

We add the controls, attributes, render triggers and the triggers for the Map Loader to be shown. 

### 2. Map loaded

Once this class has loaded we can start loading map sources. 
All the sources from `tile-sources.ts` are added to the map. This is either a GeoJSON, rastertile or vectortile. The Relevant and Context layers are both depending on the `tile-sources.ts` list. 

### 3. Context and Relevant Layers

When the sources are loaded the context layer and relevant layers can be rendered on top of it. 

A RelevantLayer can contain a Hover and/or Click event. This opens up the popup or the modal. 


In `MapRelevantLayers` conform Maplibre Styling syntax are :

- mapLayer
- currentLayers
- newLayers
- layer



## Changing data 

![](public/images/documentatie_data_layers.png)


## Changing and defining the styling for the style layers 

![](public/images/documentatie_style_layers.png)
