declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NEXT_PUBLIC_CONFIG: 'dook' | 'dego'

      NEXT_PUBLIC_AUTH_URL: string
      NEXT_PUBLIC_ENV: 'dev' | string
    }
  }
}

export {}
