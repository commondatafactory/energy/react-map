import { MapLayerMouseEvent } from 'maplibre-gl'

declare module 'maplibre-gl' {
  export interface MapLayerMouseEventCustom extends MapLayerMouseEvent {
    features?: GeoJSON.Feature[] &
      {
        source: string
        sourceLayer: string
      }[]
  }

  export interface MapLayerMouseEvent {
    features?: any
  }
}
