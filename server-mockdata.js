// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const cors = require('cors')

server.use(
  cors({
    origin: true,
    credentials: true,
    preflightContinue: false,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  })
)
server.options('*', cors())

server.get('/self-service/logout/browser', (req, res) => {
  res.jsonp({
    logout_url:
      'http://localhost:3080/self-service/logout?token=SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
    logout_token: 'SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
  })
})

// let isAuthorized = false

// server.get('/sessions/whoami', (req, res) => {
//   if (isAuthorized) {
//     res.jsonp({
//       id: '37a310bf-442e-4a52-a75c-eda550d71ed6',
//       active: true,
//       expires_at: '2050-06-17T07:04:01.910352Z',
//       authenticated_at: '2021-11-18T07:04:01.910352Z',
//       issued_at: '2021-11-18T07:04:01.910391Z',
//       identity: {
//         id: 'b6b0a335-c434-4bad-90fd-5b8437e38630',
//         schema_id: 'default',
//         schema_url: 'https://id.r3s.dev/schemas/default',
//         traits: {
//           name: {
//             last: 'Werdmuller',
//             first: 'Ruben',
//           },
//           email: 'rubenwerdmuller@gmail.com',
//         },
//         verifiable_addresses: [
//           {
//             id: 'c771fdf5-942b-4b65-a26b-e1e9baa19141',
//             value: 'rubenwerdmuller@gmail.com',
//             verified: true,
//             via: 'email',
//             status: 'completed',
//             verified_at: '2021-11-18T10:14:14.738388Z',
//           },
//         ],
//         recovery_addresses: [
//           {
//             id: '3efc8fc4-c514-485d-97e6-434dde32b172',
//             value: 'rubenwerdmuller@gmail.com',
//             via: 'email',
//           },
//         ],
//       },
//     })
//   } else {
//     res.status(401).jsonp({
//       error: {
//         code: 401,
//         status: 'Unauthorized',
//         request: '478c5f52640fd82faa4add449c9a3f28',
//         reason: 'No valid session cookie found.',
//         message: 'The request could not be authorized',
//       },
//     })
//   }
// })

// const getReturnUrl = (url) => {
//   let returnUrl = decodeURIComponent(url)
//   returnUrl = returnUrl.split('return_to=')
//   returnUrl.shift()
//   return returnUrl.join('')
// }

// server.get('/self-service/login/browser', (req, res) => {
//   isAuthorized = true

//   const returnUrl = getReturnUrl(req.originalUrl)
//   res.redirect(returnUrl)
// })

// server.get('/self-service/logout/browser', (req, res) => {
//   res.jsonp({
//     logout_url:
//       'http://localhost:3080/self-service/logout?token=SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
//     logout_token: 'SyyBbke21l0kWCrAugUNuLecQ9by8eDI',
//   })
// })

// server.get('/self-service/logout', (req, res) => {
//   isAuthorized = false
//   const returnUrl = getReturnUrl(req.originalUrl)
//   res.redirect(returnUrl)
// })

server.use(middlewares)

server.use(router)
server.listen(3080, () => {
  // eslint-disable-next-line no-console
  console.log('JSON Server is running on port 3080')
})
