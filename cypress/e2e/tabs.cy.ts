// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

describe('The dataLayer navigation', () => {
  beforeEach(() => {
    cy.intercept({ method: 'GET', url: /^https:\/\/files/ }, {}).as('files')
    cy.intercept({ method: 'POST', url: /^https:\/\/ds/ }, {}).as('energie')
    cy.intercept({ method: 'GET', url: /^https:\/\/acc/ }, {}).as('acceptatie')
    cy.intercept({ method: 'GET', url: '/api/**' }, {}).as('api')

    cy.visit('/')
  })

  // TABS
  it('opens a tab and shows the corresponding layers', () => {
    cy.get('[data-cy="tab-Bestemmingsplannen"]').click()
    cy.contains('Enkelbestemming').should('be.visible')
    cy.contains('Dubbelbestemming').should('be.visible')
  })

  it('opens a different tab and the navigation only shows the selectable tab layers', () => {
    cy.get('[data-cy="tab-Bestemmingsplannen"]').click()
    cy.contains('Enkelbestemming').should('be.visible')
    cy.contains('Dubbelbestemming').should('be.visible')
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Woonfunctie').should('be.visible')
    cy.contains('Bouwjaar').should('be.visible')
    cy.contains('Enkelbestemming').should('not.exist')
    cy.contains('Dubbelbestemming').should('not.exist')
  })

  // MAPLAYERS
  it('selects a layer and shows more information', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Woonfunctie').click()
    cy.contains('De Woonfunctie').should('be.visible')
  })

  it('keeps the selected layer selected when closing the tab', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Woonfunctie').click()
    cy.contains('De Woonfunctie').should('be.visible')
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('De Woonfunctie').should('exist')
  })

  it('selects another layer and only shows more information for that layer', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Woonfunctie').click()
    cy.contains('De Woonfunctie').should('be.visible')
    cy.contains('Bouwjaar').click()
    cy.contains('Het bouwjaar').should('be.visible')
    cy.contains('De woonfunctie').should('not.exist')
  })

  it('shows details to login when selecting a restricted layer', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Grote Eigenaren').click()
    cy.contains('Log in om deze kaartlaag te bekijken.').should('be.visible')
  })

  it('does not show more information when clicking on a active dataLayer', () => {
    cy.get('[data-cy="tab-Gebouwen"]').click()
    cy.contains('Woonfunctie').click()
    cy.contains('De Woonfunctie').should('be.visible')
    cy.contains('Woonfunctie').click()
    cy.contains('De Woonfunctie').should('not.exist')
  })

  // URL
  it('opens the corresponding tab and layer when navigation to a specific anchor link', () => {
    cy.visit('/?label=topo&tab=gebouw&layer=layer1')
  })
})
