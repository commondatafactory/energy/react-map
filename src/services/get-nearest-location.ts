// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export type AddressProps = {
  adresseerbaarobject_id: string
  bron: string
  buurtcode: string
  buurtnaam: string
  centroide_ll: string
  centroide_rd: string
  gekoppeld_perceel: [string]
  gemeentecode: string
  gemeentenaam: string
  huis_nlt: string
  huisnummer: number
  id: string
  identificatie: string
  nummeraanduiding_id: string
  openbareruimte_id: string
  openbareruimtetype: string
  postcode: string
  provincieafkorting: string
  provinciecode: string
  provincienaam: string
  rdf_seealso: string
  straatnaam: string
  straatnaam_verkort: string
  type: string
  waterschapscode: string
  waterschapsnaam: string
  weergavenaam: string
  wijkcode: string
  wijknaam: string
  woonplaatscode: string
  woonplaatsnaam: string
}

export const getNearestLocation = async (
  lng: number,
  lat: number,
  signal: AbortSignal
): Promise<null | AddressProps> => {
  try {
    const getLocationResponse = await fetch(
      // TODO change pdok url
      `https://geodata.nationaalgeoregister.nl/locatieserver/revgeo?lon=${lng}&lat=${lat}&rows=1&type=adres`,
      { signal }
    )

    if (!getLocationResponse.ok) {
      console.warn(`Could not fetch nearest location`)
      return null
    }

    let addressDataId = await getLocationResponse.json()
    addressDataId = addressDataId?.response.docs?.[0].id

    if (!addressDataId) {
      console.warn(`Could not fetch nearest location`)
      return null
    }

    const locationDataResponse = await fetch(
      `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?id=${addressDataId}`,
      { signal }
    )

    if (!locationDataResponse.ok) {
      console.warn(`Could not fetch location data`)
      return null
    }

    let addressData = await locationDataResponse.json()
    addressData = addressData?.response?.docs?.[0]

    if (!addressData) {
      console.warn(`Could not fetch location data`)
      return null
    }

    return addressData
  } catch (e) {
    return null
  }
}
