// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import wkt from 'wkt'
import { wktPointToGeoJson } from './wkt-to-geojson'

export const fetchLocations = async (
  searchQuery: string,
  filters = ''
): Promise<any> => {
  try {
    const response = await fetch(
      `https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?fq=*:*&bq=type:provincie%5E1&bq=type:gemeente%5E1.5&bq=type:wijk%5E1.5&bq=type:buurt%5E1.5&bq=type:woonplaats%5E1.5&bq=type:weg%5E1.5&bq=type:postcode%5E0.5 &bq=type:adres%5E1&fl=*&${filters}q=${encodeURIComponent(
        searchQuery
      )}`
    )
    return await response.json()
  } catch (e) {
    return null
  }
}

export const lookupLocation = async (item: {
  id: string | number | boolean
}): Promise<any> => {
  const response = await fetch(
    `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?fq=*:*&fl=*&id=${encodeURIComponent(
      item.id
    )}`
  )

  const validResponse = await (response.ok
    ? response
    : Promise.reject(response))

  const data = await validResponse.json()

  const geocodeResult = data.response?.docs?.[0]

  if (!geocodeResult) {
    return
  }

  geocodeResult.centroide_ll = wktPointToGeoJson(geocodeResult.centroide_ll)
  geocodeResult.geometrie_ll = wkt.parse(geocodeResult.geometrie_ll)

  return geocodeResult
}
