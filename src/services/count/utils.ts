// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Building } from '../../hooks/use-building'

// TODO: This function uses `numid`, but `useBuilding` uses `pid`. What's the
// difference?
export const filterOnNumId = <T extends { [key: string]: any }>(
  data: T[],
  verblijfsobject: Partial<Building>
): T[] =>
  data.filter(
    (bedrijf) =>
      bedrijf.numid.toString().padStart(16, '0') ===
      verblijfsobject.numid.toString().padStart(16, '0')
  )

export const mapKeysToArray = <T extends readonly string[]>(
  rows: T,
  obj: any
) =>
  rows
    .map((field) => (obj?.[field] !== undefined ? obj[field] : ''))
    .map((value) => `"${String(value).replace(/"/g, '')}"`)

export const fillArray = (data, length) =>
  data || new Array(length).fill(null).map((_) => `""`)
