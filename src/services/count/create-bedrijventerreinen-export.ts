// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Feature } from 'maplibre-gl'
import { wktPointToGeoJson } from '../map/wkt-to-geojson'
import { getKVKObjecten, KVKObjectAPI } from './services/get-kvk-objecten'

import { getVerblijfsobjecten } from './services/get-verblijfsobjecten'
import { downloadCSV } from './download-csv'
import { rechtsvormcodes } from './rechtsvormcodes'
import { filterOnNumId, mapKeysToArray } from './utils'
import { formatKvkDate } from './services/get-total'
import { downloadXLSX } from './dowload-xlsx'

const baseHeaders = [
  'numid',
  'straat',
  'huisnummer',
  'huisletter',
  'huisnummertoevoeging',
  'postcode',
  'gemeentenaam',
  'gebruiksdoelen',
  'oppervlakte',
  'pand_bouwjaar',
  'latitude',
  'longitude',
  'Indicatie_bedrijf_aanwezig_op_adres',
  'Indicatie_stichting_aanwezig_op_adres',
] as const

const kvkHeaders = [
  'KVK_Handelsnaam',
  'KVK_Nummer',
  'KVK_Bedrijfsactiviteit',
  // 'KVK_SBI_code',
  'KVK_SBI_code_hoofdactiviteit',
  'KVK_SBI_omschrijving_hoofdact',
  'KVK_SBI_code_nevenactiviteit1',
  'KVK_SBI_omschrijving_nevenactiviteit1',
  'KVK_SBI_code_nevenactiviteit2',
  'KVK_SBI_omschrijving_nevenactiviteit2',
  'KVK_Rechtsvorm',
  'KVK_Datum_inschrijving_onderneming',
  'KVK_Datum_uitschrijving_onderneming',
  'KVK_Vestigingsnummer',
  'KVK_Datum_inschrijving_vestiging',
  'KVK_Datum_uitschrijving_vestiging',
  'KVK_Indicatiehoofdvestiging',
] as const

export const createHandelsRegisterInfo = async (
  geojson: { geometry?: any },
  type: 'csv' | 'xlsx',
  filter: string
): Promise<void> => {
  // If `geojson` is null, then the 'download all' (or equivalent) button was
  // clicked, in which case we don't want to include the non KVK matched VBO
  // features. Also, if the user activated a filter, we don't want to include
  // unmatched VBO features which is similar to how the map view works.
  const includeUnmatchedVBO = !!geojson && !filter?.length

  Promise.all([
    getVerblijfsobjecten(geojson, filter),
    getKVKObjecten(geojson, false, filter),
  ]).then((data) => {
    const bagData = data[0]
    const kvkRegistrations = data[1]?.data

    const aggregatedVerblijfsobjecten = bagData.reduce(
      (acc, verblijfsobject) => {
        // Filter KVK registrations not found on selected BAG adres
        const kvkRegistrationsOnVerblijfsobject = filterOnNumId(
          kvkRegistrations,
          verblijfsobject
        )

        if (!includeUnmatchedVBO && !kvkRegistrationsOnVerblijfsobject.length) {
          return acc
        }

        // Create a list of unique KVK activities and filter out those that
        // have a duplicate dossier numbers and "Handelsnaam"
        const combinedKVKDossiersOnAdres =
          kvkRegistrationsOnVerblijfsobject.reduce((acc, obj) => {
            const key = obj.dossiernr
            acc[key] = [...(acc[key] || []), obj]
            return acc
          }, {} as Record<string, KVKObjectAPI[]>)

        const uniqueKVKdossierRegistrationsWithDescriptions =
          kvkRegistrationsOnVerblijfsobject.map((registration) => {
            const nr = combinedKVKDossiersOnAdres[registration.dossiernr]

            const hoofdactDescription = nr?.find(
              (dossier) => dossier.sbicode === dossier.hoofdact
            )

            const nevact1Description = nr?.find(
              (dossier) => dossier.sbicode === dossier.nevact1
            )

            const nevact2Description = nr?.find(
              (dossier) => dossier.sbicode === dossier.nevact2
            )

            return {
              ...registration,
              hoofdactDescription:
                hoofdactDescription?.activiteit ||
                hoofdactDescription?.l5_title,
              nevact1Description:
                nevact1Description?.activiteit || nevact1Description?.l5_title,
              nevact2Description:
                nevact2Description?.activiteit || nevact2Description?.l5_title,
            }
          })

        // Filter KVK activities that have duplicate dossier numbers. Just in
        // case the dossier number is mixed up, we also check on
        // "Handelsnaam"
        const uniqueKVKdossierRegistrations =
          uniqueKVKdossierRegistrationsWithDescriptions.filter(
            (registration, index, array) =>
              array.findIndex(
                (item) =>
                  item.dossiernr === registration.dossiernr &&
                  item.hn_1x45 === registration.hn_1x45
              ) === index
          )

        const KvkfieldsMappedToCsvHeader = uniqueKVKdossierRegistrations.map(
          (registration) => [
            registration.hn_1x45,
            registration.dossiernr,
            registration.activiteit,
            registration.hoofdact,
            registration.hoofdactDescription,
            registration.nevact1,
            registration.nevact1Description,
            registration.nevact2,
            registration.nevact2Description,
            registration.rv_fijn,
            formatKvkDate(registration.dat_inschr),
            '',
            registration.vgnummer,
            formatKvkDate(registration.dat_vest),
            '',
            '',
          ]
        )

        const stichtingen = Object.values(combinedKVKDossiersOnAdres).reduce(
          (acc, company: any) =>
            acc +
            company.reduce(
              (acc, data) => acc + (data.rv_fijn === 'Stichting' ? 1 : 0),
              0
            ),
          0
        )

        const indicatorFields = {
          Indicatie_bedrijf_aanwezig_op_adres: Object.values(
            combinedKVKDossiersOnAdres
          ).length,
          Indicatie_stichting_aanwezig_op_adres: stichtingen,
        }

        const { coordinates } = wktPointToGeoJson(verblijfsobject.point)

        const updatedVerblijfsobject = {
          ...verblijfsobject,
          latitude: coordinates[1].toFixed(2),
          longitude: coordinates[0].toFixed(2),
        }

        return [
          ...acc,
          {
            base: { ...updatedVerblijfsobject, ...indicatorFields },
            kvk: KvkfieldsMappedToCsvHeader,
          },
        ]
      },
      [] as {
        base: any
        kvk: string[][]
      }[]
    )

    const csvHeader = [
      ...baseHeaders.map((header) => `"${header}"`),
      ...kvkHeaders.map((header) => `"${header}"`),
    ].join(',')

    const csvContent = aggregatedVerblijfsobjecten
      .map((data) => {
        const baseRow = mapKeysToArray(baseHeaders, data.base)

        const kvkRows = data.kvk
          .map((row) =>
            row.map((value) =>
              value ? `"${String(value).replace(/"/g, '')}"` : ''
            )
          )
          .sort((rowA, rowB) => rowA[0].localeCompare(rowB[0]))
          .join(`\n ${baseRow.join(',')},`)
        // .join(
        //   `\n "${data.base.numid}"${',""'.repeat(baseHeaders.length - 1)}`
        // )
        return [baseRow, kvkRows].join(',')
      })
      .join('\n')

    const csv = `${csvHeader}\n${csvContent}`

    if (type === 'csv') {
      downloadCSV(csv)
    } else {
      downloadXLSX(csv, 'bedrijf')
    }
  })
}
