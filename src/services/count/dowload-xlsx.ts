// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as XLSX from 'xlsx'

export const downloadXLSX = async (csv: string, type?: string) => {
  const datum = new Date().toLocaleDateString('nl-NL', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  })

  // Creeer excel workbook
  const workbook = XLSX.utils.book_new()

  // Leest tabel met datums format
  const worksheetTabel = XLSX.read(csv, {
    type: 'string',
    cellStyles: true,
    cellDates: true,
    cellNF: true,
    dateNF: 'dd-mm-YY', //d-mmm-YY
  })
  // Voeg tabel toe
  XLSX.utils.book_append_sheet(
    workbook,
    worksheetTabel.Sheets[worksheetTabel.SheetNames[0]],
    'BasisDataset',
    true
  )

  if (type) {
    // Maak Legenda worksheet
    const data =
      type === 'auto'
        ? await require('../../fixtures/legenda_auto.json')
        : await require('../../fixtures/legenda_bedrijven.json')

    const worksheetLegenda = XLSX.utils.json_to_sheet(data)
    XLSX.utils.sheet_add_aoa(
      worksheetLegenda,
      [['Attribuut', 'Beschrijving']],
      {
        origin: 'A1',
      }
    )
    XLSX.utils.book_append_sheet(workbook, worksheetLegenda, 'Legenda')
  }

  // Write file
  XLSX.writeFileXLSX(workbook, `SelectieDownload${datum}.xlsx`, {
    cellStyles: true,
    compression: true,
    cellDates: true,
  })
}

// {
//   "w": "15", formatted text
//   "z": "15", number format string associated with the cell
//   "t": "d",  type d = Date
//   "v": "2010-01-04T00:00:00.000Z" raw value
// }
