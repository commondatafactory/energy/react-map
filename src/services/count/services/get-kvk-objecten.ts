// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export interface KVKObjectAPI {
  activiteit: string
  buurtcode: string
  buurtnaam: string
  dat_inschr: string
  dat_oprich: string
  dat_vest: string
  dossiernr: string
  gemeentecode: string
  gemeentenaam: string
  geopunt: string
  gestortkap: string
  hn_1x45: string
  hoofdact: string
  huisletter: string
  huisnr: string
  huisnummer: string
  huisnummertoevoeging: string
  id: string
  l1_code: string
  l1_title: string
  l2_code: string
  l2_title: string
  l3_code: string
  l3_title: string
  l4_code: string
  l4_title: string
  l5_code: string
  l5_title: string
  lid: string
  match_score: string
  nevact1: string
  nevact2: string
  nm_1x45: string
  numid: string
  oppervlakte: string
  pid: string
  postcode: string
  provinciecode: string
  provincienaam: string
  red_inschr: string
  red_opheff: string
  red_uitsch: string
  rv_fijn: string
  sbicode: string
  sid: string
  stat_insch: string
  subdossier: string
  toev_hsnr: string
  url: string
  vennaam: string
  vgnummer: string
  vid: string
  dat_opheff: string // NOTE zit er nu niet in, voor als die er later bij komt!
}

export const getKVKObjecten = async (
  geojson: {
    geometry?: any
  },
  reduce = false,
  filter: string
): Promise<{ totaal: number; data: KVKObjectAPI[] }> => {
  const formData = new URLSearchParams()

  if (geojson) {
    const sgj = JSON.stringify(geojson.geometry)
    formData.append('geojson', sgj)
  }

  const kvkDataselectionUrl =
    process.env.NEXT_PUBLIC_ENV === 'prod'
      ? 'https://authorized.data.vng.nl/kvkds'
      : process.env.NEXT_PUBLIC_ENV === 'acc'
      ? 'https://acc.authorized.data.commondatafactory.nl/kvkds'
      : 'http://authorized.data.vng.com/kvkds'

  try {
    const response = await fetch(
      `${kvkDataselectionUrl}/list/?${filter || ''}`,
      {
        method: 'POST',
        body: formData,
        credentials: 'include',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )

    const data = await response.json()
    let totaal

    if (filter.includes('reduce=vestigingen')) {
      totaal = data.vestigingen
    } else if (filter.includes('reduce=count')) {
      totaal = data.count
    } else if (reduce) {
      totaal = Object.keys(data).reduce(
        (acc, value) => acc + Number(data[value].count),
        0
      )
    } else {
      totaal = Number(response.headers.get('total-items'))
    }
    return { totaal, data: data || [] }
  } catch (e) {
    console.error('Download error with KVK objecten')
    return { totaal: 0, data: [] }
  }
}
