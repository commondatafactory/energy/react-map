// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { getRdwErkenning, RDWErkenningAPI } from './get-rdw-erkenning'

export interface RDWObjectAPI {
  id: string
  vid: string
  pid: string
  sid: string
  lid: string
  numid: string
  postcode: string
  huisnummer: string
  huisletter: string
  huisnummertoevoeging: string
  rdw_toevoeging: string
  volgnummer: string
  gevelnaam: string
  naam_bedrijf: string
  straat: string
  plaats: string
  oppervlakte: string
  geometry: string
  match_score: string
}

export const getRDWobjecten = async (
  geojson: {
    geometry?: any
  },
  limit = false
): Promise<{
  totaal: number
  data: (RDWObjectAPI & { rdw_erkenningen: string[] })[]
}> => {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  try {
    const parameters = limit ? `reduce=count` : ''

    const response = await fetch(
      `https://ds.rdw.commondatafactory.nl/list/?${parameters}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const totaal = Number(response.headers.get('total-items'))

    if (limit) {
      return { totaal, data: [] }
    }

    const data = await response.json()

    const mappedData = await Promise.all(
      data.map(async (data) => {
        const e = await getRdwErkenning(Number(data.volgnummer))
        return { ...data, rdw_erkenningen: e.mappedErkenningen }
      })
    )
    return { totaal, data: mappedData }
  } catch (e) {
    console.error('Download error with RDW objecten')
    return { totaal: 0, data: [] }
  }
}
