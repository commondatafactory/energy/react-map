// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

export const getTotals = async function (
  geojson: { geometry?: any },
  layer: string
): Promise<any> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)

  try {
    const response = await fetch(
      `https://ds.vboenergie.commondatafactory.nl/list/?reduce=${layer}`,
      {
        method: 'POST',
        body: formData,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded',
        },
      }
    )
    const totaal = Number(response.headers.get('total-items'))
    const data = await response.json()
    return { totaal, data }
  } catch (e) {
    console.warn(
      'Dit getekend gebied is niet in orde, leeg of te klein. Probeer een nieuwe gebied te tekenen.'
    )
  }
}

export const formatKvkDate = (dateString: string) => {
  if (!dateString) {
    return '-'
  }
  const year = parseInt(dateString.substring(0, 4))
  const month = parseInt(dateString.substring(4, 6)) - 1
  const day = parseInt(dateString.substring(6))
  const date = new Date(year, month, day)

  const options = { day: 'numeric', month: 'numeric', year: 'numeric' }
  const dutchDate = date.toLocaleDateString('nl-NL', options as any)

  if (dutchDate === 'Invalid Date') {
    return ''
  }

  return dutchDate
}
