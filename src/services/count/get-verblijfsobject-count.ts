// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { DownloadFileType } from '../../components/modal/count/Count'
import { downloadCSV } from './download-csv'
import { downloadXLSX } from './dowload-xlsx'

export const getVerblijfsobjectCount = async function (
  geojson: { geometry?: any },
  layer: string,
  filter: number[] | string[] | [string, string[], string] | string
): Promise<any> {
  switch (layer) {
    case 'energieklasse_score':
      layer = 'energieklasse'
      break
    case 'elabel_voorlopig':
      layer = 'labelscore_voorlopig'
      break
    case 'elabel_definitief':
      layer = 'labelscore_definitief'
      break
    case 'bouwjaar':
      layer = 'pand_bouwjaar'
      break
    default:
      break
  }

  const url = filter
    ? `https://ds.vboenergie.commondatafactory.nl/list/?groupby=${layer}&reduce=count&${filter}`
    : `https://ds.vboenergie.commondatafactory.nl/list/?groupby=${layer}&reduce=count`
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  let totaal
  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })
    totaal = response.headers.get('total-items')
    const data = await response.json()
    return { totaal, data }
  } catch (e) {}
}

export const getVerblijfsobjectDocument = async function (
  geojson: { geometry?: any },
  filter: number[] | string[] | [string, string[], string] | string,
  fileType: DownloadFileType
): Promise<void> {
  const formData = new URLSearchParams()
  const sgj = JSON.stringify(geojson.geometry)
  formData.append('geojson', sgj)
  const url = `https://ds.vboenergie.commondatafactory.nl/list/?format=csv&${filter}${
    fileType === 'xlsx' ? '&limit="100000"&pagesize=100000' : ''
  }`

  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
      },
    })

    if (fileType === 'xlsx') {
      downloadXLSX(await response.text())
    } else {
      downloadCSV(await response.text())
    }
  } catch (e) {}
}
