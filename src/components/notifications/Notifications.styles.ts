// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { Alert as ImportAlert } from '@commonground/design-system'
import styled from 'styled-components'

export const Container = styled.div`
  grid-column: 2;
  grid-row: 1;
  margin: var(--spacing05);
  display: flex;
  flex-direction: column;
  gap: var(--spacing05);
  width: 23rem;
  z-index: 2;

  @media screen and (max-width: 1250px) and (orientation: landscape) {
    width: 20rem;

    p,
    a {
      font-size: var(--fontSizeSmall);
    }
  }
`

export const Alert = styled(ImportAlert)`
  > * > .description {
    display: block;
    margin-top: var(--spacing03);
  }
`

export const Header = styled.span`
  align-items: center;
  display: flex;

  > span {
    font-weight: var(--fontWeightBold);
  }

  > small {
    margin: 0 8px 0 auto;
    font-size: var(--fontSizeSmall);
  }

  > svg {
    cursor: pointer;
  }
`

export const Buttons = styled.span`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`
