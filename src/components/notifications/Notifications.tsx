// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { Icon, Alert } from '@commonground/design-system'

import { useConfig } from '../../hooks/use-config'
import CloseIcon from '../../../public/images/close.svg'
import ExternalIcon from '../../../public/images/external-link.svg'
import { notificationsData } from '../../../notifications'
import * as Styled from './Notifications.styles'

const addMonths = (numOfMonths, date = new Date()) => {
  date.setMonth(date.getMonth() + numOfMonths)
  return date
}

const filterNotifications = (notifications, config) =>
  notifications.reduce((acc, update) => {
    // Filter when product is exclusive for DOOK/DEGO/other
    if (
      process.env.NEXT_PUBLIC_ENV !== 'dev' &&
      (update as any).product &&
      !(update as any).product.includes(config.product)
    ) {
      return acc
    }

    // Filter when update date has expired
    const currentDate = new Date()
    const updateDate = addMonths(3, new Date(update.date))

    if (currentDate.getTime() > updateDate.getTime()) {
      return acc
    }

    // Filter when set to not show again
    if (localStorage.getItem(`notification-${update.date}`)) {
      return acc
    }

    return [...acc, { ...update, index: acc.length + 1 }]
  }, [])

const validateNotifications = (notifications) =>
  notifications.map((notification, i) => {
    if (
      notifications.some(
        (update, _i) => i !== _i && update.date === notification.date
      )
    ) {
      throw new Error(
        `Notifications can't have same exact date: ${notification.date}`
      )
    }
  })

export const Notifications: React.FC = () => {
  const config = useConfig()

  const filtertedNotifications = React.useMemo(() => {
    const notifications = filterNotifications(notificationsData, config)
    validateNotifications(notifications)
    return notifications
  }, [])

  const [notifications, setNotifications] = useState(filtertedNotifications)

  const handleOnClose = (date) => {
    setNotifications(notifications.filter((update) => update.date !== date))
  }

  const handleOnCloseIndefinitely = (date) => {
    localStorage.setItem(`notification-${date}`, 'hidden')
    handleOnClose(date)
  }

  const dateOptions: Intl.DateTimeFormatOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }

  return (
    <>
      <Styled.Container>
        {notifications.slice(0, 1).map((update) => (
          <Styled.Alert key={update.date} variant="info">
            <Styled.Header>
              <span>
                {(update as any).title ||
                  `Update ${new Date(update.date).toLocaleDateString(
                    'nl-NL',
                    dateOptions
                  )}`}
              </span>
              <small>
                {update.index}/{filtertedNotifications.length}
              </small>
              <Icon as={CloseIcon} onClick={() => handleOnClose(update.date)} />
            </Styled.Header>

            <span className="description">{update.description}</span>

            <Styled.Buttons>
              <Alert.ActionButton
                as="a"
                href={update.updateUrl}
                target="_blank"
              >
                Volledige update bekijken
                <Icon
                  as={ExternalIcon}
                  onClick={() => handleOnClose(update.date)}
                  size="small"
                />
              </Alert.ActionButton>

              <Alert.ActionButton
                onClick={() => handleOnCloseIndefinitely(update.date)}
              >
                <p>Niet meer tonen</p>
              </Alert.ActionButton>
            </Styled.Buttons>
          </Styled.Alert>
        ))}
      </Styled.Container>
    </>
  )
}
