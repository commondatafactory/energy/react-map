// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { createContext } from 'react'
import type { IndexProps } from '../../pages'

export type ConfigProps = IndexProps['configuration']
const AppContext = createContext<ConfigProps>({} as ConfigProps)

export default AppContext
