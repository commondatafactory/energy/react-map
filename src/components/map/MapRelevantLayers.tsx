// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, {
  FC,
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from 'react'
import { Map, MapGeoJSONFeature, MapMouseEvent } from 'maplibre-gl'
import useUser from '../../hooks/use-user'
import { MapContext } from '../../providers/map-provider'
import { useTabsLayers } from '../../hooks/use-tabs-layers'
import { CustomLayerSpecification } from '../../data/make-map-layer'
import { useAccess } from '../../hooks/use-access'
import { StyleLayer } from '../../data/data-layers'
import { ZoomNotification } from './zoom-notification/ZoomNotification'
import {
  generateMapLayer,
  getDefaultInsertPoint,
} from './utilities/relevant-layers'
import MapHover from './MapHover'
import MapClick from './MapClick'

interface MapRelevantLayersProps {
  map: maplibregl.Map
}

export type SelectedStyleLayerSource = string
export type SelectedFeaturesInfo = {
  features: MapGeoJSONFeature[]
}

const MapRelevantLayers: FunctionComponent<MapRelevantLayersProps> = ({
  map,
}) => {
  const { data } = useUser()
  const { hasAccess } = useAccess()
  const {
    analyzeMode,
    threeDimensional,
    customFilters,
    setSelectedFeatureAndEvent,
  } = useContext(MapContext)

  const [currentMapLayers, setCurrentMapLayers] = useState<
    CustomLayerSpecification[][]
  >([])
  const [isVisible, setIsVisible] = useState(false)
  const [zoomDirection, setZoomDirection] = useState<'in' | 'uit'>('in')

  const { activeDataLayers } = useTabsLayers()
  const [selectedFeaturesInfo, setSelectedFeaturesInfo] =
    useState<SelectedFeaturesInfo>({
      features: [],
    })
  const [selectedStyleLayerSource, setSelectedStyleLayerSource] = useState('')
  const [currentEvent, setCurrentEvent] = useState<MapMouseEvent>(null)
  const [selectedRelevantStyleLayer, setSelectedRelevantStyleLayer] = useState()

  // Build Relevant map styleLayer(s)
  useEffect(() => {
    if (!map) {
      return
    }

    const newCurrentMapLayers = activeDataLayers.map((dataLayer) => {
      let relevantStyleLayer: Partial<StyleLayer>

      if (!hasAccess(activeDataLayers[0], data)) {
        return []
      }

      // TODO: There must be another way to apply styling to a sparse style
      // layer, while using the styling of a richer sibling layer. Maybe we
      // should introduce a `parent` property on style layers, so we can have
      // access to the entire collection of style layers currently active.
      // This way we can link layers by ID, or maybe deduce the relevant
      // layer.
      relevantStyleLayer =
        dataLayer?.styleLayers?.find((layer) => layer.isRelevantLayer) ||
        dataLayer?.styleLayers?.[0]

      // INFO: LegendQualitativeFilter and all SBI layers can set a custom
      // filter. This custom filter will supersede an optional filter set on
      // the style layer.
      const customFilter = customFilters?.find(
        (filter) => dataLayer.id === filter.id
      )

      return dataLayer?.styleLayers?.map((styleLayer) => {
        return generateMapLayer(
          styleLayer,
          relevantStyleLayer,
          threeDimensional,
          customFilter
        )
      })
    })

    setCurrentMapLayers(newCurrentMapLayers)
  }, [map, activeDataLayers, threeDimensional, customFilters])

  // Handle zoom
  useEffect(() => {
    if (!currentMapLayers.length || !activeDataLayers.length) {
      return
    }

    const checkZoomNotification = () => {
      if (activeDataLayers?.[0]?.id === 'layer0') {
        setIsVisible(false)
      } else {
        // TODO: Take care of custom zoom bounds in a different way...
        const currentMapLayer = currentMapLayers?.[0]?.find(
          (layer) => layer.minzoom
        )
        const currentZoom = map.getZoom()
        let minZoomCurrentMap = currentMapLayer?.minzoom
        const maxZoomCurrentMap =
          currentMapLayer?.['source-layer'] === 'zoobuurtcijfers2019'
            ? 12
            : currentMapLayer?.maxzoom

        // spuk layers different
        minZoomCurrentMap =
          activeDataLayers?.[0]?.id === 'layer51' ||
          activeDataLayers?.[0]?.id === 'layer54'
            ? 14
            : minZoomCurrentMap

        setZoomDirection(currentZoom < minZoomCurrentMap ? 'in' : 'uit')

        setIsVisible(
          currentZoom < minZoomCurrentMap ||
            currentZoom > maxZoomCurrentMap ||
            false
        )
      }
    }
    const handleZoomEnd = () => {
      checkZoomNotification()
    }

    checkZoomNotification()
    map.on('zoomend', handleZoomEnd)

    return () => {
      map.off('zoomend', handleZoomEnd)
    }
  }, [activeDataLayers, currentMapLayers])

  useEffect(() => {
    if (!analyzeMode) {
      // Disables all click actions
      setSelectedStyleLayerSource(null)
    }
  }, [analyzeMode])

  useEffect(() => {
    setSelectedFeatureAndEvent({
      event: currentEvent,
      features: selectedFeaturesInfo?.features,
      relevantStyleLayerDetailModal: selectedRelevantStyleLayer,
    })
  }, [
    selectedFeaturesInfo?.features,
    currentEvent?.lngLat,
    selectedRelevantStyleLayer,
  ])

  return (
    <>
      {currentMapLayers[0]?.map((mapLayer) => (
        <MapLayer key={mapLayer.id} map={map} mapLayer={mapLayer} />
      ))}

      <ZoomToLayer map={map} />

      {isVisible && (
        <ZoomNotification isVisible={isVisible} zoomDirection={zoomDirection} />
      )}

      {analyzeMode !== 'count' &&
        currentMapLayers[0]?.map((layer, i) => (
          <React.Fragment key={'MapActions' + layer.id + i}>
            {((layer.type !== 'symbol' &&
              activeDataLayers[0]?.styleLayers[i]?.popup) ||
              (layer.type === 'raster' &&
                activeDataLayers[0]?.styleLayers[i]?.featureRequest)) && (
              <MapHover
                key={'hover' + layer.id + i}
                map={map}
                mapLayerId={layer.id}
                styleLayer={activeDataLayers[0].styleLayers[i]}
                selectedFeatureID={selectedFeaturesInfo?.features[0]?.id}
              />
            )}
            {((layer.type !== 'symbol' &&
              !activeDataLayers[0]?.styleLayers[i]?.isHighlightLayer &&
              activeDataLayers[0]?.styleLayers[i]?.detailModal) ||
              (layer.type === 'raster' &&
                !activeDataLayers[0]?.styleLayers[i]?.isHighlightLayer)) && (
              <MapClick
                key={'click' + layer.id + i}
                map={map}
                mapLayerId={layer.id}
                styleLayer={activeDataLayers[0].styleLayers[i]}
                selectedFeaturesInfo={selectedFeaturesInfo}
                setSelectedFeaturesInfo={setSelectedFeaturesInfo}
                setSelectedStyleLayerSource={setSelectedStyleLayerSource}
                setSelectedRelevantStyleLayer={setSelectedRelevantStyleLayer}
                currentEvent={currentEvent}
                setCurrentEvent={setCurrentEvent}
                isActive={
                  activeDataLayers[0].styleLayers[i]?.tileSource
                    ?.sourceTitle === selectedStyleLayerSource
                }
              />
            )}
          </React.Fragment>
        ))}
    </>
  )
}

type MapLayerProps = {
  map: Map
  mapLayer: CustomLayerSpecification
}
const MapLayer: FC<MapLayerProps> = ({ map, mapLayer }) => {
  useEffect(() => {
    const insertBefore = mapLayer.insert || getDefaultInsertPoint(map, mapLayer)
    const removeLayer = () => {
      map.getLayer(mapLayer.id) && map.removeLayer(mapLayer.id)
    }

    removeLayer()
    map.addLayer(mapLayer, insertBefore)
    map.setFilter(mapLayer.id, mapLayer.filter, { validate: true })

    return removeLayer
  }, [map, mapLayer])

  return null
}

type ZoomProps = {
  map: Map
}
const ZoomToLayer: FC<ZoomProps> = ({ map }) => {
  const { activeDataLayers } = useTabsLayers()
  const dataLayer = activeDataLayers[0]
  const relevantStyleLayer =
    dataLayer?.styleLayers?.find((layer) => layer.isRelevantLayer) ||
    dataLayer?.styleLayers?.[0]

  if (!relevantStyleLayer) {
    return null
  }

  // FIX: This appears to throw when executed. Something about updating
  // RelevantLayers, which is a parent of this component.
  const zoomTo = relevantStyleLayer.tileSource?.minzoom
  if (zoomTo && map.getZoom() < zoomTo) {
    map.zoomTo(zoomTo)
  }

  return null
}
export default MapRelevantLayers
