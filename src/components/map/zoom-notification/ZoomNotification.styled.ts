// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

interface ZoomNotificationStyledProps {
  $isVisible: boolean
}

export const Container = styled.div<ZoomNotificationStyledProps>`
  opacity: ${(props) => (props.$isVisible ? 1 : 0)};
  align-items: center;
  background-color: rgba(0, 0, 0, 0.43);
  border-radius: 100px;
  color: white;
  display: grid;
  grid-area: 1/2;
  grid-auto-flow: column;
  height: fit-content;
  justify-content: center;
  margin-left: 1rem;
  margin-top: 1rem;
  padding: 10px 20px;
  width: fit-content;
  z-index: 2;
  transition: opacity 0.5s ease-in-out;
`

export const Text = styled.p``
