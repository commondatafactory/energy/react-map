// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FunctionComponent, useEffect, useRef } from 'react'
import maplibregl from 'maplibre-gl'

export const makePopupContent = (
  { feature, styleLayer, postalCodes },
  selection
) => {
  if (!feature) {
    return null
  }

  const popupData = styleLayer?.popup(feature)
  let title =
    popupData?.title ||
    (styleLayer.attrName &&
      `${feature.properties[styleLayer.attrName]} ${styleLayer.unit}`)
  title = selection ? 'Selectie: ' + title : title
  const content = popupData?.content
  const titleHtml = title ? `<h4>${title}</h4>` : ''
  const contentHtml = content ? `<p>${content}</p>` : ''
  const postalCodesHtml = postalCodes ? `<p>${postalCodes}</p>` : ''

  return (
    (titleHtml || contentHtml) && `${titleHtml}${contentHtml}${postalCodesHtml}`
  )
}

interface MapPopupProps {
  map: maplibregl.Map
  context: any
  isSelected?: boolean
}

export const MapPopup: FunctionComponent<MapPopupProps> = ({
  map,
  context: { lngLat, ...context },
  isSelected,
}) => {
  const popupRef = useRef<maplibregl.Popup>(null)

  useEffect(() => {
    if (!map || !lngLat || !context) {
      return
    }

    const initPopup = new maplibregl.Popup({
      closeButton: false,
      closeOnClick: false,
      closeOnMove: false,
      offset: [0, -15],
    })

    if (!popupRef.current) {
      initPopup.addTo(map)
      popupRef.current = initPopup
    }

    return () => {
      initPopup.remove()
      popupRef.current = null
    }
  }, [])

  useEffect(() => {
    if (!map || !popupRef.current || !lngLat) {
      return
    }

    popupRef.current.setLngLat(lngLat)
    const content = makePopupContent(context, isSelected)
    content && popupRef.current.setHTML(content)
    isSelected && popupRef.current.addClassName('selection')
  }, [lngLat])

  return null
}
