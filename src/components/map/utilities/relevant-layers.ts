// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Map } from 'maplibre-gl'
import { getBuildingLayer } from '../../../data/context-style-layers'
import { StyleLayer } from '../../../data/data-layers'
import {
  CustomLayerSpecification,
  makeInvertLayer,
  makeMapLayer,
} from '../../../data/make-map-layer'
import { CustomFilter } from '../../../providers/map-provider'

export const getDefaultInsertPoint = (
  map: Map,
  mapLayer: CustomLayerSpecification
) => {
  // Specific layers
  if (mapLayer.type === 'fill-extrusion') {
    const pointLayer = map
      .getStyle()
      .layers.find(
        (mapLayer) => mapLayer.type === 'circle' || mapLayer.type === 'symbol'
      )
    if (pointLayer) {
      return pointLayer.id
    }
    return ''
  }

  if (mapLayer.id === 'bag:panden:fill') {
    const rasterLayer = map
      .getStyle()
      .layers.find((mapLayer) => mapLayer.type === 'raster')
    if (rasterLayer) {
      return rasterLayer.id
    }
  }

  // Generic layers
  if (mapLayer.type === 'circle') {
    return 'labels_populated_places'
  } else if (mapLayer.type === 'raster') {
    // TODO: Something to do with liander or rioned layers? Find out if this
    // can be deleted, or needs fixing etc.
    if (map.getLayer('bag:panden:fill-extrusion')) {
      return 'bag:panden:fill-extrusion'
    }
  } else if (mapLayer.type === 'symbol') {
    return ''
  }

  // All other layers
  return 'road_labels'
}

export const generateMapLayer = (
  styleLayer: Partial<StyleLayer>,
  relevantLayer: Partial<StyleLayer>,
  threeDimensional: boolean,
  customFilter: CustomFilter
): CustomLayerSpecification => {
  let newLayer: CustomLayerSpecification

  if (styleLayer.id === 'layer0') {
    newLayer = getBuildingLayer(
      'layer0',
      threeDimensional
    ) as CustomLayerSpecification
  } else if (styleLayer.id === 'noDataLayer') {
    newLayer = makeInvertLayer({
      ...relevantLayer,
      id: 'noDataLayer',
    }) as CustomLayerSpecification
  } else if (styleLayer.geomType === 'symbol') {
    const layer = { ...relevantLayer }
    layer.geomType = 'symbol'

    newLayer = makeMapLayer(
      { ...layer },
      threeDimensional,
      customFilter,
      styleLayer.attrName
    ) as CustomLayerSpecification
  } else if (styleLayer.tileSource) {
    newLayer = makeMapLayer(
      styleLayer,
      threeDimensional,
      customFilter
    ) as CustomLayerSpecification
  } else {
    throw Error('Huh?')
  }

  // Create unique maplayer ID
  const id = [
    newLayer.source,
    newLayer['source-layer'],
    newLayer.type,
    newLayer.id,
  ]
    .filter(Boolean)
    .join(':')

  return {
    ...newLayer,
    id,
    insert: styleLayer.insert || '',
  }
}
