// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'
import { Spinner } from '@commonground/design-system'

export const StyledSpinnerContainer = styled.div`
  width: 36px;
  height: 36px;
  background-color: var(--colorBackground);
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: var(--spacing02);
  box-shadow: var(--boxShadow);
  animation: 1s popIn;
  animation-fill-mode: both;
  visibility: hidden;
`

export const StyledSpinner = styled(Spinner)`
  border: none;
  outline: none;
  box-shadow: none;
  background: none;
  pointer-events: none !important;
  margin: 0 !important;
  margin-right: 0 !important;
`
