// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useContext } from 'react'
import { MapContext } from '../../../providers/map-provider'
import { StyledSpinnerContainer, StyledSpinner } from './map-spinner.styles'

export const MapSpinner: FC = () => {
  const { mapLoading } = useContext(MapContext)

  return mapLoading ? (
    <StyledSpinnerContainer>
      <StyledSpinner />
    </StyledSpinnerContainer>
  ) : null
}
