// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { Spinner } from '@commonground/design-system'
import { Feature } from 'maplibre-gl'

import { createAutoBrancheInfo } from '../../../services/count/create-autobranche-export'
import { createHandelsRegisterInfo } from '../../../services/count/create-bedrijventerreinen-export'
import { getBovagObjecten } from '../../../services/count/services/get-bovag-objecten'
import { getKVKObjecten } from '../../../services/count/services/get-kvk-objecten'
import { getRDWobjecten } from '../../../services/count/services/get-rdw-objecten'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import { Download } from './Download'
import { DownloadFileType } from './Count'

import * as Styled from './Count.styled'

interface CountProps {
  selectedPolygonForCount: Feature
  activeData: 'autobranche' | 'bedrijventerreinen'
}

export const CountDownloadAutoBranche: FunctionComponent<CountProps> = ({
  selectedPolygonForCount,
  activeData,
}) => {
  const [totaalRdw, setTotaalRdw] = useState(0)
  const [totaalBovag, setTotaalBovag] = useState(0)
  const [totaalActiviteiten, setTotaalActiviteiten] = useState(0)
  const [isDownloadable, setIsDownloadable] = useState(true)
  const [isLoading, setIsLoading] = useState(false)
  const [totaalVestigingen, setTotaalVestigingen] = useState(0)

  const { activeDataLayers } = useTabsLayers()

  const rdwDownloadLimit = 250

  useEffect(() => {
    ;(async () => {
      if (selectedPolygonForCount?.id) {
        setIsLoading(true)

        await Promise.all([
          activeData === 'autobranche' &&
            getRDWobjecten(selectedPolygonForCount, true),
          activeData === 'autobranche' &&
            getBovagObjecten(selectedPolygonForCount, true),

          getKVKObjecten(
            selectedPolygonForCount,
            true,
            activeData === 'autobranche'
              ? 'any_match-l2_code=45&any_match-l2_code=77&reduce=vestigingen'
              : 'reduce=vestigingen'
          ),
          getKVKObjecten(
            selectedPolygonForCount,
            false,
            activeData === 'autobranche'
              ? 'any_match-l2_code=45&any_match-l2_code=77&reduce=count'
              : 'reduce=count'
          ),
        ]).then(
          ([rdwObjecten, bovagObjecten, kvkVestigingen, kvkActiviteiten]) => {
            setIsDownloadable(true)

            if (rdwObjecten) {
              if (rdwObjecten.totaal > rdwDownloadLimit) {
                setIsDownloadable(false)
              }
              setTotaalRdw(rdwObjecten.totaal)
            }

            if (bovagObjecten) {
              setTotaalBovag(bovagObjecten.totaal)
            }
            if (kvkVestigingen) {
              setTotaalVestigingen(kvkVestigingen.totaal)
            }
            if (kvkActiviteiten) {
              setTotaalActiviteiten(kvkActiviteiten.totaal)
            }
          }
        )

        setIsLoading(false)
      }
    })()
  }, [selectedPolygonForCount])

  const handleDownload = (fileType: DownloadFileType) => {
    if (activeData === 'autobranche') {
      createAutoBrancheInfo(selectedPolygonForCount, fileType)
    }

    if (activeData === 'bedrijventerreinen') {
      createHandelsRegisterInfo(selectedPolygonForCount, fileType, '')
    }
  }

  return (
    <>
      {activeDataLayers[0].name !== 'Adressen (verblijfsobjecten)' && (
        <Styled.Totals>
          {activeData === 'autobranche' && (
            <Styled.TotalText>
              {isLoading ? (
                <Spinner />
              ) : (
                <>
                  <span>{totaalRdw.toLocaleString('nl-NL')}</span> Bedrijven met
                  RDW erkenning
                </>
              )}
            </Styled.TotalText>
          )}

          {activeData === 'autobranche' && (
            <Styled.TotalText>
              {isLoading ? (
                <Spinner />
              ) : (
                <>
                  <span>{totaalBovag.toLocaleString('nl-NL')}</span> Bedrijven
                  met BOVAG lidmaatschap
                </>
              )}
            </Styled.TotalText>
          )}
          <Styled.TotalText>
            {isLoading ? (
              <Spinner />
            ) : (
              <>
                <span>{totaalVestigingen.toLocaleString('nl-NL')}</span>{' '}
                Vestigingen geregistreerd in de KVK
              </>
            )}
          </Styled.TotalText>
          <Styled.TotalText>
            {isLoading ? (
              <Spinner />
            ) : (
              <>
                <span>{totaalActiviteiten.toLocaleString('nl-NL')}</span>{' '}
                Bedrijfsactiviteiten geregistreerd in de KVK
              </>
            )}
          </Styled.TotalText>
        </Styled.Totals>
      )}

      {selectedPolygonForCount?.id && (
        <Download
          disabled={!isDownloadable}
          handleDataDownload={handleDownload}
        />
      )}

      {!isDownloadable && (
        <p style={{ fontSize: '0.875rem' }}>
          Bij meer dan {rdwDownloadLimit} bedrijven met RDW erkenning kun je de
          data niet downloaden. Maak je selectie kleiner.
        </p>
      )}
    </>
  )
}
