// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import { Spinner } from '@commonground/design-system'
import { Feature } from 'maplibre-gl'
import { createHandelsRegisterInfo } from '../../../services/count/create-bedrijventerreinen-export'
import { getKVKObjecten } from '../../../services/count/services/get-kvk-objecten'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import { useMap } from '../../../hooks/use-map'
import { Download } from './Download'
import { DownloadFileType } from './Count'

import * as Styled from './Count.styled'

interface CountProps {
  selectedPolygonForCount: Feature
  activeData: 'autobranche' | 'bedrijventerreinen'
}

export const CountDownloadKVK: FunctionComponent<CountProps> = ({
  selectedPolygonForCount,
  activeData,
}) => {
  const [totaalActiviteiten, setTotaalActiviteiten] = useState(0)
  const [totaalVestigingen, setTotaalVestigingen] = useState(0)
  const [sbiCodeAggregate, setSBICodesAggregate] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [urlFilter, setUrlFilter] = useState('')
  const { activeDataLayers } = useTabsLayers()

  const { customFilters } = useMap()

  // Fetch count data to display in the modal
  useEffect(() => {
    ;(async () => {
      if (!selectedPolygonForCount?.id) {
        return
      }

      setIsLoading(true)
      setUrlFilter(
        customFilters[0].list.reduce((acc, el) => {
          acc += `any_match-l${el.length}_code=${el}&`
          return acc
        }, '')
      )

      // INFO: If list.length > 0 and there is a urlFilter, that means
      // that the user selected some SBI codes. Otherwise, if filter[1]
      // is true, the user has selected all SBI codes.
      if (
        (customFilters[0].list.length && urlFilter.length) ||
        customFilters[0].filter[1]
      ) {
        await Promise.all([
          getKVKObjecten(
            selectedPolygonForCount,
            true,
            urlFilter + 'groupby=sbicode&reduce=count'
          ),
          getKVKObjecten(
            selectedPolygonForCount,
            true,
            urlFilter + 'reduce=vestigingen'
          ),
          getKVKObjecten(
            selectedPolygonForCount,
            true,
            urlFilter + 'reduce=count'
          ),
        ]).then(([kvkSbiCodes, kvkVestigingen, kvkActiviteiten]) => {
          if (kvkSbiCodes) {
            setSBICodesAggregate(kvkSbiCodes.data)
          }

          if (kvkActiviteiten) {
            setTotaalActiviteiten(kvkActiviteiten.totaal)
          }

          if (kvkVestigingen) {
            setTotaalVestigingen(kvkVestigingen.totaal)
          }
        })
      } else {
        setTotaalActiviteiten(0)
        setSBICodesAggregate([])
        setTotaalVestigingen(0)
        setUrlFilter('')
      }

      setIsLoading(false)
    })()
  }, [
    selectedPolygonForCount,
    customFilters[0].list.length,
    customFilters[0].filter[1],
    urlFilter.length,
  ])

  // Fetch the actual rows and package them as a file for downloading.
  const handleDownload = (fileType: DownloadFileType) => {
    if (activeData === 'bedrijventerreinen') {
      createHandelsRegisterInfo(selectedPolygonForCount, fileType, urlFilter)
    }
  }

  // FIX: The layer name 2 lines down does not exist anymore.
  return (
    <>
      {activeDataLayers[0].id !== 'layerDook1' && (
        <Styled.Totals>
          <Styled.TotalText>
            {isLoading ? (
              <Spinner />
            ) : (
              <>
                <span>{totaalVestigingen.toLocaleString('nl-NL')}</span>{' '}
                Vestigingen geregistreerd in de KVK
              </>
            )}
          </Styled.TotalText>

          <Styled.TotalText>
            {isLoading ? (
              <Spinner />
            ) : (
              <>
                <span>{totaalActiviteiten.toLocaleString('nl-NL')}</span>{' '}
                bedrijfsactiviteiten binnen de Vestigingen
              </>
            )}
          </Styled.TotalText>

          <Styled.TotalText>
            {isLoading ? (
              <Spinner />
            ) : (
              <>
                <span>{Object.entries(sbiCodeAggregate).length}</span>{' '}
                verschillende sbicodes gevonden
              </>
            )}
          </Styled.TotalText>
        </Styled.Totals>
      )}

      {selectedPolygonForCount?.id && (
        <Download handleDataDownload={handleDownload} />
      )}
    </>
  )
}
