// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { FC, useEffect, useState, useContext } from 'react'

import { DrawContext } from '../../providers/draw-provider'
import { useTabsLayers } from '../../hooks/use-tabs-layers'
import { useMap } from '../../hooks/use-map'
import { Count } from './count/Count'
import { ModalBasics } from './ModalBasics'
import { ArmoedeDetail } from './detail/ArmoedeDetail'
import { BuildingDetail } from './detail/BuildingDetail'
import { Detail } from './detail/Detail'
import { EigendomsSituatieDetail } from './detail/EigendomsSituatieDetail'
import { RDWdetail } from './detail/RDWDetail'
import { VerbruikDetail } from './detail/VerbruikDetail'

export const Modal: FC = () => {
  const { setDrawMode } = useContext(DrawContext)
  const { activeDataLayers } = useTabsLayers()
  const { analyzeMode, setAnalyzeMode, selectedFeatureAndEvent } = useMap()

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')

  // Modal uit on layer change
  const [previousActiveDataLayer, setPreviousActiveDataLayer] =
    useState<typeof activeDataLayers[0]>()

  const handleModalClose = () => {
    setTitle(null)
    setDescription(null)
    setAnalyzeMode(null)
    setDrawMode(null)
  }

  useEffect(() => {
    if (analyzeMode !== null) {
      return
    }

    setAnalyzeMode('watching-selection')
  }, [selectedFeatureAndEvent])

  // Initiate draw mode
  useEffect(() => {
    if (analyzeMode === 'count') {
      setDrawMode('initPolygons')

      const firstActiveDataLayer = activeDataLayers.find(
        (datalayer) => datalayer?.id
      )

      if (!firstActiveDataLayer || firstActiveDataLayer.id === 'layer0') {
        setTitle('Selecteer een laag')
        setDescription('')
        return
      }

      setTitle('Verblijfsobjecten analyseren')
      setDescription(
        firstActiveDataLayer.id ? `Verdeling ${firstActiveDataLayer.name}` : ''
      )
    }
  }, [
    activeDataLayers,
    analyzeMode,
    selectedFeatureAndEvent?.relevantStyleLayerDetailModal,
  ])

  // Close the modal if the new layer uses a different tile source
  useEffect(() => {
    const firstActiveDataLayer = activeDataLayers.find(
      (datalayer) => datalayer?.id
    )

    if (!firstActiveDataLayer || !previousActiveDataLayer) {
      return
    }

    if (firstActiveDataLayer.id !== previousActiveDataLayer.id) {
      // TODO: These two conditions seem to have the same effect. Both paths
      // set analyze mode to null, which closes the GUI elements of the modal.
      // Why not always use handleModalClose? Investigate.
      if (
        analyzeMode === 'watching-selection' &&
        firstActiveDataLayer.styleLayers[0].tileSource !==
          previousActiveDataLayer.styleLayers[0].tileSource
      ) {
        setAnalyzeMode(null)
      } else if (analyzeMode === 'count') {
        handleModalClose()
      }
    }

    setPreviousActiveDataLayer(firstActiveDataLayer)
  }, [activeDataLayers])

  // Set the content mode for the modal
  const modalMode = (() => {
    if (!analyzeMode) {
      return null
    }

    if (analyzeMode === 'count') {
      return 'count'
    }

    if (analyzeMode === 'watching-selection') {
      if (
        !selectedFeatureAndEvent?.relevantStyleLayerDetailModal ||
        // selectedFeatureAndEvent?.features?.[0]?.id can be an emptry string in case of raster layers
        selectedFeatureAndEvent?.features?.[0]?.id === undefined ||
        selectedFeatureAndEvent?.features?.[0]?.id === null
      ) {
        return null
      }

      return selectedFeatureAndEvent.relevantStyleLayerDetailModal.detailModal({
        id: selectedFeatureAndEvent.features[0].id,
        properties: {},
      }).type
    }

    return null
  })()

  if (!modalMode) {
    return <></>
  }

  return (
    <ModalBasics
      title={title}
      description={description}
      handleClose={handleModalClose}
    >
      {modalMode === 'count' && <Count handleModalClose={handleModalClose} />}

      {modalMode === 'building' && <BuildingDetail setTitle={setTitle} />}

      {modalMode === 'verbruik' && <VerbruikDetail setTitle={setTitle} />}

      {modalMode === 'eg' && <EigendomsSituatieDetail setTitle={setTitle} />}

      {modalMode === 'armoede' && (
        <ArmoedeDetail setTitle={setTitle} setDescription={setDescription} />
      )}

      {modalMode === 'rdw-detail' &&
        selectedFeatureAndEvent.features
          .filter(
            (feature, index, self) =>
              index === self.findIndex((item) => item.id === feature.id)
          )
          .map((feature, i, self) => (
            <RDWdetail
              key={feature.id}
              setTitle={setTitle}
              feature={feature}
              filteredFeatures={self}
            />
          ))}

      {modalMode === 'detail' &&
        selectedFeatureAndEvent.features
          .filter(
            (feature, index, self) =>
              index === self.findIndex((item) => item.id === feature.id)
          )
          .map((feature, i, self) => (
            <Detail
              key={feature.id}
              feature={feature}
              setTitle={setTitle}
              filteredFeatures={self}
            />
          ))}
    </ModalBasics>
  )
}
