// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { Icon } from '@commonground/design-system'
import EditIcon from '../../../public/images/pencil.svg'
import DeleteIcon from '../../../public/images/delete.svg'
import Close from '../../../public/images/close.svg'
import * as Styled from './Modal.styles'

interface MapModalProps {
  handleClose: any
  title: string
  description?: string
  className?: string
  handleDelete?: () => void | null | false | undefined
  handleEdit?: () => void | null | false | undefined
  [props: string]: any
}

export const modalWidth = 482

export const ModalBasics: React.FC<MapModalProps> = ({
  children,
  handleClose,
  title,
  description,
  className,
  handleDelete,
  handleEdit,
  ...props
}) => (
  <Styled.MainContainer width={modalWidth} className={className} {...props}>
    <Styled.Header>
      <h1>{title}</h1>
      {handleDelete && (
        <Styled.Icon role="button" onClick={handleDelete}>
          <Icon as={DeleteIcon} size="x-large" />
        </Styled.Icon>
      )}
      {handleEdit && (
        <Styled.Icon role="button" onClick={handleEdit}>
          <Icon as={EditIcon} size="x-large" />
        </Styled.Icon>
      )}

      <Styled.Icon role="button" onClick={handleClose}>
        <Icon as={Close} size="x-large" />
      </Styled.Icon>
    </Styled.Header>
    {description && <Styled.Description>{description}</Styled.Description>}
    <Styled.Content>{children}</Styled.Content>
  </Styled.MainContainer>
)
