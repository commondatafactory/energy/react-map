// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'

import Graph from '../../asset-components/graphs/verticalGraph'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'

interface DetailProps {
  setTitle
}

export const EigendomsSituatieDetail: FC<DetailProps> = ({ setTitle }) => {
  const { selectedFeatureAndEvent } = useMap()
  const [InwonersData, setInwonersData] = useState([])
  const [HuishoudenData, setHuishoudenData] = useState([])

  const content =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal?.detailModal({
      id: selectedFeatureAndEvent?.features?.[0]?.id,
      properties: selectedFeatureAndEvent?.features?.[0]?.properties,
    })

  const properties = selectedFeatureAndEvent?.features?.[0].properties

  useEffect(() => {
    setTitle(content.title)
  }, [])

  useEffect(() => {
    if (
      properties &&
      Object.values(properties).length > 0 &&
      (properties.aantal_inwoners_0_tot_15_jaar ||
        properties.aantal_inwoners_15_tot_25_jaar ||
        properties.aantal_inwoners_25_tot_45_jaar ||
        properties.aantal_inwoners_45_tot_65_jaar ||
        properties.aantal_inwoners_65_jaar_en_ouder ||
        properties.aantal_eenpersoonshuishoudens ||
        properties.aantal_meerpersoonshuishoudens_zonder_kind)
    ) {
      setInwonersData([
        {
          key: '0 tot 15 jaar',
          value: properties.aantal_inwoners_0_tot_15_jaar || 0,
        },
        {
          key: '15 tot 25 jaar',
          value: properties.aantal_inwoners_15_tot_25_jaar || 0,
        },
        {
          key: '25 tot 45 jaar',
          value: properties.aantal_inwoners_25_tot_45_jaar || 0,
        },
        {
          key: '45 tot 65 jaar',
          value: properties.aantal_inwoners_45_tot_65_jaar || 0,
        },
        {
          key: '65 of ouder',
          value: properties.aantal_inwoners_65_jaar_en_ouder || 0,
        },
      ])

      setHuishoudenData([
        {
          key: 'Eenpersoons',
          value: properties.aantal_eenpersoonshuishoudens || 0,
        },
        {
          key: 'Meerpers zonder kinderen',
          value: properties.aantal_meerpersoonshuishoudens_zonder_kind || 0,
        },
        {
          key: 'Een-ouder huishouden',
          value: properties.aantal_eenouderhuishoudens || 0,
        },
        {
          key: 'Twee-ouder huishouden',
          value: properties.aantal_tweeouderhuishoudens || 0,
        },
      ])
    } else {
      setInwonersData([])
      setHuishoudenData([])
    }
  }, [properties])

  return (
    <>
      {content.content.map((item) =>
        item ? (
          <Styled.Item key={item.join('')}>
            <p>{item[0]}</p>
            <p>{item[1]}</p>
          </Styled.Item>
        ) : null
      )}
      {content.url && (
        <Styled.Item>
          <p>Externe link</p>
          <p>
            <a target="blank" href={content.url}>
              Klik hier
            </a>
          </p>
        </Styled.Item>
      )}
      <Styled.StyledGraph>
        {InwonersData.length > 0 && (
          <>
            <h4 className="graphTitle">Verdeling inwoners naar leeftijd</h4>
            <Graph
              key="graphverdelingleeftijd"
              dataSet={InwonersData}
              colors={['var(--colorInfo)']}
              margin={{ top: 10, right: 10, bottom: 90, left: 20 }}
              height={250}
              time={750}
              labels
            />
          </>
        )}
      </Styled.StyledGraph>
      <Styled.StyledGraph>
        {HuishoudenData.length > 0 && (
          <>
            <h4 className="graphTitle">Verdeling type huishoudens</h4>
            <Graph
              key="graphverdelinghuishoudens"
              dataSet={HuishoudenData}
              colors={['var(--colorInfo)']}
              margin={{ top: 10, right: 10, bottom: 90, left: 20 }}
              height={250}
              time={750}
              labels
            />
          </>
        )}
      </Styled.StyledGraph>
    </>
  )
}
