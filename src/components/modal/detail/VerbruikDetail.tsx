// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'

import Graph from '../../asset-components/graphs/verticalGraph'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'

interface DetailProps {
  setTitle
}

export const VerbruikDetail: FC<DetailProps> = ({ setTitle }) => {
  const { selectedFeatureAndEvent } = useMap()
  const [dataElek, setDataElek] = useState([])
  const [dataGas, setDataGas] = useState([])

  const content =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal?.detailModal({
      id: selectedFeatureAndEvent?.features?.[0]?.id,
      properties: selectedFeatureAndEvent?.features?.[0]?.properties,
    })
  const properties = selectedFeatureAndEvent?.features?.[0].properties

  useEffect(() => {
    setTitle(content.title)
  }, [])

  useEffect(() => {
    if (properties && Object.values(properties).length > 0) {
      setDataElek([
        {
          key: 'Appartement',
          value:
            properties.gemiddeld_elektriciteitsverbruik_appartement >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_appartement
              : 0,
        },
        {
          key: 'Tussen woning',
          value:
            properties.gemiddeld_elektriciteitsverbruik_tussenwoning >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_tussenwoning
              : 0,
        },
        {
          key: 'Hoek woning',
          value:
            properties.gemiddeld_elektriciteitsverbruik_hoekwoning >= 0
              ? properties.gemiddeld_elektriciteitsverbruik_hoekwoning
              : 0,
        },
        {
          key: 'TweeOnder EenKap',
          value:
            properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning >= 0
              ? properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning
              : 0,
        },
        {
          key: 'Vrijstaand',
          value:
            properties.gem_elektriciteitsverbruik_vrijstaande_woning >= 0
              ? properties.gem_elektriciteitsverbruik_vrijstaande_woning
              : 0,
        },
      ])
      setDataGas([
        {
          key: 'Appartement',
          value:
            properties.gemiddeld_gasverbruik_appartement >= 0
              ? properties.gemiddeld_gasverbruik_appartement
              : 0,
        },
        {
          key: 'Tussen woning',
          value:
            properties.gemiddeld_gasverbruik_tussenwoning >= 0
              ? properties.gemiddeld_gasverbruik_tussenwoning
              : 0,
        },
        {
          key: 'Hoek woning',
          value:
            properties.gemiddeld_gasverbruik_hoekwoning >= 0
              ? properties.gemiddeld_gasverbruik_hoekwoning
              : 0,
        },
        {
          key: 'TweeOnder EenKap',
          value:
            properties.gemiddeld_gasverbruik_2_onder_1_kap_woning >= 0
              ? properties.gemiddeld_gasverbruik_2_onder_1_kap_woning
              : 0,
        },
        {
          key: 'Vrijstaand',
          value:
            properties.gemiddeld_gasverbruik_vrijstaande_woning >= 0
              ? properties.gemiddeld_gasverbruik_vrijstaande_woning
              : 0,
        },
      ])
    } else {
      setDataElek([])
      setDataGas([])
    }
  }, [properties])

  return (
    <>
      {content.content.map(
        (item) =>
          item && (
            <Styled.Item key={item.join('')}>
              <p>{item[0]}</p>
              <p>{item[1]}</p>
            </Styled.Item>
          )
      )}
      {content.url && (
        <Styled.Item>
          <p>Externe link</p>
          <p>
            <a target="blank" href={content.url}>
              Klik hier
            </a>
          </p>
        </Styled.Item>
      )}
      <Styled.StyledGraph>
        <h4 className="graphTitle">
          Gemiddeld elektriciteitsverbruik naar woning type
        </h4>
        <h5 className="graphNote">
          Elektriciteit (
          <span style={{ color: '#ffbc2c', fontWeight: 'bold' }}>kwh</span>)
        </h5>
        {dataElek && (
          <Graph
            dataSet={dataElek}
            colors={['#ffbc2c']}
            height={200}
            margin={{ top: 10, right: 0, bottom: 110, left: 35 }}
            time={750}
            labels
          />
        )}
      </Styled.StyledGraph>
      <Styled.StyledGraph>
        <h4 className="graphTitle">Gemiddeld gasverbruik naar woning type</h4>
        <h5 className="graphNote">
          Gas (
          <span style={{ color: 'var(--colorInfo)', fontWeight: 'bold' }}>
            m3
          </span>
          )
        </h5>
        {dataGas && (
          <Graph
            dataSet={dataGas}
            colors={['var(--colorInfo)']}
            height={200}
            margin={{ top: 10, right: 0, bottom: 110, left: 35 }}
            time={750}
            labels
          />
        )}
      </Styled.StyledGraph>
    </>
  )
}
