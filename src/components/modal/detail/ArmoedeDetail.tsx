// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, useEffect, useState } from 'react'
import Graph2 from '../../asset-components/graphs/horizontalBarsDouble'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './Detail.styled'

type DetailProps = {
  setTitle
  setDescription
}

export const ArmoedeDetail: FC<DetailProps> = ({
  setTitle,
  setDescription,
}) => {
  const { selectedFeatureAndEvent } = useMap()

  const attrName =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal.attrName

  const content =
    selectedFeatureAndEvent.relevantStyleLayerDetailModal?.detailModal({
      id: selectedFeatureAndEvent?.features?.[0]?.id,
      properties: selectedFeatureAndEvent?.features?.[0]?.properties,
    })
  const properties = selectedFeatureAndEvent?.features?.[0].properties
  const colorScale =
    selectedFeatureAndEvent?.relevantStyleLayerDetailModal.colorScale

  useEffect(() => {
    setTitle(content.title)
    setDescription(content.desc)
  }, [])

  const [dataSet, setDataSet] = useState([])
  const [colors, setColors] = useState([])
  const [subTilte, setSubTitle] = useState('')
  const [dataSet2, setDataSet2] = useState([])

  const namen = [
    'alle huishoudens',
    'eigenaar bewoner',
    'huur (corporatie)',
    'huur (overig)',
    'onbekend',
  ]

  const attrbOverzicht = {
    heq: [
      properties.tot_heq >= 0 ? properties.tot_heq : 0,
      properties.koop_heq >= 0 ? properties.koop_heq : 0,
      properties.huur_c_heq >= 0 ? properties.huur_c_heq : 0,
      properties.huur_o_heq >= 0 ? properties.huur_o_heq : 0,
      properties.onb_heq >= 0 ? properties.onb_heq : 0,
    ],
    lihe: [
      properties.tot_lihe >= 0 ? properties.tot_lihe : 0,
      properties.koop_lihe >= 0 ? properties.koop_lihe : 0,
      properties.huur_c_lihe >= 0 ? properties.huur_c_lihe : 0,
      properties.huur_o_lihe >= 0 ? properties.huur_o_lihe : 0,
      properties.onb_lihe >= 0 ? properties.onb_lihe : 0,
    ],

    lilek: [
      properties.tot_lilek >= 0 ? properties.tot_lilek : 0,
      properties.koop_lilek >= 0 ? properties.koop_lilek : 0,
      properties.huur_c_lilek >= 0 ? properties.huur_c_lilek : 0,
      properties.huur_o_lilek >= 0 ? properties.huur_o_lilek : 0,
      properties.onb_lilek >= 0 ? properties.onb_lilek : 0,
    ],

    lihe_lilek: [
      properties.tot_lihelek2 >= 0 ? properties.tot_lihelek2 : 0,
      properties.koop_lihelek2 >= 0 ? properties.koop_lihelek2 : 0,
      properties.huur_c_lihelek2 >= 0 ? properties.huur_c_lihelek2 : 0,
      properties.huur_o_lihelek2 >= 0 ? properties.huur_o_lihelek2 : 0,
      properties.onb_lihelek2 >= 0 ? properties.onb_lihelek2 : 0,
    ],
    lekwi: [
      properties.tot_lekwi >= 0 ? properties.tot_lekwi : 0,
      properties.koop_lekwi >= 0 ? properties.koop_lekwi : 0,
      properties.huur_c_lekwi >= 0 ? properties.huur_c_lekwi : 0,
      properties.huur_o_lekwi >= 0 ? properties.huur_o_lekwi : 0,
      properties.onb_lekwi >= 0 ? properties.onb_lekwi : 0,
    ],
  }

  function makeDataSet(namen, lijst) {
    const dataset = []
    const colors = []
    for (let i = 0; i < namen.length; i++) {
      const o = {
        key: namen[i],
        value: parseFloat(lijst[i]),
      }
      colors.push(colorScale(parseFloat(lijst[i])))
      dataset.push(o)
    }
    setDataSet(dataset)
    setColors(colors)
    return
  }

  useEffect(() => {
    if (properties && Object.values(properties).length > 0) {
      if (attrName.search('heq') > 0) {
        makeDataSet(namen, attrbOverzicht['heq'])
        setSubTitle('Hoge energiequote (HEq)')
      } else if (attrName.search('lihelek2') > 0) {
        makeDataSet(namen, attrbOverzicht['lihe_lilek'])
        setSubTitle(
          'Laag inkomen en hoge energierekening en/of lage energiekwaliteit (LIHE en/of LILEK)'
        )
      } else if (attrName.search('lihe') > 0) {
        makeDataSet(namen, attrbOverzicht['lihe'])
        setSubTitle('Laag inkomen, hoge energierekening (LIHE)')
      } else if (attrName.search('lilek') > 0) {
        makeDataSet(namen, attrbOverzicht['lilek'])
        setSubTitle('Laag inkomen, lage energiekwaliteit (LILEK) ')
      } else if (attrName.search('lekwi') > 0) {
        makeDataSet(namen, attrbOverzicht['lekwi'])
        setSubTitle(
          'Lage energiekwaliteit, weinig investeringsmogelijkheden (LEKWI)'
        )
      }
    } else {
      setDataSet([])
    }
  }, [properties])

  function makeDataSet2(namen, lijst, aantal_huishoudens) {
    const dataset = []
    const colors = []
    for (let i = 0; i < namen.length; i++) {
      const o = {
        key: namen[i],
        percentage: parseFloat(lijst[i]),
        aantal: aantal_huishoudens[namen[i]],
      }
      colors.push(colorScale(parseFloat(lijst[i])))
      dataset.push(o)
    }

    return dataset
  }

  useEffect(() => {
    if (properties && Object.values(properties).length > 0) {
      let datasetje = []

      const aantal_huishoudens = {
        'alle huishoudens': parseFloat(properties.tot_huishoudens) * 1000 || 0,
        'huur (corporatie)':
          parseFloat(properties.huur_c_huishoudens) * 1000 || 0,
        'huur (overig)': parseFloat(properties.huur_o_huishoudens) * 1000 || 0,
        'eigenaar bewoner': parseFloat(properties.koop_huishoudens) * 1000 || 0,
        onbekend: parseFloat(properties.onb_huishoudens) * 1000 || 0,
      }

      if (attrName.search('heq') > 0) {
        datasetje = makeDataSet2(
          namen,
          attrbOverzicht['heq'],
          aantal_huishoudens
        )
      } else if (attrName.search('lihelek2') > 0) {
        datasetje = makeDataSet2(
          namen,
          attrbOverzicht['lihe_lilek'],
          aantal_huishoudens
        )
      } else if (attrName.search('lihe') > 0) {
        datasetje = makeDataSet2(
          namen,
          attrbOverzicht['lihe'],
          aantal_huishoudens
        )
      } else if (attrName.search('lilek') > 0) {
        datasetje = makeDataSet2(
          namen,
          attrbOverzicht['lilek'],
          aantal_huishoudens
        )
      } else if (attrName.search('lekwi') > 0) {
        datasetje = makeDataSet2(
          namen,
          attrbOverzicht['lekwi'],
          aantal_huishoudens
        )
      }
      setDataSet2(datasetje)
    }
  }, [properties])

  return (
    <>
      {content.content.map((item) =>
        item ? (
          <Styled.Item key={item.join('')}>
            <p>{item[0]}</p>
            <p>{item[1]}</p>
          </Styled.Item>
        ) : null
      )}
      {content.url && (
        <Styled.Item>
          <p>Externe link</p>
          <p>
            <a target="blank" href={content.url}>
              Klik hier
            </a>
          </p>
        </Styled.Item>
      )}
      {/* <Styled.StyledGraph>
        <h4 className="graphTitle">Verdeling naar type eigenaar</h4>
        <h5 className="graphNote">
          {subTilte} <span style={{ fontWeight: 'bold' }}>%</span>
        </h5>
        {dataSet && (
          <Graph
            dataSet={dataSet}
            colors={colors}
            height={200}
            margin={{ top: 10, right: 0, bottom: 110, left: 35 }}
            time={750}
            labels
          />
        )}
      </Styled.StyledGraph> */}

      <Styled.StyledGraph>
        <h4 className="graphTitle">
          Verdeling naar type eigenaar, percentage en absoluut aantal
          huishoudens
        </h4>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <h5 className="graphNote">
            <span style={{ fontWeight: 'bold' }}>%</span> {subTilte}
          </h5>
          <h5 style={{ textAlign: 'right' }} className="graphNote">
            <span style={{ fontWeight: 'bold' }}>Aantal huishoudens</span>
          </h5>
        </div>
        {dataSet2 && (
          <Graph2
            dataSet={dataSet2}
            colors={colors}
            height={250}
            margin={{ top: 10, right: 0, bottom: 0, left: 100 }}
            time={750}
          />
        )}
      </Styled.StyledGraph>
    </>
  )
}
