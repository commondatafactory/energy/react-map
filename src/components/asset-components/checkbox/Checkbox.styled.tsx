// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const Label = styled.label`
  display: flex;
  cursor: pointer;
  width: 100%;
  line-height: 1;
  transition: 180ms all ease-in-out;
  margin-top: 4px;
  @supports (-moz-appearance: none) {
    margin-top: 1px;
  }
`

export const Text = styled.span`
  margin-left: 0.5em;
`

export const Checkbox = styled.input`
  cursor: pointer;
  margin: 0.2rem;
  margin: 0;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  width: 16px;
  height: 16px;
  border-radius: 10%;
  margin-right: 0.5em;

  border: 2px solid var(--colorPaletteGray500);
  transition: 0.2s all linear;
  box-shadow: inset 0em 0em var(--colorInfo);
  margin: auto;

  :checked {
    box-shadow: inset 0px 0px 0px 1px #fff;
    background-color: var(--colorInfo);
  }
`
