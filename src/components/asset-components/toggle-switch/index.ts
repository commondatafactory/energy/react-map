// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export { ToggleSwitch } from './toggle-switch'
