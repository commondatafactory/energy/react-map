// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const StyledSlider = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: var(--colorPaletteGray500);
  transition: 0.4s;
  border-radius: 12px;

  &:before {
    position: absolute;
    content: '';
    height: 12px;
    width: 12px;
    left: 2px;
    bottom: 2px;
    background-color: white;
    transition: 0.4s;
    border-radius: 50%;
  }
`

export const StyledLabel = styled.label`
  display: inline-block;
  height: 16px;
  margin-right: var(--spacing03);
  position: relative;
  width: 29px;

  input {
    height: 0;
    opacity: 0;
    width: 0;
  }
  input:not([disabled]):focus + ${StyledSlider} {
    box-shadow: 0 0 1px var(--colorInfo);
  }
  input:not([disabled]):checked + ${StyledSlider} {
    background-color: var(--colorInfo);
  }
  input:not([disabled]):checked + ${StyledSlider}:before {
    transform: translateX(13px);
  }
  input:disabled + ${StyledSlider} {
    cursor: not-allowed;
    :before {
      background-color: white;
    }
  }
`
