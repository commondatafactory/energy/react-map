// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useState, ReactNode } from 'react'

import * as Styled from './Tooltip.styled'

interface TooltipProps {
  text: string
  position?: 'top' | 'bottom' | 'left'
  children: ReactNode
  style?: any
}

export const Tooltip: FunctionComponent<TooltipProps> = ({
  position,
  children,
  text,
  style,
}) => {
  const [hover, setHover] = useState(false)

  return (
    <>
      <Styled.TooltipWrapper
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        {children}
      </Styled.TooltipWrapper>

      {hover && (
        <Styled.Tooltip
          style={{ ...style, visibility: hover ? 'visible' : 'hidden' }}
          className={position}
        >
          <p dangerouslySetInnerHTML={{ __html: text }} />
        </Styled.Tooltip>
      )}
    </>
  )
}
