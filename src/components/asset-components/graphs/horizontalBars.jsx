// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
import * as Styled from './Graphs.styled'

function Graph({ dataSet, colors, width, time, colorScale, labels }) {
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']

  const graph = useRef(null)
  const barHeight = 25

  useEffect(() => {
    if (dataSet && graph.current) {
      const div = d3.select(graph.current)
      const max =
        d3.max(Object.entries(dataSet).map(([_, { value }]) => value)) * 1.1 + 1

      const labelMapping = d3.scaleOrdinal(colorScale.domain(), labels)
      const x = d3
        .scaleLinear()
        .range([0, width - 55])
        .domain([-2, max])
        .clamp(true)

      div
        .selectAll('.dataElement')
        .data(dataSet)
        .join(
          (enter) => {
            const dataElement = enter
              .append('div')
              .classed('dataElement', true)
              .attr('id', (d) => d.key)
              .style('width', width)
            dataElement
              .append('p')
              .classed('title', true)
              .html((d) => (d.value > 0 ? labelMapping(d.key) : ''))
            dataElement
              .append('div')
              .classed('rectangle', true)

              .style('height', barHeight + 'px')
              .style('background', (d) => colorScale(d.key))
              .style('width', (d) => x(d.value) + 'px')

            dataElement
              .select('.rectangle')
              .append('p')
              .classed('values', true)
              .style('color', function () {
                const c = d3.color(this.parentNode.style.backgroundColor)
                return c.r + c.g + c.b > 400 ? '#424242' : '#ffffff'
              })
              .html((d) =>
                d.value > 0 && d.value.toString().length > 4
                  ? ` ${d.value.toLocaleString('nl-NL')}`
                  : ` ${d.value}`
              )
          },
          (update) => {
            update
              .select('.rectangle')
              .style('background', (d) => colorScale(d.key))
              .transition()
              .duration(time)
              .style('width', (d) => x(d.value) + 'px')

            update
              .select('.values')
              .style('color', () => {
                const c = d3.color(this.parentNode.style.backgroundColor)
                return c.r + c.g + c.b > 400 ? '#424242' : '#ffffff' // fancy code voor contrast labels
              })
              .html((d) => (d.value > 0 ? ` ${d.value}` : ''))
            update
              .select('.title')
              .html((d) => (d.value > 0 ? labelMapping(d.key) : ''))
          },
          (exit) => {
            exit.style('width', '0px').remove()
          }
        )
    }
  }, [dataSet, width, colors])

  return <Styled.GraphDiv ref={(el) => (graph.current = el)} />
}

export default Graph
