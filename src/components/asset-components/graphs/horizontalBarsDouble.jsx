// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useEffect, useRef, useState } from 'react'
import * as d3 from 'd3'
import * as Styled from './Graphs.styled'

function scaleWithGaps(scale, where, gapSize) {
  scale = scale.copy()
  var offsets = {}
  var i = 0
  var offset = -(scale.step() * gapSize * where.length) / 2
  scale.domain().forEach((d, j) => {
    if (j == where[i]) {
      offset += scale.step() * gapSize
      ++i
    }
    offsets[d] = offset
  })
  var newScale = (value) => scale(value) + offsets[value]
  // Give the new scale the methods of the original scale
  for (var key in scale) {
    newScale[key] = scale[key]
  }
  newScale.copy = function () {
    return scaleWithGaps(scale, where, gapSize)
  }
  return newScale
}

function Graph({ dataSet, colors, time, margin, height }) {
  const graph = useRef(null)

  const [parentWidth, setParentWidth] = useState(0)

  useEffect(() => {
    setParentWidth(
      graph.current.parentNode.clientWidth - margin.left - margin.right
    )
  }, [])

  margin = margin || { top: 0, right: 0, bottom: 0, left: 150 }
  height =
    height - margin.top - margin.bottom || 350 - margin.top - margin.bottom
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']

  useEffect(() => {
    // inital setting up graph
    d3.select(graph.current)
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('width', parentWidth + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr(
        'viewBox',
        `0 0 ${parentWidth + margin.left + margin.right} ${
          height + margin.top + margin.bottom
        }`
      )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parentWidth, height])

  useEffect(() => {
    if (dataSet && graph.current) {
      const colorScale = d3
        .scaleOrdinal()
        .domain(dataSet.map((el) => el.key))
        .range(colors)

      // x scale
      const max = d3.max(
        Object.entries(dataSet).map(([_, { aantal }]) => aantal)
      )
      const n = Object.keys(dataSet).length
      const x = d3
        .scaleLinear()
        .range([0, parentWidth])
        .domain([0, max])
        .clamp(true)

      const svg = d3.select(graph.current)

      //  y categories
      const y_ = d3
        .scaleBand()
        .range([0, height])
        .domain(dataSet.map((el) => el.key))
        .padding(0.1)

      const y = scaleWithGaps(y_, [1], 0.5)

      const yAxis = d3.axisLeft(y)

      // Keep one axis
      svg
        .selectAll('g.y.axis.totaal')
        .data([1])
        .join(
          (enter) => {
            enter
              .append('g')
              .attr('transform', `translate(${margin.left},${margin.top})`)
              .classed('y axis totaal', true)
              .call(yAxis)
              .call((g) => g.select('.domain').remove())
              .call((g) => g.selectAll('.tick line').remove())
          },
          (update) => {
            update
              .call(yAxis)
              .call((g) => g.select('.domain').remove())
              .call((g) => g.selectAll('.tick line').remove())
          }
        )

      // First part, armoede percentage
      svg
        .selectAll('rect.percentage01')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .classed('percentage01', true)
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .attr('height', y.bandwidth())
              .attr('x', () => {
                return x(0)
              })
              .attr('y', ({ key }) => {
                return y(key)
              })
              .attr('fill', ({ key }) => colorScale(key))
              .attr('width', 0)
              .call((enter) => enter.transition().duration(time))
              .attr('width', function (d) {
                return x((d.percentage * d.aantal) / 100)
              }),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('fill', ({ key }) => colorScale(key))
                .attr('width', function (d) {
                  return x((d.percentage * d.aantal) / 100)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit.transition().duration(time).attr('width', 0).remove()
            )
        )
      // Second part part
      svg
        .selectAll('rect.percentage02')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .classed('percentage02', true)
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .attr('height', y.bandwidth())
              .attr('x', (d) => {
                return x((d.percentage * d.aantal) / 100)
              })
              .attr('y', (d) => {
                return y(d.key)
              })
              .attr('fill', '#dddddd')
              .attr('width', 0)
              .call((enter) => enter.transition().duration(time))
              .attr('width', function (d) {
                return x(((100 - d.percentage) * d.aantal) / 100)
              }),

          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('x', (d) => {
                  return x((d.percentage * d.aantal) / 100)
                })
                .attr('width', function (d) {
                  return x(((100 - d.percentage) * d.aantal) / 100)
                })
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition()
                .duration(time)
                .attr('x', 0)
                .attr('width', 0)
                .remove()
            )
        )

      // labels totaal aantal huishoudens
      svg
        .selectAll('.label')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('label', true)
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .attr('text-anchor', 'left')
              .style('fill', 'var(--colorPaletteGray600)')
              .style('font-weight', 'bold')
              .attr('dy', '4px')
              .attr('y', function (d) {
                return y(d.key) + y.bandwidth() / 2
              })
              .text((d) => {
                return d3.format(',')(d.aantal)
              })
              .attr('x', function (d) {
                return x(max) - d.aantal.toString().length * 7 - 5
              }),

          (update) =>
            update.call((update) =>
              update
                .text((d) => {
                  return d3.format(',')(d.aantal)
                })
                .attr('x', function (d) {
                  return x(max) - d.aantal.toString().length * 7 - 5
                })
            ),
          (exit) =>
            exit.call((exit) => exit.transition().duration(time).remove())
        )

      // labels percentage energie armoede
      svg
        .selectAll('.label2')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('text')
              .classed('label2', true)
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .attr('text-anchor', 'left')
              .style('fill', ' rgba(117, 117, 117, 1)')

              .attr('dy', '4px')
              .attr('x', function (d) {
                return x((d.percentage * d.aantal) / 100) + 5
              })
              .attr('y', function (d) {
                return y(d.key) + y.bandwidth() / 2
              })
              .text((d) => {
                if (d.percentage > 0) {
                  return d3.format(',')(d.percentage) + '%'
                }
              }),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('x', function (d) {
                  return x((d.percentage * d.aantal) / 100) + 5
                })
                .text((d) => {
                  return d3.format(',')(d.percentage) + '%'
                })
            ),
          (exit) =>
            exit.call((exit) => exit.transition().duration(time).remove())
        )
    }
  }, [dataSet, parentWidth, colors])

  return <Styled.GraphSvg ref={(el) => (graph.current = el)} />
}

export default Graph
