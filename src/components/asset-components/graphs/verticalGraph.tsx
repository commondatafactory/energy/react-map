// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import React, { useEffect, useRef, useState } from 'react'
import * as d3 from 'd3'
import { wrapVert } from '../../../utils/svg/wrap-text'
import * as Styled from './Graphs.styled'

function Graph({
  dataSet,
  colors,
  margin,
  height,
  average,
  time,
  labels,
}: {
  dataSet: any
  colors: string[]
  margin: {
    top: number
    right: number
    bottom: number
    left: number
  }
  height: number
  average?: any
  time: any
  labels: any
}) {
  const graph = useRef(null)
  margin = margin || { top: 0, right: 0, bottom: 0, left: 30 }
  height =
    height - margin.top - margin.bottom || 120 - margin.top - margin.bottom
  colors = colors || ['#7794A1', '#dddddd', '#0ba19f']

  const [parentWidth, setParentWidth] = useState(0)

  useEffect(() => {
    setParentWidth(
      graph.current.parentNode.clientWidth - margin.left - margin.right
    )
  }, [])

  useEffect(() => {
    if (parentWidth) {
      // inital setting up graph
      d3.select(graph.current)
        .attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('width', parentWidth + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .attr(
          'viewBox',
          `0 0 ${parentWidth + margin.left + margin.right} ${
            height + margin.top + margin.bottom
          }`
        )
    }
  }, [parentWidth])

  useEffect(() => {
    // Update graph
    if (graph.current && dataSet && parentWidth) {
      const max =
        d3.max(Object.entries(dataSet).map(([_, { value }]) => value)) * 1.1 + 1
      const x = d3
        .scaleBand()
        .range([0, parentWidth])
        .domain(dataSet.map((el) => el.key))
        .padding(0.1)

      const y = d3.scaleLinear().range([height, 0]).domain([0, max])
      const colorscale = d3
        .scaleOrdinal()
        .domain(dataSet.map((el) => el.key))
        .range(colors)

      const svg = d3.select(graph.current)

      // Y AXIS
      y.domain([0, max])
      const yAxis = d3
        .axisLeft(y)
        .ticks(5)
        .tickFormat((d: any) => {
          if (d / 10000 >= 1) {
            d = d / 1000 + 'K'
          }
          return d
        })

      svg
        .selectAll('g.y.axis')
        .data([max])
        .join(
          (enter) =>
            enter
              .append('g')
              .attr('transform', `translate(${margin.left}, ${margin.top})`)
              .classed('y axis', true)
              .call(yAxis)
              .call((enter) => enter.transition().duration(time).call(yAxis)),
          (update) =>
            update.call((update) =>
              update.transition().duration(time).call(yAxis)
            )
        )

      const xAxis = d3.axisBottom(x).ticks(10)

      // Keep one axis
      svg
        .selectAll('g.x.axis')
        .data([max])
        .join(
          (enter) =>
            enter
              .append('g')
              .attr(
                'transform',
                `translate(${margin.left},${height + margin.top})`
              )
              .classed('x axis', true)
              .call(xAxis)
              .selectAll('.tick text')
              .attr('transform', ({ length }) => {
                return length >= 2
                  ? 'translate(-10,10) rotate(-60)'
                  : 'translate(0,0) rotate(0)'
              })
              .style('text-anchor', ({ length }) => {
                if (length >= 2) {
                  return 'end'
                }
              })
              .call(wrapVert, margin.bottom),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .call(xAxis)
                .selectAll('.tick text')
                .attr('transform', ({ length }) => {
                  return length >= 2
                    ? 'translate(-10,10) rotate(-60)'
                    : 'translate(0,0) rotate(0)'
                })
                .style('text-anchor', ({ length }) => {
                  if (length >= 2) {
                    return 'end'
                  }
                })
                .call(wrapVert, margin.bottom)
            )
        )

      svg
        .selectAll('rect')
        .data(dataSet)
        .join(
          (enter) =>
            enter
              .append('rect')
              .attr('class', 'bar')
              .attr('transform', `translate(${margin.left} , ${margin.top})`)
              .attr('width', x.bandwidth())
              .attr('x', ({ key }) => x(key))
              .style('fill', ({ key }) => colorscale(key))
              .attr('y', ({ value }) => y(value))
              .attr('height', ({ value }) => height - y(value))
              .call((enter) =>
                enter
                  .transition()
                  .duration(time)
                  .attr('y', ({ value }) => y(value))
                  .attr('height', ({ value }) => height - y(value))
              ),
          (update) =>
            update.call((update) =>
              update
                .transition()
                .duration(time)
                .attr('y', ({ value }) => y(value))
                .attr('x', ({ key }) => x(key))
                .attr('width', x.bandwidth())
                .attr('height', ({ value }) => height - y(value))
                .style('fill', ({ key }) => colorscale(key))
            ),
          (exit) =>
            exit.call((exit) =>
              exit
                .transition()
                .duration(time)
                .attr('y', height)
                .attr('height', 0)
                .remove()
            )
        )

      if (average) {
        svg
          .selectAll('line.average')
          .data([average])
          .join(
            (enter) =>
              enter
                .append('line')
                .classed('average', true)
                .attr('transform', `translate(${margin.left} , ${margin.top})`)
                .attr('x1', 0) // x(0)
                .style('stroke', '#7794A1')
                .style('stroke-dasharray', '3,3')
                .style('stroke-width', 2)
                .attr('x2', parentWidth)
                .attr('y1', y(average))
                .attr('y2', y(average)),
            (update) =>
              update.call((update) =>
                update
                  .transition()
                  .duration(time)
                  .attr('y1', y(average))
                  .attr('y2', y(average))
              ),
            (exit) =>
              exit.call((exit) => exit.transition().duration(time).remove())
          )
      }

      if (labels) {
        svg
          .selectAll('.label')
          .data(dataSet)
          .join(
            (enter) =>
              enter
                .append('text')
                .classed('label', true)
                .attr('transform', `translate(${margin.left}, ${margin.top})`)
                .attr('dx', ({ value }) => `-${value.toString().length * 3}px`)
                .attr('dy', '-7px')
                .attr('y', ({ value }) => y(value))
                .attr('x', ({ key }) => x(key) + x.bandwidth() / 2)
                .text(({ value }) =>
                  value.toString().length > 4
                    ? value.toLocaleString('nl-NL')
                    : value
                ),
            (update) =>
              update.call((update) =>
                update
                  .transition()
                  .duration(time)
                  .attr('y', ({ value }) => y(value))
                  .attr('x', ({ key }) => x(key) + x.bandwidth() / 2)
                  .text(({ value }) =>
                    value.toString().length > 4
                      ? value.toLocaleString('nl-NL')
                      : value
                  )
              ),
            (exit) =>
              exit.call((exit) => exit.transition().duration(time).remove())
          )
      }
    }
  }, [dataSet, parentWidth, colors])

  return (
    <Styled.GraphSvg
      className="graphVertical"
      ref={(el) => (graph.current = el)}
    />
  )
}

export default Graph
