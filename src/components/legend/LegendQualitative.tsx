// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent } from 'react'
import { StyleLayer } from '../../data/data-layers'
import LegendClassificationItem from './LegendItem'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
}

const LegendQualitative: FunctionComponent<LegendProps> = ({ styleLayer }) => {
  function makeLegendStops(styleLayer) {
    const colorRange = [].concat(styleLayer.colorScale.domain())
    const legendRange = [].concat(styleLayer.legendStops)
    const stops = []
    for (let i = 0; i < legendRange.length; i++) {
      stops.push([legendRange[i], styleLayer.colorScale(colorRange[i])])
    }
    return stops
  }
  return (
    <>
      {makeLegendStops(styleLayer).map((item) => (
        <LegendClassificationItem
          key={item[0]}
          color={item[1]}
          name={item[0]}
          opacity={styleLayer.opacity || 1}
        />
      ))}
    </>
  )
}

export default LegendQualitative
