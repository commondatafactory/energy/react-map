// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { Icon } from '@commonground/design-system'

import CloseIcon from '../../../public/images/close.svg'
import LegendIcon from '../../../public/images/legend.svg'

import { useTabsLayers } from '../../hooks/use-tabs-layers'
import LegendContent from './LegendContent'

import * as Styled from './Legend.styled'

export const Legend = () => {
  const [isCollapsed, setIsCollapsed] = useState(true)
  const { activeDataLayers } = useTabsLayers()

  return (
    <Styled.LegendContainer>
      {isCollapsed ? (
        <>
          <Styled.CloseButton onClick={() => setIsCollapsed(false)}>
            <Icon as={CloseIcon} />
          </Styled.CloseButton>

          {activeDataLayers.map((dataLayer) => (
            <div key={`${dataLayer.id}legends`} style={{ margin: '0.8rem 0' }}>
              {dataLayer.styleLayers.map(
                (styleLayer, index) =>
                  styleLayer.id !== 'layer0' &&
                  styleLayer.geomType !== 'symbol' &&
                  styleLayer.id !== 'noDataLayer' && (
                    <LegendContent
                      key={`${dataLayer?.id}${index}`}
                      dataLayer={dataLayer}
                      styleLayer={styleLayer}
                    />
                  )
              )}
            </div>
          ))}
        </>
      ) : (
        <Styled.LegendButton onClick={() => setIsCollapsed(true)}>
          <Icon as={LegendIcon} />
        </Styled.LegendButton>
      )}
    </Styled.LegendContainer>
  )
}
