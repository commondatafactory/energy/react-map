// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent } from 'react'

interface props {
  image: string | string[]
}

const LegendWMSImage: FunctionComponent<props> = ({ image }): any => {
  return (
    <>
      {Array.isArray(image) ? (
        image.map((url) => {
          return (
            <div key={url} className="LegendImage">
              <img alt="legend from wms" src={url} />
            </div>
          )
        })
      ) : (
        <div key={image} className="LegendImage">
          <img alt="legend from wms" src={image} />
        </div>
      )}
    </>
  )
}

export default LegendWMSImage
