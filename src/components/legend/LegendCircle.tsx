// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent, useEffect, useState } from 'react'
import * as d3 from 'd3'
import { StyleLayer } from '../../data/data-layers'
import { useMap } from '../../hooks/use-map'

interface LegendProps {
  styleLayer: Partial<StyleLayer>
}

interface legendItems {
  backgroundColor: string
  color: string
  label: string
}

export const LegendCircle: FunctionComponent<LegendProps> = ({
  styleLayer,
}) => {
  const [legendItems, setLegendItems] = useState<legendItems[]>([])
  const { customFilters } = useMap()

  useEffect(() => {
    if (!styleLayer.legendStops) return
    if (styleLayer.colorScale) {
      const colorRange = styleLayer.colorScale.domain()
      const legendItems = styleLayer.legendStops.map((item, i) => ({
        label: item,
        color: d3
          .color((styleLayer as any).colorScale(colorRange[i]))
          .darker(0.7)
          .toString(),
        backgroundColor: d3
          .color((styleLayer as any).colorScale(colorRange[i]))
          .copy({ opacity: styleLayer.opacity || 0.8 })
          .toString(),
      }))
      setLegendItems(legendItems)
    } else if (styleLayer.color) {
      const legendItems = styleLayer.legendStops.map((item, i) => ({
        label: item,
        color: d3
          .color(styleLayer.color as any)
          .darker(0.7)
          .toString(),
        backgroundColor: d3
          .color(styleLayer.color as any)
          .copy({ opacity: styleLayer.opacity || 0.8 })
          .toString(),
      }))
      setLegendItems(legendItems)
    }
    // TODO: Test whether this filter will still work using the new customFilters data setup: Werkt niet meer. uitgezet.
    // .filter((item) =>
    //   customFilters
    //     ? customFilters?.some(
    //         (filter) => item.label.slice(0, 1) === filter[2]
    //       )
    //     : item
    // )
  }, [styleLayer, customFilters])

  return (
    <>
      {legendItems.map((item) => (
        <div key={item.label} className="legendItem">
          <div
            className="dot"
            data-testid="color"
            style={{
              backgroundColor: item.backgroundColor,
              border: `2px solid ${item.color}`,
              flexShrink: 0,
            }}
          />
          <p>{item.label}</p>
        </div>
      ))}
    </>
  )
}
