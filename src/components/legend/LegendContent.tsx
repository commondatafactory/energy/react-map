// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FC } from 'react'

import { DataLayerProps, StyleLayer } from '../../data/data-layers'
import LegendQualitative from './LegendQualitative'
import LegendQualitativeFilter from './LegendQualitativeFilter'
import LegendContinuousBar from './LegendContinuousBar'
import LegendWMSImage from './LegendWMSImage'
import { LegendCircle } from './LegendCircle'
import { LegendLine } from './LegendLine'

import * as Styled from './Legend.styled'

interface LegendContentProps {
  dataLayer: Partial<DataLayerProps>
  styleLayer: Partial<StyleLayer>
}

const LegendContent: FC<LegendContentProps> = ({ dataLayer, styleLayer }) => {
  let legendComponent = null

  if (
    styleLayer &&
    dataLayer &&
    Object.keys(styleLayer).length &&
    !styleLayer.noLegend
  ) {
    const expressionType = styleLayer.expressionType
    const geomType = styleLayer.geomType
    switch (geomType) {
      case 'raster':
        legendComponent = <LegendWMSImage image={styleLayer.image} />
        break

      case 'circle':
        if (
          expressionType === 'match' ||
          expressionType === 'case' ||
          styleLayer.color
        ) {
          legendComponent = <LegendCircle styleLayer={styleLayer} />
        } else if (expressionType === 'interpolate') {
          // TODO make legend continuous for circles.
          legendComponent = (
            <LegendContinuousBar
              firstLabel={styleLayer.firstLabel}
              lastLabel={styleLayer.lastLabel}
              styleLayer={styleLayer}
              average={styleLayer.average}
            />
          )
        }
        break

      case 'line':
        legendComponent = <LegendLine styleLayer={styleLayer} />
        break

      default:
        // default == FILL
        if (expressionType === 'interpolate') {
          legendComponent = (
            <LegendContinuousBar
              firstLabel={styleLayer.firstLabel}
              lastLabel={styleLayer.lastLabel}
              styleLayer={styleLayer}
              average={styleLayer.average}
            />
          )
        } else if (expressionType === 'match' || expressionType === 'case') {
          legendComponent = (styleLayer as any).filterValues ? (
            <LegendQualitativeFilter
              id={dataLayer.id}
              styleLayer={styleLayer}
              filterValues={styleLayer.filterValues}
              filterNames={styleLayer.filterNames}
              attrName={styleLayer.filterAttrb}
            />
          ) : (
            <LegendQualitative styleLayer={styleLayer} />
          )
        } else if (expressionType === 'step') {
          legendComponent = <LegendQualitative styleLayer={styleLayer} />
        }
        break
    }
  }

  const hasLegendInfo = styleLayer.unit || styleLayer.scale

  return (
    <>
      {legendComponent && (
        <>
          {hasLegendInfo && (
            <Styled.LegendInfo key={`legendinfo ${dataLayer?.id}`}>
              <p>{styleLayer.unit}</p>
              <small>{styleLayer.scale}</small>
            </Styled.LegendInfo>
          )}
          <Styled.ContentContainer>{legendComponent}</Styled.ContentContainer>
        </>
      )}
    </>
  )
}

export default LegendContent
