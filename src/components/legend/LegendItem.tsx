// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { FunctionComponent } from 'react'

interface ItemProps {
  name: string
  color: string
  opacity?: number
}

const LegendClassificationItem: FunctionComponent<ItemProps> = ({
  name,
  color,
  opacity = 1,
}) => {
  return (
    <div key={name} className="legendItem">
      <div
        key={name}
        data-testid="color"
        style={{ backgroundColor: color, opacity: opacity }}
      />
      <p>{name}</p>
    </div>
  )
}

export default LegendClassificationItem
