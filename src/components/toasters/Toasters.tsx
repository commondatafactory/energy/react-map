// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC } from 'react'
import { createPortal } from 'react-dom'
import { Toaster } from './Toaster'

type ToastersProps = {
  setToasters: any
  toasters: any
}

export const Toasters: FC<ToastersProps> = ({ toasters, setToasters }) => {
  return (
    <>
      {typeof window !== 'undefined' &&
        createPortal(
          <>
            {toasters.map((toaster) => (
              <Toaster
                key={toaster.nth}
                setToasters={setToasters}
                {...toaster}
              />
            ))}
          </>,
          window.document.body
        )}
    </>
  )
}
