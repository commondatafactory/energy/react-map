// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC } from 'react'
import { Icon } from '@commonground/design-system'

import { parseDate } from '../../../utils/parse-date'
import CalendarIcon from '../../../../public/images/calendar.svg'
import RefreshIcon from '../../../../public/images/refresh.svg'
import ExternalIcon from '../../../../public/images/external-link-2.svg'
import SourceIcon from '../../../../public/images/road-map-line.svg'
import type { DataLayerProps } from '../../../data/data-layers'
import * as Styled from './Layer.styled'

interface LayerMetaDataProps {
  dataLayer: Partial<DataLayerProps>
}

export const LayerMetaData: FC<LayerMetaDataProps> = ({ dataLayer }) => (
  <>
    <Styled.MetaDataItem>
      <Styled.MetaDataTitle>
        {Object.keys(dataLayer.sourceDescription).length > 1
          ? 'Bronnen'
          : 'Bron'}
      </Styled.MetaDataTitle>
      <>
        {Object.keys(dataLayer.sourceDescription).map((source, i) =>
          !dataLayer?.sourceDescription?.[source] ? (
            <React.Fragment key={i} />
          ) : (
            <div key={i}>
              <Styled.SourceTitle>
                {dataLayer.sourceDescription[source].url ? (
                  <Styled.ExternalLink
                    href={dataLayer.sourceDescription[source].url}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Icon as={ExternalIcon} size="small" />
                    {source}
                  </Styled.ExternalLink>
                ) : (
                  <Styled.DateWrapper>
                    <Icon as={SourceIcon} size="small" />
                    {source}
                  </Styled.DateWrapper>
                )}
              </Styled.SourceTitle>
              {dataLayer.sourceDescription[source].updateFrequency && (
                <Styled.SourceDate>
                  {dataLayer.sourceDescription[source]?.updateFrequency && (
                    <Styled.DateWrapper>
                      <Icon as={RefreshIcon} size="small" />
                      {dataLayer.sourceDescription[source]?.updateFrequency}
                    </Styled.DateWrapper>
                  )}
                  {dataLayer.sourceDescription[source]?.referenceDate && (
                    <Styled.DateWrapper>
                      <Icon as={CalendarIcon} size="small" />
                      {parseDate(
                        dataLayer.sourceDescription[source].referenceDate
                      )}
                    </Styled.DateWrapper>
                  )}
                </Styled.SourceDate>
              )}
            </div>
          )
        )}
      </>
    </Styled.MetaDataItem>
  </>
)
