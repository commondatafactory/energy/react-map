// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useContext } from 'react'

import type { DataLayerProps } from '../../../data/data-layers'

import { useIsMobile } from '../../../hooks/use-is-mobile'
import { MapContext } from '../../../providers/map-provider'
import { TabProps } from '../../../providers/tab-layer-provider'
import { PolygonIcon } from './PolygonIcon'

import * as Styled from './Layer.styled'
import { LayerMetaData } from './LayerMetaData'

interface LayerDetailsProps {
  dataLayer: Partial<DataLayerProps>
}

export const LayerDetails: FC<LayerDetailsProps> = ({ dataLayer }) => {
  const { setAnalyzeMode } = useContext(MapContext)
  const { isMobile } = useIsMobile()

  const handleClick = () => {
    setAnalyzeMode(dataLayer.drawType || null)
  }

  return (
    <Styled.LayerDetails>
      <p dangerouslySetInnerHTML={{ __html: dataLayer?.desc }} />

      {!isMobile && (
        <>
          {dataLayer?.drawType === 'count' && (
            <Styled.CustomButton
              variant="link"
              size="small"
              onClick={handleClick}
            >
              <PolygonIcon inline />
              Gebied analyseren
            </Styled.CustomButton>
          )}
          <LayerMetaData dataLayer={dataLayer} />
        </>
      )}
    </Styled.LayerDetails>
  )
}
