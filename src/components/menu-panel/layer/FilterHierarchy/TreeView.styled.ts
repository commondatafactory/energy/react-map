// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const FilterOptions = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  gap: var(--spacing03);
  margin-top: var(--spacing03);
  margin-bottom: var(--spacing03);
  line-height: 1.5rem;

  & p {
    font-size: var(--fontSizeSmall);
    padding: var(--spacing02);
  }
`

export const FilterOptionsTag = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  justify-items: center;
  width: fit-content;
  background-color: var(--colorPaletteGray300);
  border-radius: var(--spacing02);
`

export const Tree = styled.div`
  display: flex;
  flex-direction: column;
  font-size: var(--fontSizeSmall);
  margin-top: var(--spacing01);
  margin: 0;
  padding: 0;
  width: 100%;
  margin-bottom: var(--spacing04);

  > .first {
    border-top: 1px var(--colorPaletteGray300) solid;
    padding-top: var(--spacing02);
    padding-bottom: var(--spacing02);
  }
`

export const TreeNode = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  line-height: var(--fontSizeMedium);
  width: 100%;
  position: relative;
  cursor: initial;

  &.active {
    background-color: var(--colorInfoLight);

    & li svg {
      fill: var(--colorInfo);
    }
  }

  &.hasChildren:hover {
    background-color: var(--colorBrand2);
    cursor: pointer;
  }

  & li {
    cursor: pointer;
    list-style-type: none;
    margin: calc(0 - var(--spacing01));
    padding: var(--spacing01);
  }

  > svg {
    fill: var(--colorPaletteGray900);
    transform: rotate(-90deg);
  }

  svg.collapsed {
    transform: rotate(0deg);
  }
`

export const ChildrenTree = styled.div`
  margin-left: 1rem;
`

export const TreeNodeTitle = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  min-height: var(--spacing05);
  padding: var(--spacing01);
  text-transform: lowercase;
  width: 100%;

  ::first-letter {
    text-transform: uppercase;
  }

  p {
    margin: 0;
  }

  p::first-letter {
    text-transform: uppercase;
  }

  div {
    border: none;
  }

  .treeNodeCode {
    font-weight: bold;
    background-color: #ece2cf;
    padding: 3px;
    margin: 3px;
    color: var(--colorPaletteGray600);
  }

  .searchText {
    font-weight: bold;
    color: var(--colorTextLink);
  }
`

export const IconWrapper = styled.div`
  height: 100%;
  padding: 6px 8px 6px 8px;
  margin-right: -4px;

  > svg {
    width: 16px;
  }
`

export const MoreInformationWrapper = styled.div`
  margin-left: auto;
`
