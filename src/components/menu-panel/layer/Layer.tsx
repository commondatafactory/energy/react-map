// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FC } from 'react'

import useUser from '../../../hooks/use-user'
import { setURLParams } from '../../../utils/url-params'
import { useAccess } from '../../../hooks/use-access'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import { DataLayerProps } from '../../../data/data-layers'
import RadioButton from '../../asset-components/layer-radio-button/LayerRadioButton'
import { LayerDetails } from './LayerDetails'
import { LockIcon } from './LockIcon'
import { PolygonIcon } from './PolygonIcon'
import { SublayerDetails } from './SublayerDetails'
import { LayerAuth } from './LayerAuth'

import * as Styled from './Layer.styled'

interface MenuLayerProps {
  dataLayer: Partial<DataLayerProps>
  handleClick: (dataLayer: Partial<DataLayerProps>) => void
  isActive: boolean
}

const Layer: FC<MenuLayerProps> = ({ dataLayer, handleClick, isActive }) => {
  const { isMobile } = useIsMobile()
  const { isLoggedIn, data } = useUser()
  const { hasAccess } = useAccess()
  const { setActiveDataLayers, setActiveSublayerName, activeSublayerName } =
    useTabsLayers()

  const handleSublayerClick = (sublayer: Partial<DataLayerProps>) => {
    setURLParams('sublayer', sublayer.id)
    setActiveSublayerName(sublayer.name)

    // FIX: Just like in TabLayerProvider this is a hack. Overriding the parent
    // layer's style layers with a sublayer's one is not the way to go.
    const newDataLayer = { ...dataLayer }
    newDataLayer.styleLayers = sublayer.styleLayers
    delete newDataLayer.sublayers

    setActiveDataLayers((activeDataLayers) => {
      const newActiveDataLayers = [...activeDataLayers]
      newActiveDataLayers[0] = newDataLayer
      return newActiveDataLayers
    })
  }

  const isLayerAccessible = hasAccess(dataLayer, data)
  const className = isActive ? 'active' : ''

  return (
    <Styled.Layer key={dataLayer.id} className={className}>
      <Styled.LayerHeading
        className={className}
        onClick={() => handleClick(dataLayer)}
      >
        <h3>{dataLayer.name}</h3>
        <div className={className}>
          {dataLayer.drawType === 'count' && !isMobile && <PolygonIcon />}

          {isLayerAccessible ? (
            <RadioButton
              value={dataLayer.id}
              checked={isActive}
              name={dataLayer.name}
            />
          ) : (
            <LockIcon />
          )}
        </div>
      </Styled.LayerHeading>

      {isActive && (
        <Styled.LayerContent>
          {isLayerAccessible ? (
            dataLayer.sublayers ? (
              <SublayerDetails
                dataLayer={dataLayer}
                sublayers={dataLayer.sublayers}
                handleSublayerClick={handleSublayerClick}
                activeSublayerName={activeSublayerName}
              />
            ) : (
              <LayerDetails dataLayer={dataLayer} />
            )
          ) : (
            <LayerAuth description={dataLayer.desc} isLoggedIn={isLoggedIn} />
          )}
        </Styled.LayerContent>
      )}
    </Styled.Layer>
  )
}

export default Layer
