// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FunctionComponent } from 'react'
import type { DataLayerProps } from '../../../data/data-layers'
import { RadioButton } from '../../asset-components/radio-button/RadioButton'
import { useIsMobile } from '../../../hooks/use-is-mobile'
import { PolygonIcon } from './PolygonIcon'
import * as Styled from './Layer.styled'
import { LayerMetaData } from './LayerMetaData'

interface LayerDetailsProps {
  dataLayer: Partial<DataLayerProps>
  sublayers: Partial<DataLayerProps['sublayers']>
  handleSublayerClick: any
  activeSublayerName: string
}
export const SublayerDetails: FunctionComponent<LayerDetailsProps> = ({
  dataLayer,
  sublayers,
  handleSublayerClick,
  activeSublayerName,
}): JSX.Element => {
  const handleClick = () => {
    // TODO: if count on sublayers
  }
  const { isMobile } = useIsMobile()

  return (
    <Styled.LayerDetails>
      <p dangerouslySetInnerHTML={{ __html: dataLayer?.desc }} />
      {dataLayer?.drawType && dataLayer?.drawType === 'count' && (
        <Styled.CustomButton variant="link" size="small" onClick={handleClick}>
          <PolygonIcon inline />
          Gebied analyseren
        </Styled.CustomButton>
      )}
      <Styled.SublayerDetails>
        {dataLayer.name !== 'Bereidheid Energietransitiemaatregelen' && (
          <p>Kies een andere weergave: </p>
        )}
        {sublayers?.map((sublayer) => {
          return (
            <div
              key={sublayer.name}
              onClick={() => handleSublayerClick(sublayer)}
              style={{ flexDirection: 'column' }}
            >
              <div>
                <RadioButton
                  value={sublayer.name}
                  checked={sublayer.name === activeSublayerName}
                  onChange={() => ({})}
                  name={sublayer.name}
                />

                <span
                  style={{ fontWeight: '600' }}
                  key={'label' + sublayer.name}
                >
                  {sublayer.name}
                </span>
              </div>
              {sublayer.desc ? (
                <p key={'desc' + sublayer.name}>{sublayer.desc}</p>
              ) : (
                ''
              )}
            </div>
          )
        })}
      </Styled.SublayerDetails>
      {!isMobile && <LayerMetaData dataLayer={dataLayer} />}
    </Styled.LayerDetails>
  )
}
