// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { FC, useEffect } from 'react'

import { DataLayerProps, staticBuildingsLayer } from '../../../data/data-layers'
import { setURLParams } from '../../../utils/url-params'
import { useAccess } from '../../../hooks/use-access'
import { useTabsLayers } from '../../../hooks/use-tabs-layers'
import Layer from '../layer'
import useUser from '../../../hooks/use-user'
import { LayerSbiCodes } from '../layer/LayerSbiCodes'
import Tab from './Tab'

import * as Styled from './Tabs.styled'

const Tabs: FC = () => {
  const {
    activeDataLayers,
    isActiveDataLayer,
    activeTab,
    setActiveDataLayers,
    setActiveSublayerName,
    visibleLayers,
    visibleTabs,
  } = useTabsLayers()
  const { hasAccess } = useAccess()
  const { data } = useUser()

  const handleLayerActivation = (dataLayer: Partial<DataLayerProps>) => {
    const isActive = isActiveDataLayer(dataLayer)

    if (!isActive) {
      setURLParams({
        tab: activeTab.id,
        layer: dataLayer.id,
        sublayer: dataLayer.sublayers ? dataLayer.sublayers[0].id : null,
      })
    } else {
      if (
        dataLayer.layerType === 'sbi-codes' &&
        visibleLayers[0]?.layerType === 'sbi-codes'
      ) {
        return
      }

      setURLParams({ layer: null, sublayer: null })
    }

    const newActiveDataLayers = [...activeDataLayers]

    if (!isActive) {
      if (dataLayer.sublayers?.length) {
        // TODO: sublayers deleten is geen best practice
        const newDataLayer = { ...dataLayer.sublayers[0], ...dataLayer }
        delete newDataLayer.sublayers
        newActiveDataLayers[0] = newDataLayer
        setActiveSublayerName(dataLayer.sublayers[0].name)
      } else {
        newActiveDataLayers[0] = dataLayer
      }
    } else {
      newActiveDataLayers[0] = staticBuildingsLayer
    }

    setActiveDataLayers(newActiveDataLayers)

    if (typeof window !== 'undefined') {
      ;(window as any).goatcounter?.count({
        path: (path) => `${dataLayer.name} path: ${path}`,
        event: true,
      })
    }
  }

  useEffect(() => {
    if (
      visibleLayers[0]?.layerType === 'sbi-codes' &&
      activeDataLayers[0]?.layerType !== 'sbi-codes'
    ) {
      handleLayerActivation(visibleLayers[0])
    }
  }, [activeTab?.id, visibleLayers])

  return (
    <Styled.Container>
      {visibleTabs.map((tab) => (
        <Tab
          isActive={tab.id === activeTab?.id}
          iconSlug={tab.iconSlug}
          tab={tab}
          key={tab.id}
          slug={tab.id}
          title={tab.title}
          hasActiveLayers={!!(activeDataLayers?.[0]?.id !== 'layer0')}
        >
          <p
            className="styled-description"
            dangerouslySetInnerHTML={{ __html: tab.description }}
          />

          {visibleLayers[0]?.layerType === 'sbi-codes' && (
            <LayerSbiCodes
              key={visibleLayers[0].name}
              dataLayer={visibleLayers[0]}
            />
          )}

          {!visibleLayers[0]?.layerType && (
            <>
              <h3 className="headingSmall">Beschikbare kaartlagen:</h3>
              {visibleLayers.map(
                (dataLayer) =>
                  dataLayer.tab === tab.id && (
                    <Layer
                      key={dataLayer.name}
                      dataLayer={dataLayer}
                      isActive={isActiveDataLayer(dataLayer)}
                      handleClick={handleLayerActivation}
                    />
                  )
              )}
            </>
          )}
        </Tab>
      ))}
    </Styled.Container>
  )
}

export default Tabs
