// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC } from 'react'
import { IconFlippingChevron } from '@commonground/design-system'

import { useIsMobile } from '../../hooks/use-is-mobile'
import { Authentication } from '../authentication'
import { LocationNavBar } from './location-nav-bar/LocationNavBar'
import Header from './header/Header'
import Tabs from './tabs/Tabs'

import * as Styled from './MenuPanel.styled'
import MenuPanelFooter from './footer/Footer'

export type MenuPanelProps = {
  isCollapsed?: boolean
  setIsCollapsed?: React.Dispatch<React.SetStateAction<boolean>>
}

const MenuPanel: FC<MenuPanelProps> = ({
  isCollapsed = false,
  setIsCollapsed,
}) => {
  const { isMobile } = useIsMobile()

  return (
    <>
      <Styled.Container>
        {process.env.NEXT_PUBLIC_CONFIG !== 'dego' && <Authentication />}
        <Header />
        <LocationNavBar />
        {isMobile && (
          <Styled.CollapseButton
            $isCollapsed={isCollapsed}
            onClick={() => setIsCollapsed(!isCollapsed)}
          >
            <IconFlippingChevron />
          </Styled.CollapseButton>
        )}
        {(!isMobile || !isCollapsed) && <Tabs />}
      </Styled.Container>

      <MenuPanelFooter />
    </>
  )
}

export default MenuPanel
