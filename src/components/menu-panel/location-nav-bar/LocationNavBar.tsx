// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Input from '@commonground/design-system/dist/components/Form/TextInput/Input'
import { setURLParams } from '../../../utils/url-params'
import {
  fetchLocations,
  lookupLocation,
} from '../../../services/map/lookup-location'
import { useIsMobile } from '../../../hooks/use-is-mobile'

import SearchIcon from '../../../../public/images/search.svg'
import ResetIcon from '../../../../public/images/close.svg'
import { useMap } from '../../../hooks/use-map'
import * as Styled from './LocationNavBar.styled'

export const LocationNavBar = () => {
  const router = useRouter()
  const { query } = router || {}
  const queryParam = String(query?.query || '')

  const [displayName, setDisplayName] = useState(queryParam)
  const [resultList, setResultList] = useState([])
  const [searchTerm, setSearchTerm] = useState('')
  const [cursorPosition, setCursorPosition] = useState(-1)

  const { setSearchGeometry } = useMap()
  const { isMobile } = useIsMobile()

  const handleClear = () => {
    setSearchGeometry(null)
    setDisplayName('')
    setResultList([])
    setURLParams('query', null)
  }

  const handleSearch = (newSearchTerm) => {
    setSearchTerm(newSearchTerm)
  }

  const clearList = useCallback(() => {
    setResultList([])
    setSearchTerm('')
    setCursorPosition(-1)
  }, [setCursorPosition, setResultList])

  const handleChange = async (e) => {
    const newSearchTerm = e.target.value
    setSearchTerm(newSearchTerm)
    if (newSearchTerm.trim() === '') {
      clearList()
    }
    handleSearch(newSearchTerm)

    setDisplayName(e.target.value || '')

    if (!e.target.value?.length) {
      handleClear()
    }

    if (e.target.value.length >= 2 && e.target.value.trim() !== '') {
      const maxResults = isMobile ? 5 : 8
      const locations = await fetchLocations(
        e.target.value,
        `rows=${maxResults}&`
      )

      if (!locations?.response.docs) {
        return
        //   showToast({
        //     title: 'Service om locaties op te halen is momenteel offline',
        //     variant: 'error',
        //   })
        //   return
      }

      setResultList(locations.response.docs)
    }
  }

  const isSearchEmpty = searchTerm.trim() === ''
  const noResultsFound = resultList.length === 0 && !isSearchEmpty
  // const hasResults = resultList.length !== 0 && isSearchEmpty

  const handleLookup = async (item) => {
    const geocodeResult = await lookupLocation(item)

    if (!geocodeResult) {
      console.warn('Suggested location not found')
      return
    }
    setSearchGeometry(geocodeResult)
    clearList()
  }

  const onHandleClick = (item, e) => {
    e.preventDefault()
    setDisplayName(item.weergavenaam || '')
    setURLParams('query', item.weergavenaam || '')
    handleLookup(item)
    clearList()
  }

  const handleKeyDown = useCallback(
    async (e) => {
      if (e.key === 'Enter') {
        e.preventDefault()
      }
      if (resultList.length > 0) {
        switch (e.key) {
          case 'Enter':
            if (cursorPosition >= 0) {
              const item = resultList[cursorPosition]
              onHandleClick(item, e)
              clearList()
              e.target.blur()
            }
            break
          case 'ArrowDown':
            if (resultList.length - 1 > cursorPosition) {
              setCursorPosition(cursorPosition + 1)
            }
            break
          case 'ArrowUp':
            if (cursorPosition !== 0) {
              setCursorPosition(cursorPosition - 1)
            }
            break
          case 'Backspace':
            clearList()
            break
          case 'Escape':
            clearList()
            break
          default:
            break
        }
      }
    },
    [clearList, cursorPosition, resultList]
  )

  // Alleen voor router url
  useEffect(() => {
    setDisplayName(queryParam)
  }, [queryParam])

  useEffect(() => {
    if (queryParam) {
      ;(async () => {
        const locations = await fetchLocations(queryParam)

        if (!location) {
          console.warn('Suggested location query not found')
          return
        }
        await handleLookup(locations?.response.docs?.[0])
      })()
    }
  }, [query])

  return (
    <Styled.NavBarContainer>
      <Input
        data-cy="location-search-input"
        placeholder="Zoek uw locatie hier"
        type="text"
        value={displayName}
        onChange={handleChange}
        onFocus={handleChange}
        onKeyDown={handleKeyDown}
        autoComplete="off"
        autoFocus="off"
        icon={SearchIcon}
        iconAtEnd={ResetIcon}
        spellCheck="false"
        handleIconAtEnd={handleClear}
        style={{ width: '100%' }}
      />

      {noResultsFound ? (
        <Styled.ResultList>
          <Styled.ResultListItem>
            Geen resultaten gevonden
          </Styled.ResultListItem>
        </Styled.ResultList>
      ) : (
        resultList.length > 0 && (
          <Styled.ResultList data-cy="location-search-results">
            {resultList.map((item, i) => (
              <Styled.ResultListItem
                className={i === cursorPosition ? 'focus' : ''}
                key={item.id}
                value={item.id}
                onClick={(e) => onHandleClick(item, e)}
              >
                <Styled.ItemType>{item.type}</Styled.ItemType>{' '}
                {item.weergavenaam}
              </Styled.ResultListItem>
            ))}
          </Styled.ResultList>
        )
      )}
    </Styled.NavBarContainer>
  )
}
