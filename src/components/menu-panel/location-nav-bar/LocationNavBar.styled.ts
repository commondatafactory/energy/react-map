// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const NavBarContainer = styled.div`
  position: relative;
  margin: var(--spacing05) var(--spacing05) auto var(--spacing05);
  /* z-index: 100; */

  svg {
    color: var(--colorPaletteGray500);
  }

  .invalid {
    color: var(--colorError);
  }
`

export const ResultList = styled.ul`
  max-height: fit-content;
  list-style: none;
  margin: 0;
  padding: 0;
  border-width: 1px;
  /* z-index: 10; */
  position: absolute;
  shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  background-color: var(--colorBackground);
  border-color: var(--colorPaletteGray500);
  width: 100%;
  margin-top: var(--spacing02);
  overflow: hidden;
  overflow-y: hidden;
`

export const ResultListItem = styled.li`
  border-width: 0px;
  line-height: var(--lineHeightText);
  padding-top: var(--spacing04);
  padding-bottom: var(--spacing04);
  padding-left: var(--spacing05);
  padding-right: var(--spacing05);
  border-color: var(--colorPaletteGray200);
  cursor: pointer;
  width: 100%;
  min-height: 3rem;
  border-bottom-width: 1px;

  &:hover {
    background-color: var(--colorPaletteGray200);
  }

  &.focus {
    background-color: var(--colorPaletteGray200);
  }
`

export const ItemType = styled.span`
  font-weight: bold;
  background-color: #ece2cf;
  padding: 3px;
`
