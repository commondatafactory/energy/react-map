// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { css } from 'styled-components'

export const NlxDarkThemeVariables = css`
  :root {
    --colorPaletteGray50: #fafafa;
    --colorPaletteGray100: #f5f5f5;
    --colorPaletteGray200: #eeeeee;
    --colorPaletteGray300: #e0e0e0;
    --colorPaletteGray400: #bdbdbd;
    --colorPaletteGray500: #9e9e9e;
    --colorPaletteGray600: #757575;
    --colorPaletteGray700: #616161;
    --colorPaletteGray800: #424242;
    --colorPaletteGray900: #212121;
    --colorBrand1: #ffbc2c;
    --colorBrand2: #33373e;
    --colorBrand3: #474e57;
    --colorBrand4: #1ea1d5;
    --colorInfo: #1ea1d5;
    --colorInfoLight: #6ca9c6;
    --colorWarning: #ffbc2c;
    --colorWarningLight: #ffd680;
    --colorError: #fe636d;
    --colorErrorLight: #f67f8d;
    --colorSuccess: #75a25b;
    --colorSuccessLight: #88b76d;
    --colorBackground: var(--colorPaletteGray900);
    --colorBackgroundAlt: #313131;
    --colorFocus: #1ea1d5;
    /* // Text */
    --colorText: var(--colorPaletteGray300);
    --colorTextInverse: var(--colorPaletteGray900);
    --colorTextLabel: var(--colorPaletteGray500);
    --colorTextDisabled: rgba(255, 255, 255, 0.3);
    --colorTextLink: var(--colorBrand4);
    --colorTextLinkHover: #82cfe8;
    --colorTextLinkDisabled: rgba(255, 255, 255, 0.2);
    /* // Alert */
    --colorAlertInfo: var(--colorInfo);
    --colorAlertInfoBackground: #434950;
    --colorAlertWarning: var(--colorWarning);
    --colorAlertWarningBackground: #4d4941;
    --colorAlertError: var(--colorError);
    --colorAlertErrorBackground: #4f4344;
    --colorAlertSuccess: var(--colorSuccess);
    --colorAlertSuccessBackground: #454b42;
    /* // Buttons */
    --colorBackgroundButtonPrimary: var(--colorBrand1);
    --colorBackgroundButtonPrimaryHover: #ffd06b;
    --colorBackgroundButtonPrimaryDisabled: rgba(255, 255, 255, 0.1);
    --colorTextButtonPrimary: var(--colorTextInverse);
    --colorTextButtonPrimaryDisabled: var(--colorTextDisabled);
    --colorBackgroundButtonSecondary: rgba(255, 255, 255, 0.3);
    --colorBackgroundButtonSecondaryHover: rgba(255, 255, 255, 0.4);
    --colorBackgroundButtonSecondaryDisabled: rgba(255, 255, 255, 0.1);
    --colorBackgroundButtonSecondarySelected: var(--colorBrand4);
    --colorTextButtonSecondary: var(--colorPaletteGray100);
    --colorTextButtonSecondaryDisabled: var(--colorTextDisabled);
    --colorTextButtonSecondarySelected: var(--colorTextInverse);
    --colorBackgroundButtonDanger: var(--colorAlertError);
    --colorBackgroundButtonDangerHover: #f46b7a;
    --colorBackgroundButtonDangerDisabled: rgba(255, 255, 255, 0.1);
    --colorTextButtonDanger: var(--colorTextInverse);
    --colorTextButtonDangerDisabled: rgba(255, 255, 255, 0.3);
    /* // Input */
    --colorBackgroundInput: rgba(255, 255, 255, 0.1);
    --colorBackgroundInputDisabled: rgba(255, 255, 255, 0.05);
    --colorBorderInput: transparent;
    --colorBorderInputDisabled: transparent;
    --colorBorderInputError: var(--colorError);
    --colorBorderInputFocus: var(--colorFocus);
    --colorTextInputLabel: var(--colorPaletteGray400);
    --colorTextInputLabelDisabled: rgba(255, 255, 255, 0.3);
    --colorTextInputPlaceholder: rgba(255, 255, 255, 0.6);
    --colorTextInputDisabled: var(--colorTextDisabled);
    --colorTextInputError: var(--colorError);
    /* // Checkbox & Radio */
    --colorBackgroundChoiceSelected: var(--colorPaletteGray300);
    --colorBackgroundChoiceDisabled: rgba(255, 255, 255, 0.1);
    --colorBorderChoice: var(--colorPaletteGray500);
    --colorBorderChoiceFocus: var(--colorFocus);
    /* // Dropdown, Select, Popover */
    --colorBackgroundDropdown: var(--colorBackgroundInput);
    --colorBackgroundDropdownHover: #515151;
    --colorBackgroundDropdownActive: var(--colorPaletteGray600);
    --colorBackgroundDropdownSelected: var(--colorPaletteGray700);
    --colorBorderDropdownFocus: var(--colorFocus);
    /* // Switch */
    --colorBackgroundSwitch: var(--colorPaletteGray600);
    --colorBackgroundSwitchChecked: var(--colorBrand4);
    --colorBackgroundSwitchDisabled: rgba(255, 255, 255, 0.1);
    --colorBackgroundSwitchIndicatorDisabled: rgba(255, 255, 255, 0.2);
    /* // Drawer */
    --colorBackgroundDrawer: var(--colorBackgroundAlt);
    /* // Table */
    --colorBorderTable: var(--colorPaletteGray800);
    --colorBackgroundTableHover: rgba(255, 255, 255, 0.1);
    --colorBorderTableFocus: var(--colorFocus);
    /* Spacings */
    --xs: 0;
    --sm: 576;
    --md: 768;
    --lg: 992;
    --baseFontSize: 16px;
    --spacing01: 0.125rem;
    --spacing02: 0.25rem;
    --spacing03: 0.5rem;
    --spacing04: 0.75rem;
    --spacing05: 1rem;
    --spacing06: 1.5rem;
    --spacing07: 2rem;
    --spacing08: 2.5rem;
    --spacing09: 3rem;
    --spacing10: 4rem;
    --spacing11: 5rem;
    --spacing12: 6rem;
    --lineHeightText: 150%;
    --lineHeightHeading: 125%;
    --fontWeightRegular: 500;
    --fontWeightSemiBold: 600;
    --fontWeightBold: 700;
    --fontSizeSmall: 0.875rem;
    --fontSizeMedium: 1rem;
    --fontSizeLarge: 1.125rem;
    --fontSizeXLarge: 1.5rem;
    --fontSizeXXLarge: 2rem;
  }

  /* 
#E0E4EA
#212121
#E0E4EA
#F8F9FC
#ffbc2c
#F8F9FC
#474E57
#212121
#FFBC2C
#F8F9FC
#212121
#262D30
#474E57
#fff
#E0E4EA
#474E57

#fafafa
#E0E0E0
#E0E0E0
#fafafa
#BDBDBD
#BDBDBD
#fafafa
#E8DDD8
#BDBDBD
#BDBDBD
#E8DDD8
#E8DDD8
#EEEEEE
#E8DDD8
#E8DDD8
#C7D5B7
#E8DDD8
#C7D5B7
#C7D5B7
#fafafa
#E8DDD8
#F0F1EE
#fafafa
#C7D5B7
#EAEEE7
#F0F1EE
#F3E9D1
#fafafa
#B0CFDE
#B0CFDE
#B0CFDE
#E0E0E0
#BDBDBD
#FE7560
#FE8753
#9E9E9E
#BDBDBD
#FCC653
#F8D07A
#F1E4C7
#F1E4C7
#F1E4C7
#EEEEEE
#EEEEEE
#9E9E9E
#9E9E9E
#BDBDBD
#fafafa
#474E57
#212121
#FAFAFA
#fff
#FE636D
#616161
#757575
#212121
#FAFAFA
#39870C
#454B42
#FAFAFA
#616161
#212121
#FAFAFA
#424242
#212121
#e3e3e3 */
`
