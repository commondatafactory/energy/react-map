// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import * as d3 from 'd3'
import {
  ScaleDiverging,
  ScaleIdentity,
  ScaleLinear,
  ScaleOrdinal,
  ScaleSequential,
  ScaleTime,
} from 'd3'

export interface ColorProps {
  type?: 'ordinal' | 'sequentional' | 'diverging' | 'time' | 'linear' | 'log'
  domain?: number[]
  ordinalDomain?: string[]
  color?: string
  colorRange?: string[]
  ticks?: number
  tickValues?: string[]
  firstTick?: string
  lastTick?: string
}

export type ColorScale =
  | ScaleLinear<string | number, string, unknown>
  | ScaleOrdinal<string | number, unknown>
  | ScaleSequential<string>
  | ScaleDiverging<string>
  | ScaleTime<number, number>

export function createColorScale(properties: ColorProps): ColorScale {
  // type of scale
  switch (properties.type) {
    case 'ordinal':
      return d3
        .scaleOrdinal()
        .domain(properties.ordinalDomain)
        .range(properties.colorRange)
    case 'linear':
      return d3
        .scaleLinear<string>()
        .range(properties.colorRange || properties.color)
        .domain(properties.domain)
        .interpolate(d3.interpolateRgb)
    case 'log':
      return d3
        .scaleLinear<string>()
        .range(properties.colorRange || properties.color)
        .domain(properties.domain)
        .interpolate(d3.interpolateRgb)
    // return d3
    //   .scaleLog()
    //   .range(properties.colorRange || properties.color)
    //   .domain(properties.domain)
    //   .interpolate(d3.interpolateRgb)
    case 'sequentional':
      // domain exactly 2 arg
      switch (properties.color) {
        case 'cividis':
          return d3
            .scaleSequential(d3.interpolateCividis)
            .domain(properties.domain)
        case 'red':
          return d3
            .scaleSequential((t: number) => d3.interpolateYlOrRd(t))
            .domain(properties.domain)
        case 'purpleInvert':
          return d3
            .scaleDiverging((t: number) => d3.interpolatePuOr(1 - t))
            .domain(properties.domain) // invert colors
        case 'log':
          return d3
            .scaleSequentialLog(d3.interpolateYlGnBu)
            .domain(properties.domain)
        default:
          return d3
            .scaleSequentialLog(d3.interpolateYlGnBu)
            .domain(properties.domain)
      }
    case 'diverging':
      // domain exactly 3 arg

      switch (properties.color) {
        case 'red':
          return d3
            .scaleDiverging((t: number) => d3.interpolateRdYlBu(1 - t))
            .domain(properties.domain) // invert colors
        case 'purple':
          return d3
            .scaleDiverging((t: number) => d3.interpolatePuOr(1 - t))
            .domain(properties.domain) // invert colors
        case 'blue':
          return d3
            .scaleDiverging((t: number) => d3.interpolateBrBG(t))
            .domain(properties.domain)
        default:
          return d3
            .scaleDiverging((t: number) => d3.interpolateInferno(1 - t))
            .domain(properties.domain)
      }
    case 'time':
      return d3.scaleTime().domain(properties.domain)
    default:
      break
  }
}
