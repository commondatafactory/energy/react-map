// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { defaultTheme } from '@commonground/design-system'

const tokens = {
  ...defaultTheme.tokens,
}

const theme = {
  ...defaultTheme,
  tokens,
  breakpoints: {
    mobile: 'min-width: 568px',
    tablet: 'min-width: 900px',
    medium: 'min-width: 1401px',
    large: 'min-width: 1920px',
  },
}

export { theme }
