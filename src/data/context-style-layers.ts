// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  CircleLayerSpecification,
  LayerSpecification,
  LineLayerSpecification,
  SymbolLayerSpecification,
} from 'maplibre-gl'

export const getBorderLayer = (id: string): LineLayerSpecification => {
  return {
    id: id,
    source: 'cbsarmoede',
    'source-layer': 'admin',
    type: 'line',
    layout: {
      visibility: 'none',
      'line-cap': 'round',
      'line-join': 'round',
    },
    paint: {
      'line-color': [
        'interpolate',
        ['linear'],
        ['zoom'],
        8,
        '#ba85bf',
        15,
        [
          'match',
          ['get', 'hierarchy'],
          1,
          '#4A354C ',
          2,
          '#9b55a2',
          3,
          '#ba85bf',
          '#00eeff',
        ],
      ],
      'line-width': [
        'interpolate',
        ['linear'],
        ['zoom'],
        8,
        ['match', ['get', 'hierarchy'], 1, 0.75, 0],
        10,
        ['match', ['get', 'hierarchy'], 1, 4, 2, 0.75, 0],
        13,
        ['match', ['get', 'hierarchy'], 1, 8, 2, 4, 3, 1.5, 0],
        16,
        ['match', ['get', 'hierarchy'], 1, 18, 2, 10, 3, 8, 0],
        22,
        ['match', ['get', 'hierarchy'], 1, 22, 2, 18, 3, 10, 0],
      ],
      'line-blur': 0.5,
      'line-opacity': [
        'interpolate',
        ['linear'],
        ['zoom'],
        8,
        1,
        16,
        ['match', ['get', 'hierarchy'], 1, 0.6, 2, 0.7, 3, 0.8, 1],
      ],
    },
  }
}

export const getSearchGeometryPolygonLayer = (
  id: string
): LineLayerSpecification => {
  return {
    id: id,
    type: 'line',
    source: 'searchGeometry',
    layout: {
      'line-join': 'round',
      'line-cap': 'round',
    },
    filter: ['any', ['==', '$type', 'Polygon'], ['==', '$type', 'LineString']],
    paint: {
      'line-width': [
        'interpolate',
        ['exponential', 1.2],
        ['zoom'],
        0,
        0,
        8,
        1,
        11,
        4,
        16,
        10,
      ],
      'line-color': '#474E57',
      'line-dasharray': [1, 2],
      'line-opacity': 0.85,
    },
  }
}

export const getSearchGeometryLayer = (
  id: string
): CircleLayerSpecification => {
  return {
    id: id,
    type: 'circle',
    source: 'searchGeometry',
    filter: ['==', '$type', 'Point'],
    layout: {
      visibility: 'visible',
    },
    paint: {
      'circle-radius': 12,
      'circle-stroke-color': '#474E57',
      'circle-stroke-width': 4,
      'circle-color': 'rgba(0,0,0,0)',
    },
  }
}

export const getAdminLabels = (id: string): SymbolLayerSpecification => {
  return {
    id,
    type: 'symbol',
    source: 'cbs',
    minzoom: 8,
    maxzoom: 22,
    'source-layer': 'labels',
    layout: {
      visibility: 'none',
      'symbol-placement': 'point',
      'symbol-avoid-edges': false,
      'text-field': [
        'format',
        ['get', 'code'],
        { 'font-scale': 0.8 },
        '\n',
        {},
        ['get', 'name'],
        {
          'font-scale': 1,
        },
      ],
      'text-font': [
        'case',
        ['==', ['get', 'hierarchy'], 1],
        ['literal', ['SourceSansPro-Semibold']],
        ['literal', ['SourceSansPro-Regular']],
      ],
      'symbol-sort-key': ['get', 'rank'],
      'text-size': ['interpolate', ['linear'], ['zoom'], 8, 12, 16, 20],
      'text-max-width': 8,
      'text-anchor': 'center',
      'text-line-height': 1,
      'text-justify': 'center',
      'text-padding': 25,
      'text-allow-overlap': false,
    },
    paint: {
      'text-color': '#212121',
      'text-halo-color': '#e3e3e3',
      'text-halo-width': ['interpolate', ['linear'], ['zoom'], 1, 0.1, 16, 2],
      'text-halo-blur': 0.3,
    },
  }
}

export const getBuildingLayer = (
  id: string,
  threeDimensional: boolean
): LayerSpecification => {
  return threeDimensional
    ? {
        id: `${id}:extrusion`,
        type: 'fill-extrusion',
        source: 'bag',
        'source-layer': 'panden2024',
        minzoom: 12,
        maxzoom: 22,
        filter: ['has', 'woonfunctie_score'],
        paint: {
          'fill-extrusion-color': [
            'case',
            ['has', 'woonfunctie_score'],
            [
              'interpolate',
              ['linear'],
              ['get', 'woonfunctie_score'],
              0,
              '#EDE3CD',
              100,
              '#E0E0E0',
            ],
            '#E0E0E0',
          ],
          'fill-extrusion-opacity': 0.8,

          'fill-extrusion-height': [
            'interpolate',
            ['linear'],
            ['zoom'],
            13,
            0,
            16,
            ['*', 1.5, ['ceil', ['to-number', ['get', 'hoogte']]]],
          ],
        },
      }
    : {
        id,
        type: 'fill',
        source: 'bag',
        'source-layer': 'panden2024',
        minzoom: 12,
        maxzoom: 22,
        filter: ['has', 'woonfunctie_score'],
        paint: {
          'fill-color': [
            'case',
            ['has', 'woonfunctie_score'],
            [
              'interpolate',
              ['linear'],
              ['get', 'woonfunctie_score'],
              0,
              '#EDE3CD',
              100,
              '#E0E0E0',
            ],
            '#E0E0E0',
          ],
          'fill-outline-color': [
            'case',
            ['has', 'woonfunctie_score'],
            [
              'interpolate',
              ['linear'],
              ['get', 'woonfunctie_score'],
              0,
              '#bfb7a5',
              100,
              '#c9c9c9',
            ],
            '#c9c9c9',
          ],
          'fill-opacity': 0.8,
        },
      }
}

export const getAerialPhotos = (id: string): LayerSpecification => {
  return {
    id: `${id}`,
    type: 'raster',
    source: 'luchtfotos',
    paint: {},
  }
}
