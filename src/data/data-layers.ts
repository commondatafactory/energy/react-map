// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { ExpressionFilterSpecification, FilterSpecification } from 'maplibre-gl'
import {
  ColorScale,
  createColorScale,
} from '../utils/make-map-layer/colorscales'
import { SourceSettingsProps, sourceSettings } from './tile-sources'
import { metaDataList, MetaData } from './meta-data'

export interface DataLayers {
  [layerName: string]: Partial<DataLayerProps>
}

type TabsDook =
  | 'adressen-en-gebouwen'
  | 'autobranche'
  | 'bedrijventerreinen'
  | 'bestemmingsplannen'
  | 'dookai'
  | 'fail'
  | 'informatiebeeld-bedrijventerreinen'
  | 'kvk'
  | 'sociaaldook'
  | 'buitengebied'

type TabsDego =
  | 'armoede'
  | 'bereidheid'
  | 'energie'
  | 'gebouw'
  | 'grond'
  | 'klimaat'
  | 'sociaal'
  | 'spuk'
  | 'vleermuis'

type AuthSubdivisionsDego = 'energie'
type AuthSubdivisionsDook =
  | 'autobranche'
  | 'bedrijventerreinen'
  | 'buitengebied'
export type AuthRole =
  | AuthSubdivisionsDego
  | AuthSubdivisionsDook
  | 'no_subdivision'

export interface DataLayerProps {
  authLocked?: boolean
  authRoles: AuthRole[]
  desc: string
  drawType: 'count'
  layerType: '' | 'sbi-codes'
  id: string
  lastUpdate: string
  name: string
  note: string
  sourceDescription: MetaData
  styleLayers: Partial<StyleLayer>[]
  sublayers?: Partial<DataLayerProps>[]
  tab: TabsDook | TabsDego
}

interface DetailViewParams {
  properties: Record<string, any>
  id: string | number
}

interface PopupProps {
  title: string
  content?: string | Record<string, any>
}
interface DetailModalProps {
  content?: Array<Array<string>>
  desc?: string
  title: string
  type:
    | any
    | 'count'
    | 'detail'
    | 'building' // extra adres
    | 'rdw-detail'
    | 'count'
    | 'armoede' // grafiek
    | 'eg'
    | 'verbruik'

  url?: string
  urlTitle?: string
}

// FIX: Improve typedef. Right now we need to use Partial everywhere in order
// to make this type useable, but the different use cases should be identified
// and defined separately.
export type StyleLayer = {
  id: string
  isRelevantLayer?: boolean

  tileSource: Partial<SourceSettingsProps>
  'source-layer'?: string
  // Where to insert this layer in the stack. Is currently only used to insert
  // layers right after the OSM layers (so at the bottom of the style layer
  // stack so to speak)
  insert?: 'labels_populated_places'
  maxzoom?: number

  // For raster layers only. URL that will be called for more information when
  // a feature is clicked.
  featureRequest?: string

  // Used to:
  // 1) Render the relevant attr name in the map popup.
  // 2) Determine colors during rendering (See `getPaintExpression`).
  // 3) Use its value to conditionally render different things (See `getPaintExpression`).
  // 4) Render its value as text in the map when `geomType` is 'symbol'.
  // 5) Render different information in count/armoede modals.
  attrName: string

  // Render properties
  geomType: 'fill' | 'line' | 'circle' | 'raster' | 'symbol'
  // Either color or colorScale will have to be set.
  color?: string
  colorScale?: ColorScale
  opacity?: number
  expressionType?: 'interpolate' | 'step' | 'match' | 'case'
  caseArray?: string[]
  hasExtrusion?: boolean
  extrusionAttr?: string
  customPaint?: any
  // Only used when the geomType is 'circle'
  offset?: number[]

  // Should the map filter from the MapProvider be ignored? Sometimes you want
  // to display multiple layers and one of them is an SBI code layer, which has
  // an interface for setting a global feature filter. If another visible layer
  // is rendering different features, the global filter will mess up. In this
  // case we want to ignore the global filter (and maybe set a customFilter).
  //
  // You can also set this to false, to have the non-SBI code layers be
  // filtered away as soon as the user set a globalFilter. This 'trick' is used
  // for data layer 'layerDook100' to hide the BAG circles when an SBI filter
  // is active.
  ignoreGlobalFilter?: boolean
  customFilter?: FilterSpecification
  // Array of attr values that will be hidden from the map. This array will be
  // used in the maplayer spec.
  noDataValue?: (number | string)[]

  // Influences behavior on map click/hover
  isHighlightLayer?: boolean

  // Legend properties
  legendStops?: number[] | string[]
  noLegend?: boolean
  unit?: string
  scale?: string
  firstLabel?: string
  lastLabel?: string
  average?: number
  image?: string | string[]
  // Only used in the part of the legend component that enables filtering.
  filterAttrb?: string
  filterNames?: string[]
  filterValues?: number[] | string[] | [string, string[], string]

  popup?: ({ properties }: DetailViewParams) => PopupProps
  detailModal?: ({ properties, id }: DetailViewParams) => DetailModalProps
}

const customPaint = {
  hoverOutlines: {
    'line-color': [
      'case',
      ['==', ['feature-state', 'hover'], 'hover'],
      '#FFBC2C',
      ['==', ['feature-state', 'click'], 'click'],
      '#FFBC2C',
      'rgba(0,0,0,0)',
    ],
    'line-width': [
      'case',
      ['==', ['feature-state', 'click'], 'click'],
      7,
      ['==', ['feature-state', 'hover'], 'hover'],
      5,
      0,
    ],
  },
}

const energyLabelClasses = [
  '',
  'G (391 < kWh/m2/jaar)',
  'F (346 - 390 kWh/m2/jaar)',
  'E (301 - 345 kWh/m2/jaar)',
  'D (256 - 300 kWh/m2/jaar)',
  'C (196 - 255 kWh/m2/jaar)',
  'B (166 - 195 kWh/m2/jaar)',
  'A (111 - 165 kWh/m2/jaar)',
  'A+ (81 - 110 kWh/m2/jaar)',
  'A++ (51 - 80 kWh/m2/jaar)',
  'A+++ (1 - 50 kWh/m2/jaar)',
  'A++++ (< 0 kWh/m2/jaar)',
]

const customFilters: { [id: string]: FilterSpecification } = {
  eancodes: [
    'all',
    ['!=', ['get', 'p6_gasm3'], 'NA'],
    [
      '!=',
      ['length', ['to-string', ['get', 'pand_gas_ean_aansluitingen']]],
      '0',
    ],
  ],
  bouwjaar: ['has', 'bouwjaar'],
  spukelabels: [
    'any',
    ['==', ['to-string', ['get', 'energieklasse_score']], '1'],
    ['==', ['to-string', ['get', 'energieklasse_score']], '2'],
    ['==', ['to-string', ['get', 'energieklasse_score']], '3'],
    ['==', ['to-string', ['get', 'energieklasse_score']], '4'],
  ],
  spukwoz: [
    '<',
    ['to-number', ['get', 'woz']],
    ['to-number', ['get', 'gemeente_woz']],
  ],
  spukwoz2: ['<', ['to-number', ['get', 'woz']], ['to-number', '429']],
  spukDownload: [
    'all',
    [
      'any',
      ['==', ['to-string', ['get', 'energieklasse_score']], '1'],
      ['==', ['to-string', ['get', 'energieklasse_score']], '2'],
      ['==', ['to-string', ['get', 'energieklasse_score']], '3'],
      ['==', ['to-string', ['get', 'energieklasse_score']], '4'],
    ],
    [
      '<',
      ['to-number', ['get', 'woz']],
      ['to-number', ['get', 'gemeente_woz']],
    ],
  ],

  vleermuisDownload: [
    'all',
    [
      'any',
      ['==', ['get', 'energieklasse_score'], -1],
      ['==', ['get', 'energieklasse_score'], 1],
      ['==', ['get', 'energieklasse_score'], 2],
      ['==', ['get', 'energieklasse_score'], 3],
      ['==', ['get', 'energieklasse_score'], 4],
      ['==', ['get', 'energieklasse_score'], 5],
    ],
    ['<', ['to-number', ['get', 'bouwjaar']], 1993],
    ['>', ['to-number', ['get', 'percentage_koopwoningen']], 50],
  ],
  // Wordt gebruikt in de postcodelaag in the 'compensatielaag' weergave.
  vleermuisPostcode: [
    'all',
    ['>', ['to-number', ['get', 'percentage_koopwoningen']], 50],
  ],

  module2: [
    'any',
    ['==', ['get', 'l3_code'], '015'],
    ['==', ['get', 'l4_code'], '0142'],
    ['==', ['get', 'l4_code'], '0143'],
    ['==', ['get', 'l4_code'], '0149'],
    ['==', ['get', 'l5_code'], '01132'],
    ['==', ['get', 'l5_code'], '01133'],
    ['==', ['get', 'l5_code'], '01192'],
    ['==', ['get', 'l5_code'], '01252'],
    ['==', ['get', 'l5_code'], '01254'],
    ['==', ['get', 'l5_code'], '01303'],
    ['==', ['get', 'l5_code'], '01304'],
    ['==', ['get', 'l5_code'], '01412'],
    ['==', ['get', 'l5_code'], '01451'],
    ['==', ['get', 'l5_code'], '01452'],
    ['==', ['get', 'l5_code'], '01461'],
    ['==', ['get', 'l5_code'], '01462'],
    ['==', ['get', 'l5_code'], '01463'],
    ['==', ['get', 'l5_code'], '01471'],
    ['==', ['get', 'l5_code'], '01472'],
    ['==', ['get', 'l5_code'], '01473'],
    ['==', ['get', 'l5_code'], '01479'],
    ['==', ['get', 'l5_code'], '93125'],
  ],

  module3: [
    'any',
    ['==', ['get', 'l2_code'], '39'],
    ['==', ['get', 'l3_code'], '771'],
    ['==', ['get', 'l3_code'], '381'],
    ['==', ['get', 'l3_code'], '382'],
    ['==', ['get', 'l3_code'], '432'],
    ['==', ['get', 'l3_code'], '433'],
    ['==', ['get', 'l3_code'], '454'],
    ['==', ['get', 'l3_code'], '492'],
    ['==', ['get', 'l3_code'], '512'],
    ['==', ['get', 'l3_code'], '521'],
    ['==', ['get', 'l4_code'], '1621'],
    ['==', ['get', 'l4_code'], '1622'],
    ['==', ['get', 'l4_code'], '1623'],
    ['==', ['get', 'l4_code'], '1624'],
    ['==', ['get', 'l4_code'], '1629'],
    ['==', ['get', 'l4_code'], '3102'],
    ['==', ['get', 'l4_code'], '3109'],
    ['==', ['get', 'l4_code'], '4311'],
    ['==', ['get', 'l4_code'], '4399'],
    ['==', ['get', 'l4_code'], '4511'],
    ['==', ['get', 'l4_code'], '4531'],
    ['==', ['get', 'l4_code'], '4532'],
    ['==', ['get', 'l5_code'], '31012'],
    ['==', ['get', 'l5_code'], '45192'],
    ['==', ['get', 'l5_code'], '45193'],
    ['==', ['get', 'l5_code'], '45194'],
    ['==', ['get', 'l5_code'], '45203'],
    ['==', ['get', 'l5_code'], '45204'],
    ['==', ['get', 'l5_code'], '45205'],
    ['==', ['get', 'l5_code'], '46212'],
    ['==', ['get', 'l5_code'], '46219'],
    ['==', ['get', 'l5_code'], '47762'],
    ['==', ['get', 'l5_code'], '50201'],
    ['==', ['get', 'l5_code'], '50401'],
  ],
}

const verblijfsobjectenDetailModal = ({ properties, id }) => ({
  type: 'detail',
  title: 'Details Verblijfsobject',
  url: `https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html#?searchQuery=${id
    .toString()
    .padStart(16, '0')}`,
  urlTitle: 'BAG viewer',
  content: [
    properties.openbareruimtenaam
      ? ['openbareruimtenaam', properties.openbareruimtenaam]
      : null,
    properties.huisnummer ? ['huisnummer', properties.huisnummer] : null,
    properties.huisletter ? ['huisletter', properties.huisletter] : null,
    properties.huisnummertoevoeging
      ? ['huisnummertoevoeging', properties.huisnummertoevoeging]
      : null,
    id && ['Nummeraanduiding id ', id],
    properties.vid && [
      'verblijfsobjectId',
      properties.vid.toString().padStart(16, '0'),
    ],
    [
      'Gebruiksdoel',
      (properties?.gebruiksdoelen as string)?.replace(';', ', ') || '',
    ],
  ],
})

export const staticBuildingsLayer: Partial<DataLayerProps> = {
  id: 'layer0',
  styleLayers: [
    {
      id: 'layer0',
      hasExtrusion: true,
    },
  ],
}

const noDataStyleLayer: Partial<DataLayerProps> = {
  id: 'noDataLayer',
}

// const postalCodesStyleLayer: Partial<DataLayerProps> = {
//   id: 'postalCodes',
// }

export const demoDataLayers: Partial<DataLayerProps>[] = [
  //
  // no layer selected
  //
  staticBuildingsLayer,
  //
  // DOOK AI DEMO
  //
  {
    id: 'layerDook300',
    tab: 'dookai',
    desc: 'Demo dook AI image detection',
    name: 'Demo AI images',
    sourceDescription: {
      dookaiimages: {
        referenceDate: '2023-01',
      },
    },
    lastUpdate: '2023-01',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'Locatie van images',
        unit: '',
        attrName: 'isScraped',
        tileSource: sourceSettings.dookaiimages,
        geomType: 'circle',
        //expressionType: 'match',
        //legendStops: [
        //  'New',
        //  'Processed',
        //],
        //colorScale: createColorScale({
        //  type: 'ordinal',
        //  colorRange: ['#ffbc2c', '#0b71a1'],
        //  ordinalDomain: [0, 1],
        //}),
        popup: ({ properties }): PopupProps => ({
          title: `Image: ${properties.image_name} ${properties.date}`,
        }),
        // detailModal: ({ properties }) => ({
        //   title: 'image from app',
        //   type: 'image',
        //   content: [
        //     ['Naam', properties.image_name],
        //     ['Datum', `${new Date(properties.time * 1000)}`],
        //     ['Scraped', properties.isScraped],
        //   ],
        // }),
      },
    ],
  },
  {
    id: 'layerDook301',
    tab: 'dookai',
    desc: 'Demo dook AI licenceplate detection',
    name: 'Demo AI licenceplates',
    sourceDescription: {
      dookailicenceplate: {
        referenceDate: '2023-01',
      },
    },
    lastUpdate: '2023-01',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: '',
        unit: '',
        attrName: 'target_image_id',
        tileSource: sourceSettings.dookailicenceplate,
        geomType: 'circle',
        //expressionType: 'match',
        //legendStops: [
        //  'New',
        //  'Processed',
        //],
        //colorScale: createColorScale({
        //  type: 'ordinal',
        //  colorRange: ['#ffbc2c', '#0b71a1'],
        //  ordinalDomain: [0, 1],
        //}),
        popup: ({ properties }): PopupProps => ({
          title: `Image: ${properties.image_name} ${properties.date}`,
        }),
        // detailModal: ({ properties }) => ({
        //   title: 'image from app',
        //   // type: 'image',
        //   content: [
        //     ['Naam', properties.image_name],
        //     ['Datum', `${new Date(properties.time * 1000)}`],
        //     ['Scraped', properties.isScraped],
        //   ],
        // }),
      },
    ],
  },
  {
    id: 'layerDook302',
    tab: 'dookai',
    desc: 'Demo dook AI text.  Met de "DOOK VNG" app in playstore google kan met gebruiker: demo  en wachtwoord 2345 foto op de kaart gezet worden.',
    name: 'Demo AI text',
    sourceDescription: {
      dookaitext: {
        referenceDate: '2023-01',
      },
    },
    lastUpdate: '2023-01',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: '',
        unit: '',
        attrName: 'target_image_id',
        tileSource: sourceSettings.dookaitext,
        geomType: 'circle',
        //expressionType: 'match',
        //legendStops: [
        //  'New',
        //  'Processed',
        //],
        //colorScale: createColorScale({
        //  type: 'ordinal',
        //  colorRange: ['#ffbc2c', '#0b71a1'],
        //  ordinalDomain: [0, 1],
        //}),
        popup: ({ properties }): PopupProps => ({
          title: `Image: ${properties.image_name} ${properties.date}`,
        }),
        // detailModal: ({ properties }) => ({
        //   title: 'image from app',
        //   // type: 'image',
        //   content: [
        //     ['Naam', properties.image_name],
        //     ['Datum', `${new Date(properties.time * 1000)}`],
        //     ['Scraped', properties.isScraped],
        //   ],
        // }),
      },
    ],
  },
]

export const dookDataLayers: Partial<DataLayerProps>[] = [
  //
  // no layer selected
  //
  staticBuildingsLayer,

  //
  // insolventies
  //
  // {
  //   authRoles: ['autobranche', 'bedrijventerreinen'],
  //   id: 'layerFail101',
  //   tab: 'fail',
  //   desc: 'Locaties van faillissementen / insolventies, de dataset wordt 3x per dag op weekdagen actueel gehouden. Data blijft maximaal 6 maanden staan.',
  //   name: 'Insolventie Register Faillissementen',
  //   sourceDescription: {
  //     'https://insolventies.rechtspraak.nl': {
  //       url: 'https://insolventies.rechtspraak.nl',
  //       referenceDate: '2024-08',
  //     },
  //   },
  //   lastUpdate: '2024-08',
  //   styleLayers: [
  //     staticBuildingsLayer.styleLayers[0],
  //     {
  //       scale: 'Tijdsindicatie',
  //       unit: 'Faillissementen',
  //       attrName: 'pt',
  //       tileSource: sourceSettings.faillissementen,
  //       geomType: 'circle',
  //       expressionType: 'interpolate',
  //       legendStops: getCurrentAndPrevDate(6),
  //       firstLabel: '> 6 maanden geleden',
  //       lastLabel: 'heden',
  //       colorScale: createColorScale({
  //         type: 'sequentional',
  //         color: 'red',
  //         domain: getCurrentAndPrevDate(6, 'middle'),
  //       }),
  //       popup: ({ properties }): Popup => ({
  //         title: `${properties.description}`,
  //       }),
  //       detailModal: ({ properties }) => ({
  //         type: 'detail',
  //         title: `Details Faillisement ${properties.name}`,
  //         desc: properties.description,
  //         url: `https://insolventies.rechtspraak.nl/#!/details/${properties.insolvency_id}`,
  //         urlTitle: 'Bekijk dit item op het Centraal Insolventieregister',
  //         content: [
  //           ['Naam', properties.name + ' ' + properties.description],
  //           ['Datum', `${new Date(properties.pt * 1000)}`],
  //           ['Register ID', properties.insolvency_id],
  //           ['KvK-nummer', properties.kvk_number],
  //           [
  //             'Vestigingsadres',
  //             `${properties.street} ${properties.street_number} ${properties.postal_code} ${properties.town}`,
  //           ],
  //         ],
  //       }),
  //     },
  //   ],
  // },
  //
  // bedrijventerreinen
  //
  {
    id: 'layerDook',
    authRoles: ['no_subdivision', 'bedrijventerreinen'],
    tab: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit OSM',
    desc: 'Deze kaartlaag laat de industriële en commerciële gebieden zien vanuit Open Street Map, een wereldkaart die vrij van licentie beschikbaar is. Getoonde datalaag: Landuse.',
    sourceDescription: {
      OSM: {
        url: 'https://wiki.openstreetmap.org/wiki/Landcover',
        referenceDate: '2021',
      },
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        // FIX: When mousing over a feature in this layer, we get an error
        // saying a feature ID must be provided.
        unit: 'Type gebied',
        filterValues: ['industrial', 'commercial'],
        filterNames: ['Industrieel', 'Commercieel'],
        filterAttrb: 'class',
        geomType: 'fill',
        attrName: 'class',
        tileSource: sourceSettings.openmaptiles,
        legendStops: ['Industrieel', 'Commercieel'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02', '#AD8886'],
          ordinalDomain: ['industrial', 'commercial'],
        }),
        popup: ({ properties }) => ({
          title: `Landgebruik`,
          content:
            properties.class === 'industrial' ? 'Industrieel' : 'Commercieel',
        }),
      },
      {
        geomType: 'line',
        attrName: 'class',
        tileSource: sourceSettings.openmaptiles,
        expressionType: 'match',
        insert: 'labels_populated_places',
        noLegend: true,
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02', '#AD8886'],
          ordinalDomain: ['industrial', 'commercial'],
        }),
      },
    ],
  },
  {
    id: 'layerDook2',
    authRoles: ['no_subdivision', 'bedrijventerreinen'],
    tab: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit topNL',
    desc: 'TOPNL-bestanden worden gebruikt als basis voor het bekijken en bewerken van geo-informatie en hebben als bron de Basisregistratie Topografie (BRT). Getoonde datalaag: Functioneel gebied.',
    sourceDescription: {
      'Kadaster, BRT': {
        url: 'https://www.pdok.nl/introductie/-/article/basisregistratie-topografie-brt-topnl',
        referenceDate: '2021',
      },
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        unit: 'Gebied',
        geomType: 'fill',
        attrName: 'typefunctioneelgebied',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'functioneelgebied',
        legendStops: ['Bedrijventerrein'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['bedrijventerrein'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: 'Functioneel gebied',
          content: properties.typefunctioneelgebied,
        }),
      },
      {
        geomType: 'line',
        attrName: 'typefunctioneelgebied',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'functioneelgebied',
        expressionType: 'match',
        insert: 'labels_populated_places',
        noLegend: true,
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['bedrijventerrein'],
        }),
      },
    ],
  },
  {
    id: 'layerDook3',
    authRoles: ['no_subdivision', 'bedrijventerreinen'],
    tab: 'bedrijventerreinen',
    name: 'Bedrijventerreinen uit IBIS',
    desc: 'IBIS bevat beschrijvende data over de 3.800 Nederlandse bedrijventerreinen. IBIS wordt gepubliceerd door het interprovinciaal Overleg (IPO).',
    sourceDescription: {
      IBIS: {
        url: 'https://www.ibis-bedrijventerreinen.nl',
        referenceDate: '2021',
      },
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        unit: 'Gebied',
        geomType: 'fill',
        attrName: 'status',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'ibis_bedrijventerrein',
        legendStops: ['Bedrijventerrein'],
        expressionType: 'match',

        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['GEACCORDEERD'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `IBIS Bedrijventerrein`,
          content: `${properties.plan_naam}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `IBIS Bedrijventerrein ${properties.plan_naam}`,
          desc: properties.kern_naam,
          content: [
            ['Oppervlakte', properties.ha_bruto + 'ha bruto'],
            ['Oppervlakte', properties.ha_netto + 'ha netto'],
            ['Status', properties.status],
            ['Millieuzone', properties.milieuzone],
          ],
        }),
      },
      {
        geomType: 'line',
        attrName: 'status',
        tileSource: sourceSettings.bedrijven,
        'source-layer': 'ibis_bedrijventerrein',
        expressionType: 'match',
        insert: 'labels_populated_places',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#BD0B02'],
          ordinalDomain: ['GEACCORDEERD'],
        }),
      },
    ],
  },

  // bestemmingsplannen
  {
    id: 'layerDook40',
    authRoles: ['no_subdivision', 'autobranche', 'bedrijventerreinen'],
    tab: 'bestemmingsplannen',
    name: 'Enkelbestemming',
    desc: 'Iedere locatie heeft een enkelbestemming en kan meerdere dubbelbestemmingen hebben. In de enkelbestemming staat beschreven of er gebouwd mag worden en wat voor bebouwing dat mag zijn. De enkelbestemming beschrijft voor welke functie de grond bestemd is (bijvoorbeeld woningen of bedrijven). <span style="color:red;">Let op!:</span> Voor het raadplegen van de meest actuele planinformatie op de wettelijk vastgestelde wijze is <a href="https://www.ruimtelijkeplannen.nl/home">Ruimtelijkeplannen.nl</a> het officiële portaal.',
    sourceDescription: {
      'Ruimtelijke plannen Wro: Ruimtelijkeplannen.nl':
        metaDataList.ruimtelijkeplannen,
    },
    lastUpdate: '2023-11',
    styleLayers: [
      {
        scale: 'WMS',
        unit: 'Enkelbestemming',
        tileSource: sourceSettings.enkelbestemming,
        geomType: 'raster',
        opacity: 0.5,
        image:
          'https://service.pdok.nl/kadaster/plu/wms/v1_0/legend/enkelbestemming/enkelbestemming.png?transparant=true',
        featureRequest:
          'https://service.pdok.nl/kadaster/plu/wms/v1_0?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&STYLES=&LAYERS=enkelbestemming&QUERY_LAYERS=enkelbestemming&FORMAT=image/png&INFO_FORMAT=application/json',
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `${properties.bestemmingshoofdgroep}`,
          content: [
            properties.bestemmingshoofdgroep
              ? ['Hoofdgroep', properties.bestemmingshoofdgroep]
              : null,
            properties.dossierid ? ['dossier id', properties.dossierid] : null,
            properties.datum ? ['Datum', properties.datum] : null,
            properties.naam ? ['Naam', properties.naam] : null,
          ],
          url: properties.verwijzingnaartekst,
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layerDook41',
    authRoles: ['no_subdivision', 'autobranche', 'bedrijventerreinen'],
    tab: 'bestemmingsplannen',
    name: 'Dubbelbestemming',
    desc: 'Een dubbelbestemming in een bestemmingsplan is een extra bestemming met daarin opgenomen regels. Hier dient dan bij het bouwen rekening mee gehouden te worden. Voorbeelden van een extra bestemming kunnen zijn: waardevolle bomen in het gebied, de aanwezigheid van hoogspanningsleidingen of een beschermd stadsgezicht. <span style="color:red;">Let op!:</span> Voor het raadplegen van de meest actuele planinformatie op de wettelijk vastgestelde wijze is <a href="https://www.ruimtelijkeplannen.nl/home">Ruimtelijkeplannen.nl</a> het officiële portaal.',
    sourceDescription: {
      'Ruimtelijke plannen Wro: Ruimtelijkeplannen.nl':
        metaDataList.ruimtelijkeplannen,
    },
    lastUpdate: '2023-11',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'WMS',
        unit: 'Dubbelbestemming',
        tileSource: sourceSettings.dubbelbestemming,
        geomType: 'raster',
        image:
          'https://service.pdok.nl/kadaster/plu/wms/v1_0/legend/bouwvlak/bouwvlak.png?transparent=true',
        featureRequest:
          'https://service.pdok.nl/kadaster/plu/wms/v1_0?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&STYLES=&LAYERS=Dubbelbestemming&QUERY_LAYERS=Dubbelbestemming&FORMAT=image/png&INFO_FORMAT=application/json',
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `${properties.bestemmingshoofdgroep}`,
          content: [
            properties.bestemmingshoofdgroep
              ? ['Hoofdgroep', properties.bestemmingshoofdgroep]
              : null,
            properties.dossierid ? ['dossier id', properties.dossierid] : null,
            properties.datum ? ['Datum', properties.datum] : null,
            properties.naam ? ['Naam', properties.naam] : null,
          ],
        }),
      },
    ],
  },

  //
  // autobranche
  //
  {
    id: 'layer2316',
    tab: 'autobranche',
    authRoles: ['autobranche'],
    name: 'Concentratie autobedrijven (uit Zicht op Onregelmatigheden)',
    desc: `Voor de ondernemingen met als bedrijfsactiviteit handel in en reparatie van auto’s (SBI 45, behalve handel in en reparatie van caravans (SBI 45194)) wordt getoond in welke gemeenten en wijken hoge concentraties van bedrijfsvestigingen voorkomen ten opzichte van het landelijke beeld.`,
    sourceDescription: {
      'Zicht op ondermijning': {
        url: 'https://www.zichtopondermijning.nl/',
        referenceDate: '2018',
      },
    },
    lastUpdate: '2022-06-15',
    sublayers: [
      {
        id: 'layer2316_1',
        name: 'Aantal vestigingen per 10.000 inwoners , concentratie 2018.',
        styleLayers: [
          {
            scale: 'Wijk',
            unit: 'absoluut',
            attrName: 'branche_vst_10k_inw_2018_concentratie',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [3, 15, 35, 50, 100],
            firstLabel: '< 3',
            lastLabel: '100 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [3, 50, 100],
            }),
            popup: ({
              properties: { branche_vst_10k_inw_2018_concentratie },
            }) => ({
              title: `${branche_vst_10k_inw_2018_concentratie}`,
            }),
          },
        ],
      },
      {
        id: 'layer2316_2',
        name: 'Aantal vestigingen per 10.000 vestigingen , concentratie 2018.',
        styleLayers: [
          {
            scale: 'Wijk',
            unit: 'absoluut',
            attrName: 'branche_vst_1000vst_2018_concentratie',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [3, 15, 35, 50, 100],
            firstLabel: '< 3',
            lastLabel: '100 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [3, 50, 100],
            }),
            popup: ({
              properties: { branche_vst_1000vst_2018_concentratie },
            }) => ({
              title: `${branche_vst_1000vst_2018_concentratie}`,
            }),
          },
        ],
      },
      {
        id: 'layer2316_3',
        name: 'Ontwikkeling aantal vestigingen per 10.000 inwoners,absoluut, 2015-2018',
        styleLayers: [
          {
            scale: 'Wijk',
            unit: '%',
            attrName: 'branche_vst_10k_inw_2015_2018_abs',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [-5, 0, 5],
            firstLabel: '< -5 ',
            lastLabel: '5 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              domain: [-5, 0, 5],
              type: 'diverging',
              color: 'blue',
            }),
            popup: ({ properties: { branche_vst_10k_inw_2015_2018_abs } }) => ({
              title: `${branche_vst_10k_inw_2015_2018_abs}`,
            }),
          },
        ],
      },
      {
        id: 'layer2316_4',
        name: 'Ontwikkeling aantal vestigingen per 10.000 inwoners, relatief , 2015-2018',
        styleLayers: [
          {
            scale: 'Wijk',
            unit: '%',
            attrName: 'branche_vst_10k_inw_2015_2018_pct',
            tileSource: sourceSettings.cbs,
            'source-layer': 'zoobuurtcijfers2019',
            legendStops: [-13, 0, 20],
            firstLabel: '< -13',
            lastLabel: '20 >',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              domain: [-13, 0, 20],
              type: 'diverging',
              color: 'blue',
            }),
            popup: ({ properties: { branche_vst_10k_inw_2015_2018_pct } }) => ({
              title: `${branche_vst_10k_inw_2015_2018_pct}%`,
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layerDook111',
    authRoles: ['autobranche'],
    tab: 'autobranche',
    name: 'Ondernemingen in de autobranche (volgens SBI)',
    desc: 'Vestigingen van ondernemingen in de autobranche (SBI 45 en 77). De exacte lijst met SBI codes is te vinden in de bijlage van de <a href="https://www.kennisplatformondermijning.nl/kennisbank/bijlage/836">Handreiking Informatiebeeld Autobranche</a> voor gemeenten.',
    sourceDescription: {
      KVK: metaDataList.KVK,
    },
    lastUpdate: '2024-08',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'verblijfsobject',
        unit: 'SBI code',
        attrName: 'l2_code',
        tileSource: sourceSettings.kvk2,
        isRelevantLayer: true,
        geomType: 'circle',
        legendStops: [
          "Handel in en reparatie van auto's, motorfietsen en aanhangers",
          "Verhuur en lease van auto's, consumentenartikelen, machines en overige roerende goederen",
        ],
        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#FF7F00', '#ffeda0', '#bebada'],
          ordinalDomain: ['45', '77'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `SBI code: ${properties.hoofdact}.`,
          content: `${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `SBI Code`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title || ''}`],
            ['Thema 3', `${properties.l3_title || ''}`],
            ['Thema 4', `${properties.l4_title || ''}`],
            ['Thema 5', `${properties.l5_title || ''}`],

            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{hn_1x45}',
      },
    ],
  },
  {
    id: 'layerDook112',
    authRoles: ['autobranche'],
    tab: 'autobranche',
    name: 'RDW met RDW erkenning',
    desc: 'Overzicht van de door RDW erkende bedrijven, op adres gekoppeld aan verblijfsobjecten en staanplaatsen in de BAG. De Match score geeft aan hoe nauwkeurig de koppeling is met de BAG, waar 0 fouten een 100% match betekend met een adres in de BAG. Ook het type erkenning wordt weergegeven.',
    sourceDescription: {
      RDW: metaDataList.RDW,
      'BAG, verblijfsobject': metaDataList.BAGverblijfsobject,
    },
    lastUpdate: '2022-06',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'match score',
        unit: 'BAG adres koppelingsscore',
        isRelevantLayer: true,
        attrName: 'match_score',
        tileSource: sourceSettings.rdw,
        geomType: 'circle',
        legendStops: [0, 10],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#984ea3', 'black'],
          domain: [0, 10],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `RDW bedrijf`,
          content: `${properties.naam_bedrijf} - ${properties.gevelnaam}`,
        }),
        detailModal: ({ properties, id }) => ({
          type: 'rdw-detail',
          title: 'RDW erkend bedrijf',
          content: [
            ['Bedrijf', properties.naam_bedrijf],
            ['Gevelnaam', properties.gevelnaam],
            ['RDW Adres', properties.rdw_toevoeging],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${
                  properties.huisnummertoevoeging
                    ? properties.huisnummertoevoeging
                    : ''
                }
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{gevelnaam}',
      },
    ],
  },
  {
    id: 'layerDook113',
    tab: 'autobranche',
    authRoles: ['autobranche'],
    name: 'Ondernemingen met BOVAG-lidmaatschap',
    desc: 'Overzicht van bedrijven met een BOVAG lidmaatschap, op adres gekoppeld aan verblijfsobjecten en staanplaatsen in de BAG. De Match score geeft aan hoe nauwkeurig de koppeling is met de BAG, waar 0 fouten een 100% match betekend met een adres in de BAG.',
    sourceDescription: {
      BOVAG: metaDataList.BOVAG,
      'BAG, verblijfsobject': metaDataList.BAGverblijfsobject,
    },
    lastUpdate: '2022-06',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'match score',
        unit: 'BAG adres koppelingsscore',
        attrName: 'match_score',
        isRelevantLayer: true,
        tileSource: sourceSettings.bovag,
        geomType: 'circle',
        legendStops: [0, 10],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#39870c', 'black'],
          domain: [0, 10],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `BOVAG bedrijf - ${properties.name}`,
          content: `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
        }),
        detailModal: ({ properties }) => ({
          type: 'rdw-detail',
          title: 'BOVAG bedrijf',
          url: properties?.name
            ? `https://www.bovag.nl/leden/${
                properties.name.charAt(properties.name.length - 1) === '.'
                  ? properties.name
                      .replaceAll(' &', '')
                      .replace(/\s/g, '-')
                      .replaceAll("'", '-')
                      .replaceAll('.', '-')
                      .slice(0, -1)
                      .toLowerCase()
                  : properties.name
                      .replaceAll(' &', '')
                      .replace(/\s/g, '-')
                      .replaceAll("'", '-')
                      .replaceAll('.', '-')
                      .toLowerCase()
              }`
            : '',
          urlTitle: 'Bovag website',
          content: [
            ['Bedrijf', properties.name],
            [
              'BOVAG Adres',
              `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
            ],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${properties.bag_toevoeging ? properties.bag_toevoeging : ''}
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            [
              'Verblijfsobject id',
              properties.vid?.toString().padStart(16, '0'),
            ],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{name}',
      },
    ],
  },

  {
    id: 'layerDook114',
    authRoles: ['autobranche'],
    tab: 'autobranche',
    name: 'Download basisdataset ten behoeve van het informatiebeeld',
    desc: 'Maak hier een download van de basisdataset zoals beschreven in Informatiefase 1 van de <a href="https://www.kennisplatformondermijning.nl/kennisbank/bijlage/836" > Handreiking Informatiebeeld Autobranche</a> voor gemeenten. Hier is data vanuit het HR (KVK), RDW erkenningen en BOVAG lidmaatschappen samengebracht en gekoppeld aan de BAG.',
    sourceDescription: {
      'BAG, verblijfsobject in viewer': metaDataList.BAGverblijfsobject,
      'BAG, verblijfsobject in download':
        metaDataList.BAGverblijfsobjectDownload,
      KVK: metaDataList.KVK,
      RDW: metaDataList.RDW,
      BOVAG: metaDataList.BOVAG,
    },
    lastUpdate: '2024-08',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        legendStops: ['BAG - Adres'],
        color: '#cacaca',
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}
             ${properties.huisnummertoevoeging || ''}
          `,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
      {
        attrName: 'match_score',
        tileSource: sourceSettings.bovag,
        offset: [15, 0],
        legendStops: ['BOVAG'],
        geomType: 'circle',
        color: '#39870c',
        popup: ({ properties }): PopupProps => ({
          title: `BOVAG bedrijf - ${properties.name}`,
          content: `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: 'BOVAG bedrijf',
          url: properties?.name
            ? `https://www.bovag.nl/leden/${
                properties.name.charAt(properties.name.length - 1) === '.'
                  ? properties.name
                      .replaceAll(' &', '')
                      .replace(/\s/g, '-')
                      .replaceAll("'", '-')
                      .replaceAll('.', '-')
                      .slice(0, -1)
                      .toLowerCase()
                  : properties.name
                      .replaceAll(' &', '')
                      .replace(/\s/g, '-')
                      .replaceAll("'", '-')
                      .replaceAll('.', '-')
                      .toLowerCase()
              }`
            : '',
          urlTitle: 'Bovag website',
          content: [
            ['Bedrijf', properties.name],
            [
              'BOVAG Adres',
              `${properties.bovag_adres ? properties.bovag_adres : ''}
              ${properties.bovag_toevoeging ? properties.bovag_toevoeging : ''}
                ${properties.bovag_postcode ? properties.bovag_postcode : ''}
                ${properties.bovag_plaats ? properties.bovag_plaats : ''}
                `,
            ],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${properties.bag_toevoeging ? properties.bag_toevoeging : ''}
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            [
              'Verblijfsobject id',
              properties.vid?.toString().padStart(16, '0'),
            ],
            [
              'Gebruiksdoel',
              [
                'Gebruiksdoel',
                (properties?.gebruiksdoelen as string)?.replace(';', ', ') ||
                  '',
              ],
            ],
          ],
        }),
      },
      {
        attrName: 'match_score',
        tileSource: sourceSettings.rdw,
        offset: [0, -15],
        legendStops: ['RDW'],
        geomType: 'circle',
        color: '#984ea3',
        popup: ({ properties }): PopupProps => ({
          title: `RDW bedrijf`,
          content: `${properties.naam_bedrijf} - ${properties.gevelnaam}`,
        }),
        detailModal: ({ properties, id }) => ({
          type: 'detail',
          title: 'RDW erkend bedrijf',
          content: [
            ['Bedrijf', properties.naam_bedrijf],
            ['Gevelnaam', properties.gevelnaam],
            ['RDW Adres', properties.rdw_toevoeging],
            [
              'BAG Adres',
              `${properties.huisnummer ? properties.huisnummer : ''}
                ${properties.huisletter ? properties.huisletter : ''}
                ${
                  properties.huisnummertoevoeging
                    ? properties.huisnummertoevoeging
                    : ''
                }
                ${properties.straat ? properties.straat : ''}
                ${properties.postcode ? properties.postcode : ''}
                ${properties.plaats ? properties.plaats : ''}`,
            ],
            [
              'Match score',
              properties.match_score +
                (properties.match_score == 1 ? ' fout' : ' fouten'),
            ],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        attrName: 'l2_code',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        isRelevantLayer: true,
        expressionType: 'match',
        offset: [-15, 0],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#cc4c02', '#FF7F00'],
          ordinalDomain: ['45', '77'],
        }),
        legendStops: ['KVK - SBI code 45', 'KVK - SBI code 77'],
        popup: ({ properties }): PopupProps => ({
          title: `KVK activiteit: ${properties.hoofdact}.`,
          content: `${properties.hn_1x45}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
            [
              'kvk url',
              'https://www.kvk.nl/bestellen/#/' +
                properties.sbicode +
                properties.vgnummer,
            ],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{hn_1x45}',
      },
    ],
  },

  //
  // vastgoed
  //
  {
    id: 'layerDook1',
    tab: 'adressen-en-gebouwen',
    authRoles: [
      'no_subdivision',
      'autobranche',
      'bedrijventerreinen',
      'buitengebied',
    ],
    name: 'Adressen (verblijfsobjecten, ligplaatsen, standplaatsen)',
    desc: 'Alle adressen in Nederland uit de BAG, onderverdeeld in verblijfsobjecten, ligplaatsen en standplaatsen.',
    lastUpdate: '2023-10',
    drawType: 'count',
    sourceDescription: {
      'BAG, verblijfsobject in viewer': metaDataList.BAGverblijfsobject,
      'BAG, verblijfsobject in download':
        metaDataList.BAGverblijfsobjectDownload,
    },
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        unit: 'BAG type',
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: ['verblijfsobject', 'standplaats', 'ligplaats'],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#bebada'],
          ordinalDomain: ['vid', 'sid', 'lid'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}
             ${properties.huisnummertoevoeging || ''}
          `,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
    ],
  },
  {
    id: 'layerDook45',
    authRoles: [
      'no_subdivision',
      'autobranche',
      'bedrijventerreinen',
      'buitengebied',
    ],
    tab: 'adressen-en-gebouwen',
    name: 'Oppervlakte verblijfsobject',
    desc: 'Deze kaartlaag biedt inzicht in de oppervlakte in m2 van een verblijfsobject.',
    lastUpdate: '2023-10',
    sourceDescription: {
      'BAG, oppervlakte': metaDataList.BAG,
    },
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Oppervlakte m2',
        expressionType: 'interpolate',
        attrName: 'pand_area',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [17, 123, 500, 1000],
        firstLabel: '< 17',
        lastLabel: '> 1000',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'log',
          colorRange: ['#fff7bc', '#fec44f', '#662506', '#000000'],
          domain: [17, 123, 1000, 200000],
        }),
        popup: ({ properties: { pand_area } }) => ({
          title: `Oppervlakte ${pand_area} m2`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.pand_area
              ? ['Oppervlakte', properties.pand_area + ' m2']
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
          ],
        }),
      },
    ],
  },

  {
    id: 'layerDook4',
    authRoles: [
      'no_subdivision',
      'autobranche',
      'bedrijventerreinen',
      'buitengebied',
    ],
    tab: 'adressen-en-gebouwen',
    name: 'Verblijfsobjecten met woonfunctie',
    desc: 'Een van de mogelijke gebruiksdoelen van een verblijfsobject is de woonfunctie. De Woonfunctie geeft een indicatie van het woninggebruik in een object. We onderscheiden 3 categorieën: objecten met alleen een woonfunctie, objecten met gedeeltelijke woonfunctie, objecten zonder woonfunctie.',
    note: '100 = wonen , 0 = niet wonen',
    lastUpdate: '2024-05-24',
    sourceDescription: {
      'BAG, gebruiksdoel woonfunctie': metaDataList.BAG,
    },
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Indicatie woonfunctie',
        attrName: 'woonfunctie_score',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        extrusionAttr: 'hoogte',

        legendStops: [
          'Geen woonfunctie',
          'Verschillende functies(waaronder wonen)',
          'Uitsluitend woonfunctie',
        ],
        expressionType: 'step',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#f1d751', '#797673', '#052f68'],
          domain: [0, 1, 99, 100],
        }),

        popup: ({ properties }): PopupProps => ({
          title: `Woonfunctie indicatie`,
          content:
            properties.woonfunctie_score === 0
              ? 'Geen woonfunctie'
              : properties.woonfunctie_score === 100
              ? 'Alleen woonfunctie'
              : `Verschillende functies (waaronder wonen)`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.pand_area
              ? ['Oppervlakte', properties.pand_area + ' m2']
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
          ],
        }),
      },
    ],
  },

  {
    id: 'layerDook44',
    authRoles: [
      'no_subdivision',
      'autobranche',
      'bedrijventerreinen',
      'buitengebied',
    ],
    tab: 'adressen-en-gebouwen',
    name: 'Bouwjaar verblijfsobject',
    desc: 'Deze kaart geeft informatie over het oorsprongelijke bouwjaar van verblijfsobjecten.',
    lastUpdate: '2022-09',
    sourceDescription: {
      'BAG, oorspronkelijke bouwjaar': metaDataList.BAG,
      'BAG, verblijfsobject in download': metaDataList.BAGdownload,
    },
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Bouwjaar',
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [1900, 1930, 1945, 1960, 1990, 1998, 2023],
        noDataValue: [1050, 0],
        expressionType: 'interpolate',
        firstLabel: 'voor 1900',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c', '#F1E7AA'],
          domain: [1500, 1900, 1945, 1995, 2015],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `Bouwjaar: ${properties.bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.pand_area
              ? ['Oppervlakte', properties.pand_area + ' m2']
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,
          ],
        }),
      },
    ],
    drawType: 'count',
  },

  //
  // KVK
  //

  {
    id: 'layerDook100',
    authRoles: ['bedrijventerreinen'],
    tab: 'informatiebeeld-bedrijventerreinen',
    name: 'Handelsregister',
    desc: 'nvt',
    sourceDescription: {
      KVK: metaDataList.KVK,
    },
    lastUpdate: '2024-08',
    layerType: 'sbi-codes',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        unit: 'BAG',
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        // Effect: this layer will be hidden when a `globalFilter` is set.
        ignoreGlobalFilter: false,
        legendStops: ['Adres zonder KVK registratie'],
        color: '#cacaca',
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}
             ${properties.huisnummertoevoeging || ''}
          `,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
      {
        scale: 'verblijfsobject',
        unit: 'SBI code- hoofdcategorie',
        isRelevantLayer: true,
        attrName: 'l1_code',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        legendStops: [
          'A. Landbouw, Bosbouw en Visserij',
          'B. Winning van Delfstoffen',
          'C. Industrie',
          'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
          'E. Winning en Distributie van Water',
          'F. Bouwnijverheid',
          'G. Groot- en Detailhandel; Reparatie Van Auto’s',
          'H. Vervoer en Opslag',
          'I. Logies-, Maaltijd- en Drankverstrekking',
          'J. Informatie en Communicatie',
          'K. Financiële Instellingen',
          'L. Verhuur van en Handel in Onroerend Goed',
          'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
          'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
          'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
          'P. Onderwijs',
          'Q. Gezondheids- en Welzijnszorg',
          'R. Cultuur, Sport en Recreatie',
          'S. Overige Dienstverlening',
          'T. Huishoudens als Werkgever',
          'U. Extraterritoriale Organisaties en Lichamen',
        ],

        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: [
            '#70401E',
            '#7FDB23',
            '#b2df8a',
            '#73824F',
            '#33a02c',
            '#043D00',
            '#ff7f00',
            '#e31a1c',
            '#d95f02',
            '#fb9a99',
            '#6a3d9a',
            '#bc80bd',
            '#000000',
            '#666666',
            '#e6ab02',
            '#ffff33',
            '#ffff99',
            '#46bcce',
            '#a6cee3',
            '#0000ff',
            '#377eb8',
          ],
          ordinalDomain: [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
          ],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{l1_code} {sbicode}',
      },
    ],
  },
  {
    id: 'layerDook100buitengebied',
    authRoles: ['buitengebied'],
    tab: 'buitengebied',
    name: 'Module 1. Download basisdataset gehele buitengebied',
    desc: 'In module 1 gaat het om een afgebakend gedeelte van het buitengebied (de ‘hotspot’) of het gehele buitengebied. De keuze voor deze module 1 hangt samen met de concentratie van signalen en meldingen in het gebied. Download de basisdataset voor module 1 bestaande uit data uit de BAG en Handelsregister (KVK).',
    sourceDescription: {
      'BAG verblijfsobject': metaDataList.BAGverblijfsobject,
      KVK: metaDataList.KVK,
      'BAG Download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-08',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'verblijfsobject',
        unit: 'SBI code- hoofdcategorie',
        geomType: 'circle',

        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: ['verblijfsobject', 'standplaats', 'ligplaats'],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#bebada'],
          ordinalDomain: ['vid', 'sid', 'lid'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}
             ${properties.huisnummertoevoeging || ''}
          `,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
      {
        isRelevantLayer: true,
        offset: [15, 0],

        attrName: 'l1_code',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        legendStops: [
          'A. Landbouw, Bosbouw en Visserij',
          'B. Winning van Delfstoffen',
          'C. Industrie',
          'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
          'E. Winning en Distributie van Water',
          'F. Bouwnijverheid',
          'G. Groot- en Detailhandel; Reparatie Van Auto’s',
          'H. Vervoer en Opslag',
          'I. Logies-, Maaltijd- en Drankverstrekking',
          'J. Informatie en Communicatie',
          'K. Financiële Instellingen',
          'L. Verhuur van en Handel in Onroerend Goed',
          'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
          'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
          'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
          'P. Onderwijs',
          'Q. Gezondheids- en Welzijnszorg',
          'R. Cultuur, Sport en Recreatie',
          'S. Overige Dienstverlening',
          'T. Huishoudens als Werkgever',
          'U. Extraterritoriale Organisaties en Lichamen',
        ],

        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: [
            '#70401E',
            '#7FDB23',
            '#b2df8a',
            '#73824F',
            '#33a02c',
            '#043D00',
            '#ff7f00',
            '#e31a1c',
            '#d95f02',
            '#fb9a99',
            '#6a3d9a',
            '#bc80bd',
            '#000000',
            '#666666',
            '#e6ab02',
            '#ffff33',
            '#ffff99',
            '#46bcce',
            '#a6cee3',
            '#0000ff',
            '#377eb8',
          ],
          ordinalDomain: [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
          ],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{l1_code} {sbicode}',
      },
    ],
  },

  {
    id: 'layerDook101buitengebied',
    authRoles: ['buitengebied'],
    tab: 'buitengebied',
    name: 'Module 2. Download basisdataset agrarische branche ',
    desc: 'Module 2 bevat alle agrarische percelen. Agrariërs zijn voornamelijk door nieuwe wet- en regelgeving en de veranderende financiële positie kwetsbaarder geworden. Aan de andere kant gaat het bij deze percelen over de kenmerken en eigenschappen van de bedrijfsuitvoering die gelegenheid bieden voor het opzetten van een opslag- en/of productielocatie van drugs en hierdoor extra kwetsbaar zijn. Ondernemingen in kwetsbare branches binnen de agrarische sector dienen ten minste meegenomen te worden in het informatiebeeld tot en met Informatiefase 2.  Download de basisdataset voor module 2 bestaande uit data uit de BAG en een selectie van risicobranches binnen de agrarische sector uit het Handelsregister (KVK). Zie bijlage 1 in de Handreiking Informatiebeeld Buitengebied voor een overzicht van de betreffende SBI-codes. ',
    sourceDescription: {
      'BAG verblijfsobject': metaDataList.BAGverblijfsobject,
      KVK: metaDataList.KVK,
      'BAG Download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-08',
    drawType: 'count',
    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'verblijfsobject',
        unit: 'BAG type',
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: ['verblijfsobject', 'standplaats', 'ligplaats'],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#bebada'],
          ordinalDomain: ['vid', 'sid', 'lid'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}`,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
      {
        isRelevantLayer: true,
        attrName: 'dossiernr',
        tileSource: sourceSettings.kvk2,
        offset: [15, 0],
        scale: 'verblijfsobject',
        unit: 'SBI code- hoofdcategorie',
        geomType: 'circle',
        legendStops: [
          'Opfokken van jongvee voor de melkveehouderij',
          'Houden van vleeskalveren',
          'Overige vleesveehouderij en zoogkoeienbedrijven',
          'Fokken en houden van paarden en ezels',
          'Fokken en houden van schapen',
          'Fokken en houden van geiten',
          'Fokvarkens- en vermeerderingsbedrijven',
          'Vleesvarkensbedrijven',
          'Gesloten en deels gesloten varkensbedrijven',
          'Opfokken en/of houden van leghennen',
          'Opfokken en/of houden van vleeskuikens',
          'Opfokken en/of houden van ouderdieren van leghennen en vleeskuikens',
          'Opfokken en of houden van overig pluimvee',
          'Fokken en houden van edel pelsdieren',
          'Fokken en houden van overige dieren (rest)',
          'Akker en of tuinbouw in combinatie met het fokken en houden van dieren',
          'Teelt van champignons en andere paddenstoelen',
          'Teelt van groenten onder glas',
          'Teelt van snijbloemen en snijheesters onder glas',
          'Teelt van aardbeien onder glas',
          'Teelt van houtig klein fruit onder gla',
          'Teelt van perkplanten onder glas',
          'Teelt van potplanten onder glas',
          'Paardensport en maneges',
        ],
        customFilter: customFilters.module2 as FilterSpecification,
        color: '#e31a1c',
        popup: ({ properties }): PopupProps => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },

      {
        geomType: 'symbol',
        attrName: '{l1_code} {sbicode}',
      },
    ],
  },
  {
    id: 'layerDook102buitengebied',
    authRoles: ['buitengebied'],
    tab: 'buitengebied',
    name: 'Module 3. Download basisdataset niet-agrarische kwetsbare branches',
    desc: 'In module 3 gaat het specifiek om bedrijven zonder agrarische bestemming in het buitengebied. Bij deze bestemmingen gaat het om de kenmerken en eigenschappen van de bedrijfsuitvoering die gelegenheid bieden voor het opzetten van een opslag- en/of productielocatie van drugs en hierdoor extra kwetsbaar zijn. Ondernemingen in kwetsbare branches binnen de niet-agrarische sectoren dienen ten minste meegenomen te worden in het informatiebeeld tot en met Informatiefase 2.  Download de basisdataset voor module 3 bestaande uit data uit de BAG en een selectie van risicobranches uit de niet-agrarische sector uit het Handelsregister (KVK). Zie bijlage 1 in de Handreiking Informatiebeeld Buitengebied voor een overzicht van de betreffende SBI-codes. ',
    sourceDescription: {
      'BAG verblijfsobject': metaDataList.BAGverblijfsobject,
      KVK: metaDataList.KVK,
      'BAG Download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-08',
    drawType: 'count',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],

      {
        scale: 'verblijfsobject',
        unit: 'BAG type',
        geomType: 'circle',
        attrName: 'postcode',
        tileSource: sourceSettings.verblijfsobjecten,
        expressionType: 'case',
        caseArray: ['vid', 'sid', 'lid'],
        legendStops: ['verblijfsobject', 'standplaats', 'ligplaats'],
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#0b71a1', '#8dd3c7', '#bebada'],
          ordinalDomain: ['vid', 'sid', 'lid'],
        }),
        popup: ({ properties }): PopupProps => ({
          title: 'Adres',
          content: `${properties.openbareruimtenaam} ${
            properties.huisnummer
          }  ${properties.huisletter || ''}`,
        }),
        detailModal: verblijfsobjectenDetailModal,
      },
      {
        isRelevantLayer: true,
        attrName: 'l1_code',
        offset: [15, 0],
        scale: 'verblijfsobject',
        unit: 'SBI code- hoofdcategorie',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        legendStops: [
          'Opfokken van jongvee voor de melkveehouderij',
          'Houden van vleeskalveren',
          'Overige vleesveehouderij en zoogkoeienbedrijven',
          'Fokken en houden van paarden en ezels',
          'Fokken en houden van schapen',
          'Fokken en houden van geiten',
          'Fokvarkens- en vermeerderingsbedrijven',
          'Vleesvarkensbedrijven',
          'Gesloten en deels gesloten varkensbedrijven',
          'Opfokken en/of houden van leghennen',
          'Opfokken en/of houden van vleeskuikens',
          'Opfokken en/of houden van ouderdieren van leghennen en vleeskuikens',
          'Opfokken en of houden van overig pluimvee',
          'Fokken en houden van edel pelsdieren',
          'Fokken en houden van overige dieren (rest)',
          'Akker en of tuinbouw in combinatie met het fokken en houden van dieren',
          'Teelt van champignons en andere paddenstoelen',
          'Teelt van groenten onder glas',
          'Teelt van snijbloemen en snijheesters onder glas',
          'Teelt van aardbeien onder glas',
          'Teelt van houtig klein fruit onder gla',
          'Teelt van perkplanten onder glas',
          'Teelt van potplanten onder glas',
          'Paardensport en maneges',
        ],
        customFilter: customFilters.module3 as FilterSpecification,
        color: '#e31a1c',
        popup: ({ properties }): PopupProps => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{l1_code} {sbicode}',
      },
    ],
  },

  {
    id: 'layerDook101',
    authRoles: ['autobranche'],
    tab: 'kvk',
    name: 'Handelsregister',
    desc: 'nvt',
    sourceDescription: {
      KVK: metaDataList.KVK,
    },
    lastUpdate: '2024-10',
    layerType: 'sbi-codes',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'verblijfsobject',
        unit: 'SBI code- hoofdcategorie',
        isRelevantLayer: true,
        attrName: 'l1_code',
        tileSource: sourceSettings.kvk2,
        geomType: 'circle',
        legendStops: [
          'A. Landbouw, Bosbouw en Visserij',
          'B. Winning van Delfstoffen',
          'C. Industrie',
          'D. Elektriciteit, Aardgas, Stoom en Gekoelde Lucht',
          'E. Winning en Distributie van Water',
          'F. Bouwnijverheid',
          'G. Groot- en Detailhandel; Reparatie Van Auto’s',
          'H. Vervoer en Opslag',
          'I. Logies-, Maaltijd- en Drankverstrekking',
          'J. Informatie en Communicatie',
          'K. Financiële Instellingen',
          'L. Verhuur van en Handel in Onroerend Goed',
          'M. Advisering, Onderzoek en Overige Specialistische Zakelijke Dienstverlening',
          'N. Verhuur Van Roerende Goederen en Overige Zakelijke Dienstverlening',
          'O. Openbaar Bestuur, Overheidsdiensten en Verplichte Sociale Verzekeringen',
          'P. Onderwijs',
          'Q. Gezondheids- en Welzijnszorg',
          'R. Cultuur, Sport en Recreatie',
          'S. Overige Dienstverlening',
          'T. Huishoudens als Werkgever',
          'U. Extraterritoriale Organisaties en Lichamen',
        ],

        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: [
            '#70401E',
            '#7FDB23',
            '#b2df8a',
            '#73824F',
            '#33a02c',
            '#043D00',
            '#ff7f00',
            '#e31a1c',
            '#d95f02',
            '#fb9a99',
            '#6a3d9a',
            '#bc80bd',
            '#000000',
            '#666666',
            '#e6ab02',
            '#ffff33',
            '#ffff99',
            '#46bcce',
            '#a6cee3',
            '#0000ff',
            '#377eb8',
          ],
          ordinalDomain: [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
          ],
        }),
        popup: ({ properties }): PopupProps => ({
          title: `KVK dossiernr  ${properties.dossiernr}`,
          content: `${properties.hoofdact} - ${properties.hn_1x45} - ${properties.l1_title}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `KVK dossiernr  ${properties.dossiernr}`,
          urlTitle: 'Handelsregister informatie',
          url: `https://www.kvk.nl/bestellen/#/${properties.dossiernr}${properties.vgnummer}`,
          content: [
            ['Handelsnaam', properties.hn_1x45],
            ['Hoofdactiviteit', properties.hoofdact],
            ['Nevenactiviteit 1', properties.nevact1],
            ['Nevenactiviteit 2', properties.nevact2],
            ['Hoofd thema', `${properties.l1_code}. ${properties.l1_title}`],
            ['Thema 2', `${properties.l2_title}`],
            ['Thema 3', `${properties.l3_title}`],
            ['Thema 4', `${properties.l4_title}`],
            ['Thema 5', `${properties.l5_title}`],
            [
              'Adres',
              `${properties.postcode}
              ${properties.huisnr}
              ${properties.huisletter || ''}
              ${properties.toev_hsnr || ''}
              `,
            ],
            ['KVK dossiernumer', properties.dossiernr],
            ['Pand id', properties.pid],
            ['Verblijfsobject id', properties.vid],
          ],
        }),
      },
      {
        geomType: 'symbol',
        attrName: '{l1_code} {sbicode}',
      },
    ],
  },
]

export const degoDataLayers: Partial<DataLayerProps>[] = [
  //
  // no layer selected
  //
  staticBuildingsLayer,
  //
  // gebouw
  //
  {
    id: 'layer1',
    tab: 'gebouw',
    name: 'Bouwjaar',
    desc: 'Het bouwjaar geeft een indicatie van de energetische kwaliteit en technische haalbaarheid: nieuwere objecten zijn relatief eenvoudiger geschikt te maken voor aardgasvrij verwarmen dan oudere (voor 1945), die vaak lastiger en duurder na te isoleren zijn.',
    sourceDescription: {
      'BAG, in viewer': metaDataList.BAG,
      'BAG, in download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-07-04',
    drawType: 'count',
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Bouwjaar',
        expressionType: 'interpolate',
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [1900, 1930, 1945, 1960, 1990, 1998, 2023],
        noDataValue: [1005, 9999, 0],
        firstLabel: 'voor 1900',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c', '#F1E7AA'],
          domain: [1500, 1900, 1945, 1995, 2015],
        }),
        popup: ({ properties: { bouwjaar } }) => ({
          title: `Bouwjaar ${bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score_2022
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer1b',
    tab: 'gebouw',
    name: 'Bouwjaar op categorie',
    desc: 'Het bouwjaar geeft een indicatie van de energetische kwaliteit en technische haalbaarheid: nieuwere objecten zijn relatief eenvoudiger geschikt te maken voor aardgasvrij verwarmen dan oudere (voor 1945), die vaak lastiger en duurder na te isoleren zijn.',
    sourceDescription: {
      'BAG, Bouwjaar in viewer': metaDataList.BAG,
      'BAG, in download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Bouwjaar',
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: ['voor 1900', '1900-1944', '1945-1994', 'na 1995'],
        noDataValue: [1005, 0],
        expressionType: 'step',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#1C2832', '#395065', '#48bf8e', '#dec42c'],
          domain: [1000, 1900, 1945, 1995],
        }),
        popup: ({ properties: { bouwjaar } }) => ({
          title: `Bouwjaar ${bouwjaar}`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer2c',
    tab: 'gebouw',
    name: 'Energie Klasse',
    desc: 'Energieklasse geeft een indicatie van de isolatie en benodigde na-isolatie voor een warmte-oplossing. Het is betrouwbaarder dan het voorlopige energielabel, maar niet altijd beschikbaar. Op de kaart zijn labels een gemiddelde, in de download zitten alle labels',
    note: 'Van 32% van de panden is een energieklasse geregistreerd',
    sourceDescription: {
      'RVO, Energielabels': metaDataList.RVO,
      'BAG, in viewer': metaDataList.BAG,
    },
    lastUpdate: '2024-07-04',
    drawType: 'count',
    styleLayers: [
      {
        scale: 'Pand',
        unit: `Energielabel klassen -
        Warmtevraag kWh/(m2.jaar)`,
        expressionType: 'match',
        tileSource: sourceSettings.bag,
        attrName: 'energieklasse_score',
        geomType: 'fill',
        legendStops: [
          'A++++ ( < 0 )',
          'A+++ ( 1 - 50 )',
          'A++ ( 51 - 80 )',
          'A+ ( 81 - 110 )',
          'A ( 111 - 165 )',
          'B ( 166 - 195 )',
          'C ( 196 - 255 )',
          'D ( 256 - 300 )',
          'E ( 301 - 345 )',
          'F ( 346 - 390 )',
          'G ( 391 < )',
        ],
        filterAttrb: 'energieklasse_score',
        filterNames: [
          'A++++',
          'A+++',
          'A++',
          'A+',
          'A',
          'B',
          'C',
          'D',
          'E',
          'F',
          'G',
        ],
        filterValues: [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
        colorScale: createColorScale({
          type: 'linear',
          colorRange: [
            '#009342',
            '#009342',
            '#009342',
            '#009342',
            '#009342',
            '#1ba943',
            '#9ecf1b',
            '#f8f51c',
            '#f4b003',
            '#df6d14',
            '#db261d',
          ],
          domain: [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
        }),
        extrusionAttr: 'hoogte',

        popup: ({ properties }): PopupProps => ({
          title: 'Energielabel klasse:',
          content: energyLabelClasses[properties.energieklasse_score],
        }),

        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer4',
    tab: 'gebouw',
    name: 'Woonfunctie',
    desc: 'De Woonfunctie geeft een indicatie van het woninggebruik in een object. We onderscheiden 3 categorieën: objecten met alleen een woonfunctie, objecten met gedeeltelijke woonfunctie, objecten zonder woonfunctie.',
    note: '100 = wonen , 0 = niet wonen',
    sourceDescription: {
      'BAG, woonfunctie per VBO': metaDataList.BAG,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Indicatie woonfunctie',
        attrName: 'woonfunctie_score',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: [
          'Geen woonfunctie',
          'Verschillende functies(waaronder wonen)',
          'Uitsluitend woonfunctie',
        ],
        expressionType: 'step',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#052f68', '#797673', '#f1d751'],
          domain: [0, 1, 99, 100],
        }),
        extrusionAttr: 'hoogte',
        popup: ({ properties }): PopupProps => ({
          title:
            properties.woonfunctie_score === 0
              ? 'Geen woonfunctie'
              : properties.woonfunctie_score === 100
              ? 'Uitsluitend woonfunctie'
              : `Verschillende functies (waaronder wonen)`,
        }),
        detailModal: ({ properties }) => ({
          title: 'Details Pand',
          type: 'building',
          content: [
            properties.bouwjaar ? ['Bouwjaar', properties.bouwjaar] : null,

            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woonfunctie_score >= 0
              ? [
                  'Woonfunctie',
                  properties.woonfunctie_score === 0
                    ? 'Geen woonfunctie'
                    : properties.woonfunctie_score === 100
                    ? 'Alleen woonfunctie'
                    : `Verschillende functies (waaronder wonen)`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer3b',
    tab: 'gebouw',
    name: 'Gemiddelde WOZ-waarde',
    desc: `De WOZ-geeft een indicatie van de waarde van de objecten in een buurt.`,
    sourceDescription: {
      'BAG, woonfunctie per VBO': metaDataList.BAG,
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Gemeente, wijk, buurt, postcode-6 ',
        unit: 'x1000€',
        opacity: 0.9,
        geomType: 'fill',
        attrName: 'woz',
        isRelevantLayer: true,
        tileSource: sourceSettings.bag,
        noDataValue: [-1],
        legendStops: [4, 185, 290, 340, 500, 655, 850],
        lastLabel: '850 <',
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'inferno',
          domain: [105, 291, 850],
        }),
        popup: ({ properties: { woz } }) => ({
          title: `${woz}x1000€`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.postcode}`,
          desc: '',
          content: [['gem. woz waarde', `${properties.woz}x1000€`]],
        }),
      },
      {
        opacity: 0.4,
        attrName: 'woz',
        noLegend: true,
        tileSource: sourceSettings.bag,
        'source-layer': 'pc6group_2024_cbs_2022',
        noDataValue: [-1],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'inferno',
          domain: [105, 291, 850],
        }),
      },
      {
        'source-layer': 'pc6group_2024_cbs_2022',
        tileSource: sourceSettings.bag,
        attrName: 'postcode',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        customPaint: customPaint.hoverOutlines,
        legendStops: ['Postcodegebied'],
      },
    ],
  },
  {
    id: 'layer3c',
    tab: 'gebouw',
    name: 'Eigendomssituatie',
    desc: 'Deze kaart geeft inzicht in  het percentage koop- en huurwoningen in een gebied. Door te klikken op de woning zie je ook het percentage in bezit van een woningcorporatie (let op: dit is niet hetzelfde als het aantal sociale huurwoningen, omdat er alleen is vastgesteld wie de eigenaar is en er niet is gekeken naar de hoogte van de huurprijs)',
    sourceDescription: {
      'BAG, woonfunctie per VBO': metaDataList.BAG,
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    sublayers: [
      {
        id: 'layer3c_1',
        name: 'Percentage koopwoningen',
        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6 ',
            opacity: 0.95,
            isRelevantLayer: true,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_koopwoningen } }) => ({
              title: `Koopwoningen`,
              content: `${percentage_koopwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'eg',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            isRelevantLayer: true,
            noDataValue: [-1],
            noLegend: true,
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
          },
          {
            'source-layer': 'pc6group_2024_cbs_2022',
            tileSource: sourceSettings.bag,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer3c_2',
        name: 'Percentage huurwoningen',

        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6',
            isRelevantLayer: true,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            expressionType: 'interpolate',
            noDataValue: [-1],
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_huurwoningen } }) => ({
              title: `Huurwoningen`,
              content: `${percentage_huurwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'eg',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            noLegend: true,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
          },
          {
            'source-layer': 'pc6group_2024_cbs_2022',
            tileSource: sourceSettings.bag,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
    ],
  },
  {
    id: 'protectedLayer4',
    tab: 'gebouw',
    name: 'Pilot Eigendoms gegevens',
    authRoles: ['energie'],
    authLocked: true,
    desc: 'Eigendoms data Apeldoorn',
    sourceDescription: {
      'Eigendomsdata Apeldoorn': metaDataList.Eigendom,
    },

    lastUpdate: '2021-05-19',
    styleLayers: [
      {
        unit: 'Type eigenaar',
        scale: 'Pand',
        attrName: 'type_eigenaar',
        tileSource: sourceSettings.eigendom,
        'source-layer': 'eigenaren',
        legendStops: [
          'Grote part verhuurder',
          'Kleine part verhuurder',
          'Woningcorporatie',
          'Overig',
        ],
        expressionType: 'match',
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#1b9e77', '#d95f02', '#7570b3', '#e7298a'],
          ordinalDomain: [
            'Grote part verhuurder',
            'Kleine part verhuurder',
            'Woningcorporatie',
            'Overig',
          ],
        }),
        popup: ({
          properties: {
            naam_niet_natuurlijk_persoon,
            openbareruimtenaam,
            type_eigenaar,
            woning_type,
          },
        }) => ({
          title: `${type_eigenaar} - ${woning_type}`,
          content: `${naam_niet_natuurlijk_persoon} , ${openbareruimtenaam}`,
        }),
        extrusionAttr: 'hoogte',
      },
    ],
  },
  {
    id: 'layer206',
    authLocked: true,
    authRoles: ['energie'],
    tab: 'gebouw',
    name: 'Grote Eigenaren',
    desc: 'Deze kaart toont het percentage verblijfsobjecten in bezit van de 7 grootste eigenaren per wijk/buurt. Een hoog percentage betekent dat een beperkt aantal eigenaren een groot invloed kan hebben op de transitie in de wijk/buurt. Bij klikken verschijnt een pop-up met eigenaren; zakelijke eigenaren worden genoemd, particuliere staan als ‘Particuliere eigenaar’.',
    sourceDescription: {
      CBS: metaDataList.CBSEigenaren,
    },
    lastUpdate: '2021-10',

    styleLayers: [
      {
        unit: '%',
        scale: 'Wijk, buurt',
        tileSource: sourceSettings.eigenaren,
        'source-layer': 'eigenarentop7',
        legendStops: [0, 15, 20, 50, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        attrName: 'grooteigenaarpct',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [0, 100],
        }),
        opacity: 0.8,

        popup: ({ properties: { grooteigenaarpct, name, code } }) => ({
          title: `${name} - ${code}`,
          content: `${grooteigenaarpct}%`,
        }),

        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Top 7 grote eigenaren van ${properties.name}`,
          desc: properties.code,
          content: [
            properties.naam_1 === 'leeg'
              ? null
              : [`1. ${properties.naam_1}`, `${properties.aantal_vbo_1}`],
            properties.naam_2 === 'leeg'
              ? null
              : [`2. ${properties.naam_2}`, `${properties.aantal_vbo_2}`],
            properties.naam_3 === 'leeg'
              ? null
              : [`3. ${properties.naam_3}`, `${properties.aantal_vbo_3}`],
            properties.naam_4 === 'leeg'
              ? null
              : [`4. ${properties.naam_4}`, `${properties.aantal_vbo_4}`],
            properties.naam_5 === 'leeg'
              ? null
              : [`5. ${properties.naam_5}`, `${properties.aantal_vbo_5}`],
            properties.naam_6 === 'leeg'
              ? null
              : [`6. ${properties.naam_6}`, `${properties.aantal_vbo_6}`],
            properties.naam_7 === 'leeg'
              ? null
              : [`7. ${properties.naam_7}`, `${properties.aantal_vbo_7}`],

            ['Totaal aantal verblijfsobjecten', `${properties.aantal_vbo}`],
            [
              'Percentage verblijfsobjecten in bezit grote eigenaren',
              `${properties.grooteigenaarpct} %`,
            ],
          ],
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  //
  // spuk
  //

  {
    id: 'layer51',
    tab: 'spuk',
    name: 'Slecht geïsoleerde woning - Energielabelklasse  D, E, F of G ',
    desc: `Een slecht geïsoleerde woning is een woning met energielabel D, E, F of G.</br></br>
    Energieklasse geeft een indicatie van hoe goed een object geisoleerd is, en daarmee ook een eerste indicatie van hoeveel er nog nageisoleerd moet worden om het object geschikt te maken voor een bepaalde warmte-oplossing. Het is betrouwbaarder dan het voorlopige energielabel, maar niet voor alle objecten beschikbaar.
    </br>Download hier alle woningen, in de download is alle informatie per adres beschikbaar.
    <span style='color:red;'> Let op!  </span>Op basis van dit inzicht, kan je niet direct de uitkering aanvragen. De data is niet voldoende gedetailleerd. Je zult altijd zelf meer onderzoek moeten doen!`,
    note: 'Van 32% van de panden is een energieklasse geregistreerd',
    sourceDescription: {
      BAG: metaDataList.BAG,
      RVO: metaDataList.RVO,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: `Energielabel klassen -
    Warmtevraag kWh/(m2.jaar)`,
        filterAttrb: 'energieklasse_score',
        filterNames: ['D', 'E', 'F', 'G'],
        filterValues: [4, 3, 2, 1],
        expressionType: 'match',
        tileSource: sourceSettings.bag,
        attrName: 'energieklasse_score',
        geomType: 'fill',
        noDataValue: [5, 6, 7, 8, 9, 10, 11, 12],
        legendStops: [
          'D ( 256 - 300 )',
          'E ( 301 - 345 )',
          'F ( 346 - 390 )',
          'G ( 391 < )',
        ],
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#f8f51c', '#f4b003', '#df6d14', '#db261d'],
          domain: [4, 3, 2, 1],
        }),
        extrusionAttr: 'hoogte',

        popup: ({ properties }): PopupProps => ({
          title: 'Energielabel klasse:',
          content: energyLabelClasses[properties.energieklasse_score],
        }),

        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Details Pand en ${properties.postcode}`,
          content: [
            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woz
              ? ['Gemiddelde WOZ waarde ', properties.woz + ' x1000€']
              : null,
            properties.gemeente_woz
              ? ['Gem WOZ waarde gemeente', properties.gemeente_woz + ' x1000€']
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
    drawType: 'count',
  },
  {
    id: 'layer52',
    tab: 'spuk',
    name: 'Gemiddelde WOZ-waarde',
    desc: `Bekijk hier ter indicatie de gemiddelde WOZ waarde per postcode gebied die lager is dan de gemiddelde WOZ waarde van de betreffende gemeente, of lager dan € 429.000. </br></br>
    Voor de aanvraag van Spuk moet minimaal 80% van de slecht geïsoleerde woningen die u isoleert een lagere WOZ-waarde dan de gemiddelde WOZ-waarde van koopwoningen in uw gemeente hebben. Of de WOZ-waarde is lager dan € 429.300.
    Let op: deze data gaat over alle woningen.`,
    sourceDescription: {
      BAG: metaDataList.BAG,
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    sublayers: [
      {
        id: 'layer52_1',
        name: 'Gemiddelde WOZ waarde lager dan gemiddelde WOZ waarde gemeente',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt, postcode-6 ',
            unit: 'x1000€',
            opacity: 0.9,
            attrName: 'woz',
            isRelevantLayer: true,
            tileSource: sourceSettings.bag,
            customFilter: customFilters.spukwoz as FilterSpecification,
            noDataValue: [-1],
            legendStops: [4, 185, 290, 340, 500, 655, 850],
            lastLabel: '850 <',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'diverging',
              color: 'inferno',
              domain: [105, 291, 850],
            }),
            popup: ({ properties: { woz, gemeente_woz, postcode } }) => ({
              title: `Gem WOZ gemeente: ${gemeente_woz}x1000€ `,
              content: `Gem WOZ ${postcode}: ${woz}x1000€ `,
            }),
          },
          {
            opacity: 0.4,
            attrName: 'woz',
            customFilter: customFilters.spukwoz as FilterSpecification,
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            legendStops: [4, 185, 290, 340, 500, 655, 850],
            noDataValue: [-1],
            noLegend: true,
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'diverging',
              color: 'inferno',
              domain: [105, 291, 850],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer52_2',
        name: 'WOZ waarde lager dan € 429.000',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt, postcode-6 ',
            unit: 'x1000€',
            opacity: 0.9,
            attrName: 'woz',
            isRelevantLayer: true,
            tileSource: sourceSettings.bag,
            customFilter:
              customFilters.spukwoz2 as ExpressionFilterSpecification,
            noDataValue: [-1],
            legendStops: [4, 185, 290, 340, 500, 655, 850],
            lastLabel: '850 <',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'diverging',
              color: 'inferno',
              domain: [105, 291, 850],
            }),
            popup: ({ properties: { woz, gemeente_woz, postcode } }) => ({
              title: `Gem WOZ gemeente: ${gemeente_woz}x1000€ `,
              content: `Gem WOZ ${postcode}: ${woz}x1000€ `,
            }),
          },
          {
            opacity: 0.4,
            attrName: 'woz',
            customFilter:
              customFilters.spukwoz2 as ExpressionFilterSpecification,
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            legendStops: [4, 185, 290, 340, 500, 655, 850],
            noLegend: true,
            noDataValue: [-1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'diverging',
              color: 'inferno',
              domain: [105, 291, 850],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
    ],
  },
  {
    id: 'layer53',
    tab: 'spuk',
    name: 'Eigendomssituatie',
    desc: `
    Hier zie je hoeveel procent van de woningen in een gebied een koopwoning is en hoeveel procent een huurwoning is. Door op het gebied te klikken is ook te zien welk percentage van de woningen in eigendom is van een woningcorporatie (let op: dit is niet hetzelfde als het aantal sociale huurwoningen, omdat er alleen is vastgesteld wie de eigenaar is en er niet is gekeken naar de hoogte van de huurprijs)
    </br></br>
    De Spuk voorwaarden vraag dat de slecht geïsoleerde woning eigendom is van een eigenaar-bewoner. Of de woning is eigendom van een lid van een Vereniging van Eigenaar (VvE), wooncoöperatie of woonvereniging. Ten minste één woning in het gebouw moet eigendom zijn van een eigenaar-bewoner.
    `,
    sourceDescription: {
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    sublayers: [
      {
        id: 'layer53_1',
        name: 'Percentage koopwoningen',
        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6',
            opacity: 0.95,
            isRelevantLayer: true,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_koopwoningen } }) => ({
              title: `Koopwoningen`,
              content: `${percentage_koopwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            noLegend: true,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            isRelevantLayer: true,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-99999999, -99999998, -1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            noDataValue: [-1],
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer53_2',
        name: 'Percentage huurwoningen',
        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6',
            isRelevantLayer: true,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_huurwoningen } }) => ({
              title: `Huurwoningen`,
              content: `${percentage_huurwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-99999999, -99999998, -1],
            noLegend: true,

            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer53_3',
        name: 'Aantal woningen in bezit woning corporatie',
        styleLayers: [
          {
            unit: '',
            scale: 'postcode-6',
            isRelevantLayer: true,
            attrName: 'aantal_huurwoningen_in_bezit_woningcorporaties',
            tileSource: sourceSettings.bag,
            noDataValue: [-1],
            legendStops: [0, 40, 60, 70, 90, 150],
            lastLabel: ' <  150',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 200],
            }),
            popup: ({
              properties: { aantal_huurwoningen_in_bezit_woningcorporaties },
            }) => ({
              title: `aantal_huurwoningen_in_bezit_woningcorporaties`,
              content: `${aantal_huurwoningen_in_bezit_woningcorporaties} `,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'aantal_huurwoningen_in_bezit_woningcorporaties',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            noLegend: true,
            legendStops: [0, 40, 60, 70, 90, 150],
            noDataValue: [-99999999, -99999998, -1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 1000],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
    ],
  },
  {
    id: 'layer54',
    tab: 'spuk',
    name: 'Mogelijke locaties',
    desc: `Bijkijk hier de objecten die voloen aan de volgende 2 voorwaarden:</br>
    1. een energielabel D, E, F of G. </br>
    2. de gemiddelde WOZ waarde per postcode gebied die lager is dan de gemiddelde WOZ waarde van de betreffende gemeente,
    </br></br>
    <span style='color:red;'> Let op!  </span>Op basis van dit inzicht, kan je niet direct de uitkering aanvragen. De data is niet voldoende gedetailleerd. Je zult altijd zelf meer onderzoek moeten doen!`,
    sourceDescription: {
      'RVO, Energielabels': metaDataList.RVO,
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: `Overzicht mogelijke adressen`,
        expressionType: 'match',
        tileSource: sourceSettings.bag,
        attrName: 'energieklasse_score',
        geomType: 'fill',
        legendStops: ['voldoet mogelijk'],
        customFilter: customFilters.spukDownload as FilterSpecification,
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#0b71a1'],
          domain: [4, 3, 2, 1],
        }),
        extrusionAttr: 'hoogte',
        popup: ({ properties }): PopupProps => ({
          title: 'Energielabel klasse:',
          content: energyLabelClasses[properties.energieklasse_score],
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Details Pand en ${properties.postcode}`,
          content: [
            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woz
              ? ['Gemiddelde WOZ waarde ', properties.woz + ' x1000€']
              : null,
            properties.gemeente_woz
              ? ['Gem WOZ waarde gemeente', properties.gemeente_woz + ' x1000€']
              : null,
          ],
        }),
      },
    ],
  },

  //
  // Vleermuis.
  //
  {
    id: 'layer51vleermuis',
    tab: 'vleermuis',
    name: 'Woningen met energielabel C of lager',
    desc: `Deze kaartlaag sluit woningen die waarschijnlijk al geïsoleerd zijn uit en toont hierdoor woningen die mogelijk onderdeel zijn van de compensatieopgave.`,
    note: 'Van 32% van de panden is een energieklasse geregistreerd',
    sourceDescription: {
      BAG: metaDataList.BAG,
      RVO: metaDataList.RVO,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: `Energielabel klassen - Warmtevraag kWh/(m2.jaar)`,
        filterAttrb: 'energieklasse_score',
        filterNames: ['C', 'D', 'E', 'F', 'G', 'Onbekend'],
        filterValues: [5, 4, 3, 2, 1, -1],
        expressionType: 'match',
        tileSource: sourceSettings.bag,
        attrName: 'energieklasse_score',
        geomType: 'fill',
        noDataValue: [6, 7, 8, 9, 10, 11, 12],
        legendStops: [
          'C (196 - 255)',
          'D (256 - 300)',
          'E (301 - 345)',
          'F (346 - 390)',
          'G (391 <)',
          '? (Onbekend)',
        ],
        colorScale: createColorScale({
          type: 'linear',
          colorRange: [
            '#9ECF1B',
            '#F8F51C',
            '#F4B003',
            '#DF6D14',
            '#DB261D',
            '#365D71',
          ],
          domain: [5, 4, 3, 2, 1, -1],
        }),
        extrusionAttr: 'hoogte',

        popup: ({ properties }): PopupProps => ({
          title: 'Energielabel klasse:',
          content:
            energyLabelClasses[properties.energieklasse_score] ?? 'Onbekend',
        }),

        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Details Pand en ${properties.postcode}`,
          content: [
            properties.energieklasse_score
              ? [
                  'Energie klasse',
                  energyLabelClasses[properties.energieklasse_score],
                ]
              : null,
            properties.woz
              ? ['Gemiddelde WOZ waarde ', properties.woz + ' x1000€']
              : null,
            properties.gemeente_woz
              ? ['Gem WOZ waarde gemeente', properties.gemeente_woz + ' x1000€']
              : null,
          ],
        }),
      },
      noDataStyleLayer,
    ],
    // drawType: 'count',
  },
  {
    id: 'layer1bvleermuis',
    tab: 'vleermuis',
    name: 'Bouwjaar voor 1993',
    desc: 'Deze kaartlaag sluit woningen die volgens moderne bouwnormen gebouwd zijn uit. Dit zijn gebouwen die vrijwel zeker zijn opgeleverd volgens de wettelijke normen vastgesteld in 1990. De overgebleven gebouwen zijn mogelijk onderdeel van de compensatieopgave.',
    sourceDescription: {
      'BAG, Bouwjaar in viewer': metaDataList.BAG,
      'BAG, in download': metaDataList.BAGdownload,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Bouwjaar',
        attrName: 'bouwjaar',
        tileSource: sourceSettings.bag,
        geomType: 'fill',
        legendStops: ['Onbekend', 'voor 1993', 'na 1993'],
        noDataValue: [1005, 0],
        expressionType: 'step',
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#999999', '#48BF8E', '#395065'],
          domain: [0, 1100, 1993],
        }),
        popup: ({ properties: { bouwjaar } }) => ({
          title: `Bouwjaar ${bouwjaar}`,
        }),
      },
      noDataStyleLayer,
    ],
  },
  {
    id: 'layer53vleermuis',
    tab: 'vleermuis',
    name: 'Mate van koopwoningen',
    desc: `De tijdelijke regeling is van toepassing op woningen in particulier bezit, niet op woningen van woningcorporaties. Daarom zijn gebieden met een hoge graad koopwoningen vormen de gebieden om te voorzien van verblijfplaatsvoorzieningen vanwege de compensatie opgave.`,
    sourceDescription: {
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-07-04',
    sublayers: [
      {
        id: 'layer53vleermuis_1',
        name: 'Percentage koopwoningen',
        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6',
            opacity: 0.95,
            isRelevantLayer: true,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_koopwoningen } }) => ({
              title: `Koopwoningen`,
              content: `${percentage_koopwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            noLegend: true,
            attrName: 'percentage_koopwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            isRelevantLayer: true,
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-99999999, -99999998, -1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            noDataValue: [-1],
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer53vleermuis_2',
        name: 'Percentage huurwoningen',
        styleLayers: [
          {
            unit: '%',
            scale: 'Gemeente, wijk, buurt, postcode-6',
            isRelevantLayer: true,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            legendStops: [0, 40, 60, 70, 90, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
            popup: ({ properties: { percentage_huurwoningen } }) => ({
              title: `Huurwoningen`,
              content: `${percentage_huurwoningen} %`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'percentage_huurwoningen',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            legendStops: [0, 40, 60, 70, 90, 100],
            noDataValue: [-99999999, -99999998, -1],
            noLegend: true,

            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [0, 100],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
      {
        id: 'layer53vleermuis_3',
        name: 'Aantal woningen in bezit woning corporatie',
        styleLayers: [
          {
            unit: '',
            scale: 'postcode-6',
            isRelevantLayer: true,
            attrName: 'aantal_huurwoningen_in_bezit_woningcorporaties',
            tileSource: sourceSettings.bag,
            noDataValue: [-1],
            legendStops: [0, 40, 60, 70, 90, 150],
            lastLabel: ' <  150',
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 200],
            }),
            popup: ({
              properties: { aantal_huurwoningen_in_bezit_woningcorporaties },
            }) => ({
              title: `aantal_huurwoningen_in_bezit_woningcorporaties`,
              content: `${aantal_huurwoningen_in_bezit_woningcorporaties} `,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Detail postcode gebied ${properties.postcode}`,
              desc: '',
              content: [
                properties.woningvoorraad && [
                  'Aantal woningen',
                  properties.woningvoorraad,
                ],
                [
                  'Koopwoningen',
                  properties.percentage_koopwoningen >= 0
                    ? properties.percentage_koopwoningen + '%'
                    : properties.percentage_huurwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen',
                  properties.percentage_huurwoningen >= 0
                    ? properties.percentage_huurwoningen + '%'
                    : properties.percentage_koopwoningen === 100
                    ? '0%'
                    : '-',
                ],
                [
                  'Huurwoningen in bezit corporatie ',
                  properties.aantal_huurwoningen_in_bezit_woningcorporaties >= 0
                    ? properties.aantal_huurwoningen_in_bezit_woningcorporaties
                    : '-',
                ],
                properties.aantal_niet_bewoonde_woningen && [
                  'Niet bewoonde woningen',
                  properties.aantal_niet_bewoonde_woningen,
                ],
                properties.aantal_inwoners && [
                  'Aantal inwoners',
                  properties.aantal_inwoners,
                ],
              ],
            }),
          },
          {
            opacity: 0.3,
            attrName: 'aantal_huurwoningen_in_bezit_woningcorporaties',
            tileSource: sourceSettings.bag,
            'source-layer': 'pc6group_2024_cbs_2022',
            noLegend: true,
            legendStops: [0, 40, 60, 70, 90, 150],
            noDataValue: [-99999999, -99999998, -1],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 1000],
            }),
          },
          {
            'source-layer': 'cbs_pc6_2023_cbs_2022',
            tileSource: sourceSettings.cbs2022pc6,
            attrName: 'postcode',
            isHighlightLayer: true,
            geomType: 'line',
            color: '#FFBC2C',
            customPaint: customPaint.hoverOutlines,
            legendStops: ['Postcodegebied'],
          },
        ],
      },
    ],
  },
  {
    id: 'layer54vleermuis',
    tab: 'vleermuis',
    name: 'Compensatiegebieden (gecombineerd)',
    desc: `Deze kaartlaag combineert de overige kaartlagen om gebieden op voorhand te kunnen onderscheiden die wel of niet geschikter zijn voor het plaatsen van verblijfplaatsvoorzieningen.<br>
    <br>
    Bekijk hier de objecten die voldoen aan de volgende 3 voorwaarden:<br>
    1. Een energielabel C, D, E, F of G. of geen. <br>
    2. Een bouwjaar lager dan 1993 <br>
    3. Percentage Koopwoningen > 80% <br>
    `,
    // <br><br>
    //     <span style='color:red;'> Let op!  </span>Op basis van dit inzicht, kan je niet direct de uitkering aanvragen. De data is niet voldoende gedetailleerd. Je zult altijd zelf meer onderzoek moeten doen!
    sourceDescription: {
      'RVO, Energielabels': metaDataList.RVO,
      'CBS, Kerncijfers per postcode': metaDataList.CBSpc6,
    },
    lastUpdate: '2024-11-11',
    styleLayers: [
      {
        scale: '%',
        unit: 'Percentage koopwoningen (PC-6 gemiddelde)',
        tileSource: sourceSettings.bag,
        attrName: 'percentage_koopwoningen',
        geomType: 'fill',
        customFilter: customFilters.vleermuisDownload as FilterSpecification,

        legendStops: [50, 100],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [50, 100],
        }),

        extrusionAttr: 'hoogte',
        popup: ({ properties }: DetailViewParams): PopupProps => ({
          title: 'Eigenschappen:',
          // content: energyLabelClasses[properties.energieklasse_score],
          content: `<table style="white-space: nowrap">
            <tr><td>Hoogte</td><td>${properties.hoogte}m</td></tr>
            <tr><td>Energieklasse</td><td>${
              energyLabelClasses[properties.energieklasse_score] ?? 'Onbekend'
            }</td></tr>
            <tr><td>PC-6 Koopwoningen</td><td>${
              properties.percentage_koopwoningen
                ? properties.percentage_koopwoningen + '%'
                : 'Onbekend'
            }</td></tr>
          </table>
          `,
        }),
      },
      {
        opacity: 0.6,
        noLegend: true,
        attrName: 'percentage_koopwoningen',
        tileSource: sourceSettings.bag,
        'source-layer': 'pc6group_2024_cbs_2022',
        customFilter: customFilters.vleermuisPostcode as FilterSpecification,
        isRelevantLayer: true,
        maxzoom: 13,
        legendStops: [0, 40, 60, 70, 90, 100],
        noDataValue: [-99999999, -99999998, -1],
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [50, 100],
        }),
      },
    ],
  },

  //
  // energie
  //
  {
    id: 'layer21',
    tab: 'energie',
    name: 'Gasverbruik 2023',
    desc: 'Het gasverbruik toont hoeveel aardgas een object verbruikt, waarmee je de CO2-reductie kunt berekenen bij overgang naar aardgasvrije verwarming. Het wordt weergegeven voor een postcodegebied en alleen als pand een  gasaansluiting heeft.',
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        average: 1511,
        scale: 'per postcode 6 gebied',
        unit: 'Gemiddelde m3/jaar',
        legendStops: [0, 750, 1210, 1700, 3500, 5000],
        isRelevantLayer: true,
        expressionType: 'interpolate',
        attrName: 'p6_gasm3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        lastLabel: '< 5000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 1511, 3400],
        }),
        popup: ({ properties: { p6_gasm3 } }) => ({
          title: `${p6_gasm3} m3/jaar`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.g_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6gas_2023',
        tileSource: sourceSettings.postcode_gas,
        attrName: 'g_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer22',
    tab: 'energie',
    name: 'Elektriciteitsverbruik 2023',
    desc: 'Het energieverbruik geeft aan hoeveel energie (in kWh) een object nu verbruikt. Daarmee kun je onder andere bepalen of het object gemiddeld meer of minder energie verbruikt. Het energieverbruik wordt getoond per postcode gebied.',
    note: `Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen.

    Vanaf 2019 wordt de zelf opgewekte energie bij het energieverbruik opgeteld i.t.t. de vorige jaren, en zijn de waardes dus hoger!`,
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'per postcode 6 gebied',
        unit: 'Gemiddelde kWh',
        average: 4239,
        expressionType: 'interpolate',
        legendStops: [0, 750, 2000, 3000, 4000, 5000, 6000, 7000, 10000],
        attrName: 'p6_kwh',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        lastLabel: '< 10000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'red',
          domain: [0, 4239, 10000],
        }),
        popup: ({ properties: { p6_kwh } }) => ({
          title: `${p6_kwh} kWh`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.e_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_kwh_productie >= 0 && [
              'Elektriciteitsexport',
              `${properties.p6_kwh_productie} KWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],

            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6elk_2023',
        tileSource: sourceSettings.postcode_elek,
        attrName: 'e_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer22e',
    tab: 'energie',
    name: 'Elektriciteitsproductie 2023',
    desc: `De elektriciteitsproductie toont de jaarlijkse opbrengst aan het net (alleen Liander). De productie hangt vooral af van gebruikerseigenschappen en -gedrag, zoals elektrische auto’s en huishoudelijke apparaten.`,
    note: `Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen.`,
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        average: 600,
        scale: 'per postcode 6 gebied',
        unit: 'Gemiddelde kWh',
        expressionType: 'interpolate',
        legendStops: [0, 750, 2000, 3000, 6000, 8000],
        attrName: 'p6_kwh_productie',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        lastLabel: '< 8000',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'red',
          domain: [1, 1000, 4000, 8000],
        }),
        popup: ({ properties: { p6_kwh_productie } }) => ({
          title: `${p6_kwh_productie} kWh`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.e_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_kwh_productie >= 0 && [
              'Elektriciteitsexport',
              `${properties.p6_kwh_productie} KWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],

            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6elk_2023',
        tileSource: sourceSettings.postcode_elek,
        attrName: 'e_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },

  {
    id: 'layer23',
    tab: 'energie',
    name: 'Gasverbruik per m2 2023',
    desc: `Het gasverbruik per m² pandoppervlak geeft een indicatie van de relatieve energiezuinigheid, waarbij een hoog verbruik kan wijzen op slechte isolatie, grootte of bedrijvigheid, en een laag verbruik op efficiënter verwarmen.`,
    note: '3% van de data mist',
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
      'BAG, oppervlakte': metaDataList.BAG,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'per postcode 6 gebied',
        unit: 'Gemiddelde m3 gas per m2 bodembeslag',
        average: 7.2,
        legendStops: [0, 6, 7, 9, 16, 20],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3_per_m2',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',

        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 7.2, 17],
        }),
        popup: ({ properties: { p6_gasm3_per_m2 } }) => ({
          title: `${p6_gasm3_per_m2} m3/m2/jaar`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.g_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_kwh_productie >= 0 && [
              'Elektriciteitsexport',
              `${properties.p6_kwh_productie} KWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],

            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6gas_2023',
        tileSource: sourceSettings.postcode_gas,
        attrName: 'g_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer24',
    tab: 'energie',
    name: 'Gasverbruik per m3 2023',
    desc: `Het gasverbruik per kubieke meter object geeft een indicatie van de energiezuinigheid van een object. Een hoog gasverbruik kan immers veroorzaakt worden door slechte isolatie, maar ook doordat een object erg groot is.

    De hoogte van een pand komt uit de BAG3D van TU-delft met behulp van laser metingen. Helaas is niet van elk pand
    een hoogte beschikbaar. Samen met het feit dan kleinverbruik gegevens over meerdere panden gaat zitten er grote
    gaten in de kaart. De kaart lijkt overeen te komen met de m2 laag maar in hoogbouw zijn toch aanzienlijke verschillen te zien.`,
    note: '24% van de data mist (of volume van een pand, of het gasverbruik ervan)',
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
      'BAG, oppervlakte': metaDataList.BAG,
      'BAG3D TuDelft, inhoud': metaDataList.BAG3D,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'per postcode 6 gebied',
        unit: 'Gemiddelde m3 gas per m3 object',
        average: 1.2,
        legendStops: [0, 1, 1.2, 1.4, 3],
        expressionType: 'interpolate',
        attrName: 'p6_gasm3_per_m3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',

        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [0, 1.2, 3],
        }),
        popup: ({ properties: { p6_gasm3_per_m3 } }) => ({
          title: `${p6_gasm3_per_m3} m3 gas per m3 object per jaar`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.g_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_kwh_productie >= 0 && [
              'Elektriciteitsexport',
              `${properties.p6_kwh_productie} KWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6gas_2023',
        tileSource: sourceSettings.postcode_gas,
        attrName: 'g_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer26',
    tab: 'energie',
    name: 'Aantal gasaansluitingen in een postcode gebied 2023',
    desc: `Deze kaart geeft inzicht in het aantal aardgasaansluitingen in een postcode gebied. Bij hoogbouw of in grote samengevoegde postcode 6 gebieden kan het over veel aansluitingen gaan. Energie kleinverbruik gegevens worden aangeleverd van tot postcode en kunnen resulteren in geografisch niet handige of grote gebieden. Gebieden kunnen een hoog gemiddeld gasverbuik hebben en slechts een paar aansluitingen. `,
    note: 'Wordt in postcode groepen gepubliceerd om anonimiteit te waarborgen.',
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'per postcode 6 gebied',
        unit: 'Aantal gasaansluitingen',
        average: 27,
        legendStops: [0, 16, 30, 50, 100, 200],
        expressionType: 'interpolate',
        attrName: 'p6_gas_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'diverging',
          color: 'purple',
          domain: [5, 27, 100],
        }),
        popup: ({ properties: { p6_gas_aansluitingen } }) => ({
          title: `${p6_gas_aansluitingen} aansluitingen in postcode gebied`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.group_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],
            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6gas_2023',
        tileSource: sourceSettings.postcode_gas,
        attrName: 'g_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer27',
    tab: 'energie',
    authRoles: ['energie'],
    authLocked: true,
    name: 'Aantal gasaansluitingen per pand - EAN codes',
    desc: `Het aantal EAN codes is een indicatie voor het aantal gas aansluitingen binnen een object.  LET OP ! Het gaat om kleinverbruik aansluitingen, bij 0 aansluitingen kan het een grootverbruik aansluiting zijn.`,
    note: 'Van ~7 miljoen panden zijn EAN codes gevonden',
    lastUpdate: '2024-07-04',
    sourceDescription: {
      EDSN: metaDataList.EDSN,
    },
    styleLayers: [
      {
        scale: 'per pand',
        unit: 'Aantal aansluitingen',
        legendStops: [1, 2, 4, 6, 12, 24, 30, 50],
        expressionType: 'interpolate',
        attrName: 'pand_gas_ean_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'red',
          domain: [1, 12],
        }),
        popup: ({ properties: { pand_gas_ean_aansluitingen } }) => ({
          title: `${pand_gas_ean_aansluitingen}`,
        }),
        detailModal: ({ properties }) => ({
          type: 'building',
          title: '',
          desc: ' ',
          content: [
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
          ],
        }),
      },
    ],
    drawType: 'count',
  },
  {
    id: 'layer28',
    tab: 'energie',
    authRoles: ['energie'],
    authLocked: true,
    name: 'Wel of geen gasaansluiting - volgens EANcode boek',
    desc: `Het aantal EAN codes is een indicatie voor het aantal gas aansluitingen binnen een object.  LET OP ! Het gaat om kleinverbruik aansluitingen, bij 0 aansluitingen kan het een grootverbruik aansluiting zijn.`,
    note: 'Van ~7 miljoen panden zijn EAN codes gevonden',
    sourceDescription: {
      EDSN: metaDataList.EDSN,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Pand',
        unit: 'Gasaansluiting aanwezig',
        expressionType: 'case',
        legendStops: ['wel', 'geen'],
        attrName: 'pand_gas_ean_aansluitingen',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        customFilter: customFilters.bouwjaar,
        colorScale: createColorScale({
          type: 'ordinal',
          colorRange: ['#F1E7AA', '#395065'],
          ordinalDomain: ['0', '1'],
        }),
        popup: ({ properties: { pand_gas_ean_aansluitingen } }) => ({
          title:
            pand_gas_ean_aansluitingen > 0
              ? 'Heeft minimaal 1 gasaansluiting'
              : 'Heeft geen gasaansluiting',
        }),
        detailModal: ({ properties }) => ({
          type: 'building',
          title: ``,
          desc: ' ',
          content: [
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
          ],
        }),
      },
    ],
  },
  {
    id: 'layer29',
    tab: 'energie',
    name: 'Vermoedelijk kookgas',
    authRoles: ['energie'],
    authLocked: true,
    desc: `Deze kaartlaag laat zien welke objecten een laag gasverbruik hebben gemiddeld per aansluiting. Dit kan inzicht geven waar vermoedelijk kookgas is.`,
    note: `Geen gas = 0 m3 \n
      kookgas 1-100 m3 \n
      huishouden 100 - 6000 \n
      Van ~7 miljoen panden zijn EAN codes gevonden`,
    sourceDescription: {
      'Kleinverbruik-bestanden van alle netbeheerders van de eigen websites':
        metaDataList.energie,
    },
    lastUpdate: '2024-07-04',
    styleLayers: [
      {
        scale: 'Gemiddelde per postcode 6 gebied',
        unit: 'Categorie',
        legendStops: ['Geen verbruik', 'Kookgas', 'Regulier'],
        expressionType: 'step',
        attrName: 'p6_gasm3',
        tileSource: sourceSettings.bag,
        extrusionAttr: 'hoogte',
        colorScale: createColorScale({
          type: 'linear',
          colorRange: ['#66c2a5', '#fc8d62', '#395065'],
          domain: [0, 1, 100],
        }),
        popup: ({ properties: { p6_gasm3 } }) => ({
          title:
            p6_gasm3 <= 0
              ? 'Geen verbruik'
              : p6_gasm3 > 100
              ? 'Regulier verbruik'
              : 'Kookgas',
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Detail postcode gebied ${properties.g_report_id}`,
          desc: ' ',
          content: [
            properties.p6_gasm3 >= 0 && [
              'Gasverbruik',
              `${properties.p6_gasm3} m3/jaar`,
            ],
            properties.p6_kwh >= 0 && [
              'Elektriciteitsgebruik',
              `${properties.p6_kwh} kWh`,
            ],
            properties.p6_gasm3_per_m2 >= 0 && [
              'Gasverbruik per m2',
              `${properties.p6_gasm3_per_m2} m3 gas per m2 bodembeslag`,
            ],
            properties.p6_gasm3_per_m3 >= 0 && [
              'Gasverbruik per m3',
              `${properties.p6_gasm3_per_m3} m3 gas per m3 object`,
            ],
            properties.p6_totaal_volume_m3 >= 0 && [
              'Totale volume ',
              properties.p6_totaal_volume_m3 + ' m3',
            ],
            properties.p6_totaal_oppervlak_m2 >= 0 && [
              'totale grondbeslag',
              properties.p6_totaal_oppervlak_m2 + ' m2',
            ],

            properties.p6_gas_aansluitingen >= 0 && [
              'Aantal gasaansluitingen in postcode gebied',
              `${properties.p6_gas_aansluitingen}`,
            ],
            properties.pand_gas_ean_aansluitingen >= 0
              ? [
                  'Aantal aansluitingen in pand',
                  `${properties.pand_gas_ean_aansluitingen}`,
                ]
              : ['Geen aansluiting in pand', '-'],
            properties.p6_gasm3 <= 0
              ? ['Vermoedelijk kookgas', 'Geen verbruik']
              : properties.p6_gasm3 > 100
              ? ['Vermoedelijk kookgas', 'Regulier verbruik']
              : ['Vermoedelijk kookgas', 'Kookgas'],
          ],
        }),
      },
      {
        'source-layer': 'kv_pc6gas_2023',
        tileSource: sourceSettings.postcode_gas,
        attrName: 'g_report_id',
        isHighlightLayer: true,
        geomType: 'line',
        color: '#FFBC2C',
        legendStops: ['Postcodegebied'],
        customPaint: customPaint.hoverOutlines,
      },
    ],
  },
  {
    id: 'layer30',
    tab: 'energie',
    name: 'Buurten met kookgasaansluitingen, 2020',
    desc: `In enkele buurten in Nederland komt kookgas voor (zie toelichting). Dat betekent dat woningen zijn aangesloten op een warmtenet, maar wel een aardgasaansluiting hebben voor hun kooktoestel (gaskookplaat en/of gasoven). Die woningen zijn technisch eenvoudig aardgasvrij te maken. In deze kaart wordt weergegeven in welke buurten meer dan 20% van de woningen een kookgasaansluiting heeft. In verband met AVG kunnen we alleen weergeven welk percentage van de woningen in een buurt kookgas heeft.`,
    sourceDescription: {
      'Woningen met kookgasaansluitingen, 2020 (cbs.nl)':
        metaDataList.CBSkookgas,
    },
    lastUpdate: '2022-01',
    styleLayers: [
      {
        scale: 'Buurten met kookgas',
        unit: '%',
        opacity: 1,
        legendStops: [20, 100],
        attrName: 'percentage_kookgaswoningen',
        tileSource: sourceSettings.cbs,
        'source-layer': 'kookgasaansluitingen2020',
        expressionType: 'interpolate',
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'red',
          domain: [10, 90],
        }),
        popup: ({ properties: { percentage_kookgaswoningen } }) => ({
          title: percentage_kookgaswoningen + ' %',
        }),
      },
    ],
  },
  {
    id: 'layer31',
    tab: 'energie',
    name: 'Gemiddeld verbruik per buurt, uitgesplitst naar huur en koop',
    desc: `Binnen een wijk kunnen grote verschillen in energieverbruik bestaan (bijv. huur vs koop, woningtype). Deze kaart toont uitsplitsingen per buurt, wat invloed kan hebben op de wijkaanpak.`,
    sourceDescription: {
      'CBS, Kerncijfers wijken en buurten 2019': metaDataList.CBS,
    },
    lastUpdate: '2022-06',
    sublayers: [
      {
        id: 'layer31_1',
        name: 'Gemiddeld gasverbruik huurwoning',
        styleLayers: [
          {
            scale: 'per gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            average: 1217,
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_huurwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1700, 2500, 3500],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1217, 3190],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_huurwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer31_2',
        name: 'Gemiddeld gasverbruik Koopwoning',
        styleLayers: [
          {
            scale: 'per gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            average: 1570,
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_gasverbruikkoopwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1700, 2500, 3500],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1570, 2940],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruikkoopwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer31_2',
        name: 'Gemiddeld elektriciteitsgebruik huurwoning',
        styleLayers: [
          {
            scale: 'per gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            average: 2444,
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_elektriciteitsverbruik_huurwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 6860],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 2444, 4000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_huurwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer31_3',
        name: 'Gemiddeld elektriciteitsgebruik koopwoning',
        styleLayers: [
          {
            scale: 'per gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            average: 3454,
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2018',
            attrName: 'gemiddeld_elektriciteitsverbruikkoopwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 1080, 2000, 3000, 4000, 7830],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [1080, 3454, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruikkoopwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer31_4',
        name: 'Gemiddeld elektriciteitsverbruik totaal',
        styleLayers: [
          {
            scale: 'per gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            average: 3212,
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_totaal',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_huurwoning && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer32',
    tab: 'energie',
    name: 'Gemiddeld verbruik per buurt, uitgesplitst naar type woning',
    desc: `Binnen een wijk kunnen grote verschillen in energieverbruik zijn (bijv. huur vs koop, woningtype). Deze kaarten tonen uitsplitsingen per buurt, zodat je kunt onderzoeken waar de verschillen vandaan komen, zoals woninggrootte, type woning of isolatiegraad.`,
    sourceDescription: {
      'CBS, Kerncijfers wijken en buurten 2019': metaDataList.CBS,
    },
    lastUpdate: '2022-06',
    sublayers: [
      {
        id: 'layer32_1',
        name: 'Gasverbruik appartement',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_appartement',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_appartement + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'Gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'Gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'Gasverbruik koopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'Elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'Elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'Elektriciteitsverbruik koopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_2',
        name: 'Gasverbruik tussenwoning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_tussenwoning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_tussenwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_3',
        name: 'Gasverbruik hoekwoning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_hoekwoning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content: properties.gemiddeld_gasverbruik_hoekwoning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_4',
        name: 'Gasverbruik 2 onder 1 kap woning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_2_onder_1_kap_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_gasverbruik_2_onder_1_kap_woning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_5',
        name: 'Gasverbruik vrijstaande woning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde m2',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_gasverbruik_vrijstaande_woning',
            tileSource: sourceSettings.cbs,

            legendStops: [0, 750, 1250, 1850, 2500, 3500, 6020],

            colorScale: createColorScale({
              type: 'diverging',
              color: 'purple',
              domain: [10, 1517, 5000],
            }),

            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_gasverbruik_vrijstaande_woning + ' m2',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_6',
        name: 'Elektriciteitsverbruik appartement',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_appartement',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_appartement +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_7',
        name: 'Elektriciteitsverbruik tussenwoning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_tussenwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_tussenwoning +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_8',
        name: 'Elektriciteitsverbruik hoekwoning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gemiddeld_elektriciteitsverbruik_hoekwoning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gemiddeld_elektriciteitsverbruik_hoekwoning + ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_9',
        name: 'Elektriciteitsverbruik 2 onder 1 kap woning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gem_elektriciteitsverbruik_2_onder_1_kap_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gem_elektriciteitsverbruik_2_onder_1_kap_woning +
                ' kwh',
            }),

            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer32_10',
        name: 'Elektriciteitsverbruik vrijstaande woning',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: 'Gemiddelde kwh',
            noDataValue: [-99999999, -99999998],
            expressionType: 'interpolate',
            'source-layer': 'buurtcijfers2019',
            attrName: 'gem_elektriciteitsverbruik_vrijstaande_woning',
            tileSource: sourceSettings.cbs,
            legendStops: [0, 680, 2000, 3000, 4000, 8100],
            colorScale: createColorScale({
              type: 'diverging',
              color: 'red',
              domain: [680, 3212, 5000],
            }),
            popup: ({ properties }) => ({
              title: `${properties.name} ${properties.code}`,
              content:
                properties.gem_elektriciteitsverbruik_vrijstaande_woning +
                ' kwh',
            }),
            detailModal: ({ properties }) => ({
              type: 'verbruik',
              title: `Gemiddeld verbruik ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.gemiddeld_gasverbruik_totaal >= 0 && [
                  'gemiddeld gasverbruik totaal',
                  properties.gemiddeld_gasverbruik_totaal + ' m2',
                ],
                properties.gemiddeld_gasverbruik_huurwoning >= 0 && [
                  'gemiddeld gasverbruik huurwoning',
                  properties.gemiddeld_gasverbruik_huurwoning + ' m2',
                ],
                properties.gemiddeld_gasverbruikkoopwoning >= 0 && [
                  'gemiddeld gasverbruikkoopwoning',
                  properties.gemiddeld_gasverbruikkoopwoning + ' m2',
                ],
                properties.gemiddeld_elektriciteitsverbruik_totaal >= 0 && [
                  'gemiddeld elektriciteitsverbruik totaal',
                  properties.gemiddeld_elektriciteitsverbruik_totaal + ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruik_huurwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruik huurwoning',
                  properties.gemiddeld_elektriciteitsverbruik_huurwoning +
                    ' kwh',
                ],
                properties.gemiddeld_elektriciteitsverbruikkoopwoning >= 0 && [
                  'gemiddeld elektriciteitsverbruikkoopwoning',
                  properties.gemiddeld_elektriciteitsverbruikkoopwoning +
                    ' kwh',
                ],
              ],
            }),
          },
        ],
      },
    ],
  },
  //
  // sociaal
  //
  {
    id: 'layer100',
    tab: 'sociaal',
    name: 'Percentage huishoudens die behoren tot de landelijke 20% huishoudens met het hoogste inkomen',
    desc: `Het gemiddelde inkomen zegt iets over de bestedingsruimte die mensen hebben, en geeft daarmee een eerste indicatie van de financiële gevolgen van de energietransitie voor deze mensen. Een lager gemiddeld inkomen betekent vaak minder bestedingsruimte, maar betekent ook dat een stijging van de (fossiele) energieprijzen meer impact heeft.Een hoger gemiddeld inkomen betekent dat de mensen die graag iets willen doen aan de energietransitie, daar waarschijnlijk ook de financiële ruimte voor hebben. Moet uiteraard samen met andere indicatoren bekeken worden, met name of er sprake is van huur of koop. `,
    note: '86% van de data mist',
    sourceDescription: {
      'CBS, Kerncijfers wijken en buurten 2019': metaDataList.CBS,
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        opacity: 0.8,
        legendStops: [0, 10, 20, 30, 50, 80, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        'source-layer': 'buurtcijfers2018',
        attrName: 'percentage_huishoudens_met_hoog_inkomen',
        tileSource: sourceSettings.cbs,
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [0, 60],
        }),
        popup: ({
          properties: { percentage_huishoudens_met_hoog_inkomen, name, code },
        }) => ({
          title: `${name} - ${code}`,
          content: `${percentage_huishoudens_met_hoog_inkomen}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8%< van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer101',
    tab: 'sociaal',
    name: 'Percentage huishoudens die behoren tot de landelijke 40% huishoudens met het laagste inkomen',
    desc: `Het percentage lage inkomens geeft een eerste indicatie van de aanwezigheid van armoedeproblematiek in een wijk. Moet bekeken worden met andere indicatoren, bij voorkeur % inkomen naar aardgas, omdat dan zichtbaar wordt waar mensen in de knel dreigen te komen door hoge gasrekeningen.`,
    note: '38% van de data mist',
    sourceDescription: {
      'CBS, Kerncijfers wijken en buurten 2019': metaDataList.CBS,
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        scale: 'Gemeente, wijk, buurt',
        unit: '%',
        opacity: 0.8,
        legendStops: [0, 10, 20, 30, 40, 50, 80, 100],
        noDataValue: [-99999999, -99999998],
        expressionType: 'interpolate',
        'source-layer': 'buurtcijfers2018',
        attrName: 'percentage_huishoudens_met_laag_inkomen',
        tileSource: sourceSettings.cbs,
        colorScale: createColorScale({
          type: 'sequentional',
          color: 'cividis',
          domain: [10, 80],
        }),
        popup: ({
          properties: { percentage_huishoudens_met_laag_inkomen, name, code },
        }) => ({
          title: `${name} - ${code}`,
          content: `${percentage_huishoudens_met_laag_inkomen}%`,
        }),
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: `Details ${properties.name}`,
          desc: `${properties.code}`,
          content: [
            properties.percentage_huishoudens_met_hoog_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 20% met het hoogste inkomen',
                  `${properties.percentage_huishoudens_met_hoog_inkomen} %`,
                ]
              : null,
            properties.percentage_huishoudens_met_laag_inkomen
              ? [
                  'Huishoudens die behoren tot de landelijke 40% met het laagste inkomen',
                  `${properties.percentage_huishoudens_met_laag_inkomen} %`,
                ]
              : null,
            properties.h50_i25_wpa
              ? [
                  'Huishoudens met laag inkomen, hoog gasverbruik',
                  `${properties.h50_i25_wpa} %`,
                ]
              : null,
            properties.er8p_i_wpa
              ? [
                  'Huishoudens dat 8%< van het inkomen besteed aan gas- en elektriciteitsrekening',
                  `${properties.er8p_i_wpa} %`,
                ]
              : null,
          ],
        }),
      },
    ],
  },
  {
    id: 'layer205',
    tab: 'sociaal',
    name: 'Financiële draagkracht van huishoudens met een koopwoning',
    desc: `Deze dataset geeft aan hoeveel procent van de huishoudens in een wijk of buurt een financiële buffer heeft van minimaal het aangevinkte getal. `,
    sourceDescription: {
      'CBS, Energie Indicatoren 2018': metaDataList.CBSEnergieIndicatoren,
    },
    lastUpdate: '2021-10',
    sublayers: [
      {
        id: 'layer205_1',
        name: 'Minimaal 5000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            opacity: 0.8,
            legendStops: [0, 100],
            expressionType: 'interpolate',
            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm5000',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            popup: ({ properties: { m5000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 5000 €: ${m5000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen && [
                  'Aandeel koopwoningen',
                  `${properties.aandeel_koopwoningen}%`,
                ],
                properties.financieel_wpg && [
                  'Aandeel met gegevens',
                  `${properties.financieel_wpg}%`,
                ],
                properties.m5000 && [
                  'min vermogen van 5000€',
                  `${properties.m5000}%`,
                ],
                properties.m10000 && [
                  'min vermogen van 10.000€',
                  `${properties.m5000}%`,
                ],
                properties.m15000 && [
                  'min vermogen van 15.000€',
                  `${properties.m15000}%`,
                ],
                properties.m20000 && [
                  'min vermogen van 20.000€',
                  `${properties.m20000}%`,
                ],
                properties.m25000 && [
                  'min vermogen van 25.000€',
                  `${properties.m25000}%`,
                ],
                properties.m30000 && [
                  'min vermogen van 30.000€',
                  `${properties.m30000}%`,
                ],
                properties.m35000 && [
                  'min vermogen van 35.000€',
                  `${properties.m35000}%`,
                ],
                properties.m40000 && [
                  'min vermogen van 40.000€',
                  `${properties.m40000}%`,
                ],
                properties.m45000 && [
                  'min vermogen van 45.000€',
                  `${properties.m45000}%`,
                ],
                properties.m50000 && [
                  'min vermogen van 50.000€',
                  `${properties.m50000}%`,
                ],
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_2',
        name: 'Minimaal 10.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm10000',
            popup: ({ properties: { m10000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 10.000€: ${m10000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_3',
        name: 'Minimaal 15.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm15000',
            popup: ({ properties: { m15000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 15.000€ : ${m15000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_4',
        name: 'Minimaal 20.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm20000',
            popup: ({ properties: { m20000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 20.000€: ${m20000}%`,
            }),

            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_5',
        name: 'Minimaal 25.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm25000',
            popup: ({ properties: { m25000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 25.000€ : ${m25000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_6',
        name: 'Minimaal 30.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm30000',
            popup: ({ properties: { m30000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 30.000€ : ${m30000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_7',
        name: 'Minimaal 35.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm35000',
            popup: ({ properties: { m35000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 35.000€ : ${m35000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_8',
        name: 'Minimaal 40.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm40000',
            popup: ({ properties: { m40000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 40.000€ : ${m40000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_9',
        name: 'Minimaal 45.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm45000',
            popup: ({ properties: { m45000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 45.000€ : ${m45000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer205_10',
        name: 'Minimaal 50.000 euro',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '%',
            legendStops: [0, 100],
            expressionType: 'interpolate',
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'cividis',
              domain: [1, 100],
            }),
            opacity: 0.8,

            tileSource: sourceSettings.cbs,
            'source-layer': 'armoedebuurtcijfers2020',
            attrName: 'm50000',
            popup: ({ properties: { m50000, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `Huishoudens met minimaal vermogen van 50.000€ : ${m50000}%`,
            }),
            detailModal: ({ properties }) => ({
              type: 'detail',
              title: `Details ${properties.name}`,
              desc: `${properties.code}`,
              content: [
                properties.aandeel_koopwoningen
                  ? [
                      'Aandeel koopwoningen',
                      `${properties.aandeel_koopwoningen}%`,
                    ]
                  : null,
                properties.financieel_wpg
                  ? ['Aandeel met gegevens', `${properties.financieel_wpg}%`]
                  : null,
                properties.m5000
                  ? ['min vermogen van 5000€', `${properties.m5000}%`]
                  : null,
                properties.m10000
                  ? ['min vermogen van 10.000€', `${properties.m5000}%`]
                  : null,
                properties.m15000
                  ? ['min vermogen van 15.000€', `${properties.m15000}%`]
                  : null,
                properties.m20000
                  ? ['min vermogen van 20.000€', `${properties.m20000}%`]
                  : null,
                properties.m25000
                  ? ['min vermogen van 25.000€', `${properties.m25000}%`]
                  : null,
                properties.m30000
                  ? ['min vermogen van 30.000€', `${properties.m30000}%`]
                  : null,
                properties.m35000
                  ? ['min vermogen van 35.000€', `${properties.m35000}%`]
                  : null,
                properties.m40000
                  ? ['min vermogen van 40.000€', `${properties.m40000}%`]
                  : null,
                properties.m45000
                  ? ['min vermogen van 45.000€', `${properties.m45000}%`]
                  : null,
                properties.m50000
                  ? ['min vermogen van 50.000€', `${properties.m50000}%`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  //
  // bereidheid
  //
  {
    id: 'layer500',
    tab: 'bereidheid',
    name: 'Vindt comfort belangrijk ',
    desc: 'Als een wijk hoog scoort op deze indicator, zijn er relatief veel huishoudens voor wie comfort een belangrijk driver is voor het nemen van energiebesparende maatregelen.',
    sourceDescription: {
      'CBS, Bereidheid energietransitiemaatregelen': metaDataList.CBSBereidheid,
    },
    lastUpdate: '2023-08',
    sublayers: [
      {
        id: 'layer500_1',
        name: 'Totaal',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'comfort_geisoleerd_indicatie',
            customFilter: ['==', ['get', 'type_eigendom'], 'Totaal'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { comfort_geisoleerd_indicatie, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${comfort_geisoleerd_indicatie}`,
            }),
          },
        ],
      },
      {
        id: 'layer500_2',
        name: 'Huur',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'comfort_geisoleerd_indicatie',
            customFilter: ['==', ['get', 'type_eigendom'], 'Huurwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { comfort_geisoleerd_indicatie, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${comfort_geisoleerd_indicatie}`,
            }),
          },
        ],
      },
      {
        id: 'layer500_3',
        name: 'Koop',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'comfort_geisoleerd_indicatie',
            customFilter: ['==', ['get', 'type_eigendom'], 'Koopwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { comfort_geisoleerd_indicatie, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${comfort_geisoleerd_indicatie}`,
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer501',
    tab: 'bereidheid',
    name: 'Vindt energiezuinige maatregelen belangrijk vanwege het milieu',
    desc: ' Als een wijk hoog scoort op deze indicator, zijn er relatief veel huishoudens voor wie het milieu een belangrijk driver is voor het nemen van energiebesparende maatregelen.',
    sourceDescription: {
      'CBS, Bereidheid energietransitiemaatregelen': metaDataList.CBSBereidheid,
    },
    lastUpdate: '2023-08',

    sublayers: [
      {
        id: 'layer501_1',
        name: 'Totaal',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'milieu_belangrijk_betalen',
            customFilter: ['==', ['get', 'type_eigendom'], 'Totaal'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { milieu_belangrijk_betalen, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${milieu_belangrijk_betalen}`,
            }),
          },
        ],
      },
      {
        id: 'layer501_2',
        name: 'Huur',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'milieu_belangrijk_betalen',
            customFilter: ['==', ['get', 'type_eigendom'], 'Huurwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { milieu_belangrijk_betalen, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${milieu_belangrijk_betalen}`,
            }),
          },
        ],
      },
      {
        id: 'layer501_3',
        name: 'Koop',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'milieu_belangrijk_betalen',
            customFilter: ['==', ['get', 'type_eigendom'], 'Koopwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({
              properties: { milieu_belangrijk_betalen, name, code },
            }) => ({
              title: `${name} - ${code}`,
              content: `${milieu_belangrijk_betalen}`,
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer502',
    tab: 'bereidheid',
    name: ' Vindt de woning al energiezuinig',
    desc: 'Als een wijk hoog scoort op deze indicator, zijn er relatief veel huishoudens die van mening zijn dat hun huis al energiezuinig is, en dat ze dus geen/weinig aanvullende maatregelen hoeven te nemen. NB1: het gaat hier om de beleving van huishoudens, niet om de daadwerkelijke energetische toestand van de woning. NB2: het gaat hier om huishoudens met een koopwoning.',
    sourceDescription: {
      'CBS, Bereidheid energietransitiemaatregelen': metaDataList.CBSBereidheid,
    },
    lastUpdate: '2023-08',
    sublayers: [
      {
        id: 'layer502_1',
        name: 'Totaal',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'al_energiezuinig',
            customFilter: ['==', ['get', 'type_eigendom'], 'Totaal'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({ properties: { al_energiezuinig, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `${al_energiezuinig}`,
            }),
          },
        ],
      },
      {
        id: 'layer502_2',
        name: 'Huur',
        styleLayers: [
          {
            scale: 'buurt, wijk, gemeente',
            unit: 'Score op de indicator',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'al_energiezuinig',
            customFilter: ['==', ['get', 'type_eigendom'], 'Huurwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({ properties: { al_energiezuinig, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `${al_energiezuinig}`,
            }),
          },
        ],
      },
      {
        id: 'layer502_3',
        name: 'Koop',
        styleLayers: [
          {
            unit: 'Score op de indicator',
            scale: 'buurt, wijk, gemeente',
            tileSource: sourceSettings.draagvlakdata,
            legendStops: ['Laag', 'Midden', 'Hoog'],
            expressionType: 'match',
            attrName: 'al_energiezuinig',
            customFilter: ['==', ['get', 'type_eigendom'], 'Koopwoning'],
            colorScale: createColorScale({
              type: 'ordinal',
              colorRange: ['#fc8d62', '#0b71a1', '#66c2a5'],
              ordinalDomain: ['Laag', 'Midden', 'Hoog'],
            }),
            opacity: 0.8,

            popup: ({ properties: { al_energiezuinig, name, code } }) => ({
              title: `${name} - ${code}`,
              content: `${al_energiezuinig}`,
            }),
          },
        ],
      },
    ],
  },
  //
  // climate
  //
  {
    id: 'layer103',
    tab: 'klimaat',
    name: 'Stedelijk Hitte Eiland effect',
    desc: "Deze kaart toont het stedelijke hitte-eiland-effect, het temperatuurverschil tussen stedelijke en omliggende landelijke gebieden in de zomer. Dit effect is 's nachts het sterkst en kan de gezondheid schaden. Het is waardevol om hittestress mee te nemen bij wijkfasering. De kaart is gebaseerd op jaargemiddelde temperaturen en is minder geschikt voor extreem warme dagen of nachten.",
    sourceDescription: {
      Klimaateffectatlas: {
        referenceDate: '2017-01-01',
        url: 'https://www.klimaateffectatlas.nl/nl/',
      },
    },
    lastUpdate: '2021-09',
    styleLayers: [
      {
        scale: 'resolutie 100x100meter',
        unit: 'Temperatuur',
        tileSource: sourceSettings.wmsHitte,
        geomType: 'raster',
        opacity: 0.8,
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=hitteeiland&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer104',
    tab: 'klimaat',
    name: 'Lokale gevoelstemperatuur',
    desc: 'De kaart toont de lokale gevoelstemperatuur op een extreem hete zomermiddag, beïnvloed door weersomstandigheden en de omgeving. Stedelijke gebieden warmen vaak meer op, wat kan leiden tot hittestress, gezondheidsproblemen en zelfs overlijden. De kaart helpt bij het ontwerpen van verkoelende buitenruimtes.',
    sourceDescription: {
      Klimaateffectatlas: {
        referenceDate: '2018',
        url: 'https://www.klimaateffectatlas.nl/nl/hittekaart-gevoelstemperatuur',
      },
    },
    lastUpdate: '2021-09',

    styleLayers: [
      {
        scale: 'resolutie 2x2 meter',
        unit: 'Temperatuur',
        tileSource: sourceSettings.wmsTemp,
        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=hittekaart_gevoelstemp_huidig&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer105',
    tab: 'klimaat',
    name: 'Wateroverlast - Waterdiepte bij intense neerslag - 1:1000 jaar',
    desc: 'Deze kaart geeft een indicatie van de maximale waterdiepte bij kortdurende intense neerslag (140 mm in 2 uur), die circa 1 keer in de 1000 jaar voorkomt. De kaart is ontwikkeld door Deltares voor de voorlopige overstromingsrisicobeoordeling volgens de Europese Overstromingsrichtlijn.',
    sourceDescription: {
      Klimaateffectatlas: metaDataList.klimaateffectatlas,
    },
    lastUpdate: '2021-09',
    styleLayers: [
      {
        scale: 'resolutie 2x2 meter',
        unit: 'Waterdiepte',
        tileSource: sourceSettings.wmsWater,
        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=waterdiepte_neerslag_140mm_2uur&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer106',
    tab: 'klimaat',
    name: 'Wateroverlast - Waterdiepte bij intense neerslag - 1:100 jaar',
    desc: "Deze kaart toont de maximale waterdiepte bij intense neerslag (70 mm in 2 uur), die 1 keer in de 100 jaar voorkomt. De kaart is ontwikkeld door Deltares voor de voorlopige overstromingsrisicobeoordeling volgens de Europese Richtlijn Overstromingsrisico'",
    sourceDescription: {
      Klimaateffectatlas: metaDataList.klimaateffectatlas,
    },
    lastUpdate: '2021-09',
    styleLayers: [
      {
        scale: 'resolutie 2x2 meter',
        unit: 'Waterdiepte',
        tileSource: sourceSettings.wmsWater2,

        geomType: 'raster',
        image:
          'https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=waterdiepte_neerslag_70mm_2uur&TRANSPARENT=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer107',
    tab: 'klimaat',
    name: 'Geluidsoverlast (Lcum)',
    desc: 'Deze kaart toont het gemiddelde geluidsniveau van wegverkeer, treinen, vliegtuigen, industrie en windturbines. Ongewenst geluid kan hinder en gezondheidsklachten veroorzaken (Lden : level day-evening-night).',
    sourceDescription: {
      'Atlas voor de Leefomgeving RIVM': {
        url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
        referenceDate: '',
      },
    },
    lastUpdate: '2021-04',
    styleLayers: [
      {
        scale: 'resolutie 10 meter',
        unit: 'Geluid Lden',
        tileSource: sourceSettings.wmsGeluid,
        geomType: 'raster',
        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&transparent=true',
        featureRequest:
          'https://data.rivm.nl/geo/alo/ows?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&STYLES=&FORMAT=image/png&QUERY_LAYERS=rivm_20210201_g_geluidkaart_lden_alle_bronnen_v3&INFO_FORMAT=application/json',
        detailModal: ({ properties }) => ({
          type: 'detail',
          title: 'Geluid Lden',
          content: [
            properties.GRAY_INDEX
              ? ['Waarde', properties.GRAY_INDEX + ' dB']
              : null,
          ],
        }),
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer108',
    tab: 'klimaat',
    name: 'Fijnstof (PM10)',
    desc: 'Op deze kaart zie je hoeveel fijnstof er gemiddeld in de lucht zat in Nederland in 2019. Het gaat om PM10: deeltjes die kleiner zijn dan 10 micrometer. Hoe minder fijnstof hoe beter de luchtkwaliteit. ',
    sourceDescription: {
      'Atlas voor de Leefomgeving RIVM': {
        url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
        referenceDate: '2019',
      },
    },
    lastUpdate: '2021-04',

    styleLayers: [
      {
        scale: 'resolutie 25 meter',
        unit: 'Fijnstof',
        tileSource: sourceSettings.wmsLucht,
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_PM102019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer109',
    tab: 'klimaat',
    name: 'Fijnstof (PM2,5)',
    desc: 'Op deze kaart zie je hoeveel fijnstof er gemiddeld in de lucht zat in 2019. Het gaat om PM2,5: deeltjes die kleiner zijn dan 2,5 micrometer. Hoe minder fijnstof hoe beter de luchtkwaliteit.',
    sourceDescription: {
      'Atlas voor de Leefomgeving RIVM': {
        url: 'https://geodata.rivm.nl/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20200715_gm_pm252018',
        referenceDate: '2019',
      },
    },
    lastUpdate: '2021-04',

    styleLayers: [
      {
        scale: 'resolutie 25 meter',
        unit: 'Fijnstof',
        tileSource: sourceSettings.wmsLucht2,
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_PM252019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  {
    id: 'layer110',
    tab: 'klimaat',
    name: 'Stikstofdioxide (NO2)',
    desc: 'Op deze kaart zie je hoeveel stikstofdioxide er gemiddeld in de lucht zat in Nederland in 2019. Hoe minder stikstofdioxide hoe beter de luchtkwaliteit is.',
    sourceDescription: {
      'Atlas voor de Leefomgeving RIVM': {
        url: 'https://www.atlasleefomgeving.nl/kaarten?config=3ef897de-127f-471a-959b-93b7597de188&gm-x=150000&gm-y=455000&gm-z=3&gm-b=1544180834512,true,1;1544969872207,true,0.8',
        referenceDate: '2019',
      },
    },
    lastUpdate: '2021-04',

    styleLayers: [
      {
        scale: 'resolutie 25 meter',
        unit: 'NO2',
        tileSource: sourceSettings.wmsLucht3,
        geomType: 'raster',

        image:
          'https://data.rivm.nl/geo/alo/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=rivm_nsl_20210101_gm_NO22019_Int_v2&transparent=true',
      },
      staticBuildingsLayer.styleLayers[0],
    ],
  },
  //
  // ground
  //
  {
    id: 'layer111',
    tab: 'grond',
    name: 'Liander Elektriciteitsnetten',
    desc: 'Deze dataset bevat de liggingsgegevens van de elektriciteitsnetten binnen het werkgebied van Liander. Het betreft de laag- (LS), midden- (MS) en hoogspanningsnetten (HS), inclusief laagspanningskasten, middenspanningsruimtes (MSR) en stations. Er wordt gewerkt aan een landsdekkende kaart.',
    sourceDescription: {
      'Liander, via PDOK': {
        url: 'https://www.pdok.nl/introductie/-/article/liander-elektriciteitsnetten-1',
        referenceDate: '2019',
      },
    },
    lastUpdate: '2021-11-17',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'Objecten',
        unit: '',
        tileSource: sourceSettings.lianderNetten,
        geomType: 'raster',
        image: [
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/hoogspanningskabels/hoogspanningskabels.png',
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/middenspanningskabels/middenspanningskabels.png',
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/laagspanningskabels/laagspanningskabels.png',
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/onderstations/onderstations.png',
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/middenspanningsinstallaties/middenspanningsinstallaties.png',
          'https://service.pdok.nl/liander/elektriciteitsnetten/wms/v1_0/legend/laagspanningsverdeelkasten/laagspanningsverdeelkasten.png',
        ],
      },
    ],
  },
  {
    id: 'layer112',
    tab: 'grond',
    name: 'Riolering',
    desc: 'Op deze kaart is de riolering weergegeven. Het gaat hierbij om verschillende aspecten van de riolering zoals verschillende soorten leidingen, putten en kolken. Heb je vragen over deze kaartlaag? Neem dan contact op met stichting Rioned via info@rioned.org',
    sourceDescription: {
      'Stichting RIONED, van PDOK': {
        url: 'https://www.pdok.nl/introductie/-/article/stedelijk-water-riolering-',
        referenceDate: '2019',
      },
    },
    lastUpdate: '2024-02-14',

    styleLayers: [
      staticBuildingsLayer.styleLayers[0],
      {
        scale: 'Objecten',
        unit: '',
        tileSource: sourceSettings.rioned,
        geomType: 'raster',
        image: [
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/AansluitingLeiding/GWSW_Aansluitleiding.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/BeheerLeiding/GWSW_Leiding.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/AansluitingPunt/GWSW_Aansluitpunt.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/BeheerPomp/GWSW_Pomp.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/BeheerPut/GWSW_Put.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/BeheerLozing/GWSW_Lozing.png',
          'https://service.pdok.nl/rioned/beheerstedelijkwater/wms/v1_0/legend/BeheerBouwwerk/GWSW_Bouwwerk.png',
        ],
      },
    ],
  },
  //
  // ENERGIE ARMOEDE //
  //

  {
    id: 'layer301',
    tab: 'armoede',
    name: 'Hoge energiequote (HEq) ',
    desc: `Deze indicator toont het percentage huishoudens met een hoge energiequote, gedefinieerd als 10% of meer van het inkomen aan energiekosten. Dit wijst op een te grote belasting van het budget door energiekosten.`,
    sourceDescription: { CBS: metaDataList.CBSArmoede },
    lastUpdate: '2024-03',
    sublayers: [
      {
        id: 'layer301_1',
        name: 'Alle huishoudens',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% Heq',
            attrName: 'tot_heq',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { tot_heq } }) => ({
              title: `${tot_heq}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met hoge energiequote`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_heq
                  ? ['Alle huishoudens', `${properties.tot_heq} %`]
                  : null,

                properties.koop_heq
                  ? ['Eigenaar bewoner', `${properties.koop_heq} %`]
                  : null,
                properties.huur_c_heq
                  ? ['Huur woningcorporatie', `${properties.huur_c_heq} %`]
                  : null,
                properties.huur_o_heq
                  ? ['Huur overig', `${properties.huur_o_heq} %`]
                  : null,
                properties.onb_heq
                  ? ['Onbekend bezit', `${properties.onb_heq} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer301_2',
        name: 'Eigenaar bewoner',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% Heq',
            attrName: 'koop_heq',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { koop_heq } }) => ({
              title: `${koop_heq}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met hoge energiequote`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_heq
                  ? ['Alle huishoudens', `${properties.tot_heq} %`]
                  : null,
                properties.koop_heq
                  ? ['Eigenaar bewoner', `${properties.koop_heq} %`]
                  : null,
                properties.huur_c_heq
                  ? ['Huur woningcorporatie', `${properties.huur_c_heq} %`]
                  : null,
                properties.huur_o_heq
                  ? ['Huur overig', `${properties.huur_o_heq} %`]
                  : null,
                properties.onb_heq
                  ? ['Onbekend bezit', `${properties.onb_heq} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer301_3',
        name: 'Huur (woningcorporatie)',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% Heq',
            attrName: 'huur_c_heq',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_c_heq } }) => ({
              title: `${huur_c_heq}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met hoge energiequote`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_heq
                  ? ['Alle huishoudens', `${properties.tot_heq} %`]
                  : null,
                properties.koop_heq
                  ? ['Eigenaar bewoner', `${properties.koop_heq} %`]
                  : null,
                properties.huur_c_heq
                  ? ['Huur woningcorporatie', `${properties.huur_c_heq} %`]
                  : null,
                properties.huur_o_heq
                  ? ['Huur overig', `${properties.huur_o_heq} %`]
                  : null,
                properties.onb_heq
                  ? ['Onbekend bezit', `${properties.onb_heq} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer301_4',
        name: 'Huur (overige verhuurders)',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% Heq',
            attrName: 'huur_o_heq',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_o_heq } }) => ({
              title: `${huur_o_heq}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met hoge energiequote`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_heq
                  ? ['Alle huishoudens', `${properties.tot_heq} %`]
                  : null,
                properties.koop_heq
                  ? ['Eigenaar bewoner', `${properties.koop_heq} %`]
                  : null,
                properties.huur_c_heq
                  ? ['Huur woningcorporatie', `${properties.huur_c_heq} %`]
                  : null,
                properties.huur_o_heq
                  ? ['Huur overig', `${properties.huur_o_heq} %`]
                  : null,
                properties.onb_heq
                  ? ['Onbekend bezit', `${properties.onb_heq} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer301_5',
        name: 'Onbekend bezit',

        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% Heq',
            attrName: 'onb_heq',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { onb_heq } }) => ({
              title: `${onb_heq}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met hoge energiequote`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_heq
                  ? ['Alle huishoudens', `${properties.tot_heq} %`]
                  : null,
                properties.koop_heq
                  ? ['Eigenaar bewoner', `${properties.koop_heq} %`]
                  : null,
                properties.huur_c_heq
                  ? ['Huur woningcorporatie', `${properties.huur_c_heq} %`]
                  : null,
                properties.huur_o_heq
                  ? ['Huur overig', `${properties.huur_o_heq} %`]
                  : null,
                properties.onb_heq
                  ? ['Onbekend bezit', `${properties.onb_heq} %`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer302',
    tab: 'armoede',
    name: 'Laag inkomen, hoge energierekening (LIHE) ',
    desc: `Deze indicator toont het percentage huishoudens met een laag inkomen (maximaal 130% van de lage-inkomensgrens) en hoge energiekosten (boven 1.213 euro in 2019). Dit wijst op een kortetermijn betaalrisico. Meer details zijn te vinden in de CBS/TNO-achtergrondrapportage.`,
    sourceDescription: { CBS: metaDataList.CBSArmoede },
    lastUpdate: '2024-03',

    sublayers: [
      {
        id: 'layer302_1',
        name: 'Alle huishoudens',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE',
            attrName: 'tot_lihe',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { tot_lihe } }) => ({
              title: `${tot_lihe}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met een laag inkomen, hoge energierekening`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihe
                  ? ['Alle huishoudens', `${properties.tot_lihe} %`]
                  : null,
                properties.koop_lihe
                  ? ['Eigenaar bewoner', `${properties.koop_lihe} %`]
                  : null,
                properties.huur_c_lihe
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihe} %`]
                  : null,
                properties.huur_o_lihe
                  ? ['Huur overig', `${properties.huur_o_lihe} %`]
                  : null,
                properties.onb_lihe
                  ? ['Onbekend bezit', `${properties.onb_lihe} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer302_2',
        name: 'Eigenaar bewoner',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE',
            attrName: 'koop_lihe',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { koop_lihe } }) => ({
              title: `${koop_lihe}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met een laag inkomen, hoge energierekening`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihe
                  ? ['Alle huishoudens', `${properties.tot_lihe} %`]
                  : null,
                properties.koop_lihe
                  ? ['Eigenaar bewoner', `${properties.koop_lihe} %`]
                  : null,
                properties.huur_c_lihe
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihe} %`]
                  : null,
                properties.huur_o_lihe
                  ? ['Huur overig', `${properties.huur_o_lihe} %`]
                  : null,
                properties.onb_lihe
                  ? ['Onbekend bezit', `${properties.onb_lihe} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer302_3',
        name: 'Huur (woningcorporatie)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE',
            attrName: 'huur_c_lihe',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_c_lihe } }) => ({
              title: `${huur_c_lihe}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met een laag inkomen, hoge energierekening`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihe
                  ? ['Alle huishoudens', `${properties.tot_lihe} %`]
                  : null,
                properties.koop_lihe
                  ? ['Eigenaar bewoner', `${properties.koop_lihe} %`]
                  : null,
                properties.huur_c_lihe
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihe} %`]
                  : null,
                properties.huur_o_lihe
                  ? ['Huur overig', `${properties.huur_o_lihe} %`]
                  : null,
                properties.onb_lihe
                  ? ['Onbekend bezit', `${properties.onb_lihe} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer302_4',
        name: 'Huur (overige verhuurders)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE',
            attrName: 'huur_o_lihe',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_o_lihe } }) => ({
              title: `${huur_o_lihe}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met een laag inkomen, hoge energierekening`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihe
                  ? ['Alle huishoudens', `${properties.tot_lihe} %`]
                  : null,
                properties.koop_lihe
                  ? ['Eigenaar bewoner', `${properties.koop_lihe} %`]
                  : null,
                properties.huur_c_lihe
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihe} %`]
                  : null,
                properties.huur_o_lihe
                  ? ['Huur overig', `${properties.huur_o_lihe} %`]
                  : null,
                properties.onb_lihe
                  ? ['Onbekend bezit', `${properties.onb_lihe} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer302_5',
        name: 'Onbekend bezit',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE',
            attrName: 'onb_lihe',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { onb_lihe } }) => ({
              title: `${onb_lihe}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met een laag inkomen, hoge energierekening`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihe
                  ? ['Alle huishoudens', `${properties.tot_lihe} %`]
                  : null,
                properties.koop_lihe
                  ? ['Eigenaar bewoner', `${properties.koop_lihe} %`]
                  : null,
                properties.huur_c_lihe
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihe} %`]
                  : null,
                properties.huur_o_lihe
                  ? ['Huur overig', `${properties.huur_o_lihe} %`]
                  : null,
                properties.onb_lihe
                  ? ['Onbekend bezit', `${properties.onb_lihe} %`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer303',
    tab: 'armoede',
    name: 'Laag inkomen, lage energiekwaliteit (LILEK) ',
    desc: `Deze indicator toont het percentage huishoudens met een laag inkomen en een slecht geïsoleerde woning, ongeacht de energierekening, zodat ook huishoudens met laag verbruik worden meegerekend. Meer details staan in de CBS/TNO-achtergrondrapportage.`,
    sourceDescription: { CBS: metaDataList.CBSArmoede },
    lastUpdate: '2024-03',

    sublayers: [
      {
        id: 'layer303_1',
        name: 'Alle huishoudens',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LILEK',
            attrName: 'tot_lilek',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { tot_lilek } }) => ({
              title: `${tot_lilek}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen, lage energiekwaliteit`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lilek
                  ? ['Totaal aantal huishoudens', `${properties.tot_lilek} %`]
                  : null,
                properties.koop_lilek
                  ? ['Eigenaar bewoner', `${properties.koop_lilek} %`]
                  : null,
                properties.huur_c_lilek
                  ? ['Huur woningcorporatie', `${properties.huur_c_lilek} %`]
                  : null,
                properties.huur_o_lilek
                  ? ['Huur overig', `${properties.huur_o_lilek} %`]
                  : null,
                properties.onb_lilek
                  ? ['Onbekend bezit', `${properties.onb_lilek} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer303_2',
        name: 'Eigenaar bewoner',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LILEK',
            attrName: 'koop_lilek',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { koop_lilek } }) => ({
              title: `${koop_lilek}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen, lage energiekwaliteit`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lilek
                  ? ['Totaal aantal huishoudens', `${properties.tot_lilek} %`]
                  : null,
                properties.koop_lilek
                  ? ['Eigenaar bewoner', `${properties.koop_lilek} %`]
                  : null,
                properties.huur_c_lilek
                  ? ['Huur woningcorporatie', `${properties.huur_c_lilek} %`]
                  : null,
                properties.huur_o_lilek
                  ? ['Huur overig', `${properties.huur_o_lilek} %`]
                  : null,
                properties.onb_lilek
                  ? ['Onbekend bezit', `${properties.onb_lilek} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer303_3',
        name: 'Huur (woningcorporatie)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LILEK',
            attrName: 'huur_c_lilek',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_c_lilek } }) => ({
              title: `${huur_c_lilek}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen, lage energiekwaliteit`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lilek
                  ? ['Totaal aantal huishoudens', `${properties.tot_lilek} %`]
                  : null,
                properties.koop_lilek
                  ? ['Eigenaar bewoner', `${properties.koop_lilek} %`]
                  : null,
                properties.huur_c_lilek
                  ? ['Huur woningcorporatie', `${properties.huur_c_lilek} %`]
                  : null,
                properties.huur_o_lilek
                  ? ['Huur overig', `${properties.huur_o_lilek} %`]
                  : null,
                properties.onb_lilek
                  ? ['Onbekend bezit', `${properties.onb_lilek} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer303_4',
        name: 'Huur (overige verhuurders)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LILEK',
            attrName: 'huur_o_lilek',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_o_lilek } }) => ({
              title: `${huur_o_lilek}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen, lage energiekwaliteit`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lilek
                  ? ['Totaal aantal huishoudens', `${properties.tot_lilek} %`]
                  : null,
                properties.koop_lilek
                  ? ['Eigenaar bewoner', `${properties.koop_lilek} %`]
                  : null,
                properties.huur_c_lilek
                  ? ['Huur woningcorporatie', `${properties.huur_c_lilek} %`]
                  : null,
                properties.huur_o_lilek
                  ? ['Huur overig', `${properties.huur_o_lilek} %`]
                  : null,
                properties.onb_lilek
                  ? ['Onbekend bezit', `${properties.onb_lilek} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer303_5',
        name: 'Onbekend bezit',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LILEK',
            attrName: 'onb_lilek',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { onb_lilek } }) => ({
              title: `${onb_lilek}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen, lage energiekwaliteit`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lilek
                  ? ['Totaal aantal huishoudens', `${properties.tot_lilek} %`]
                  : null,
                properties.koop_lilek
                  ? ['Eigenaar bewoner', `${properties.koop_lilek} %`]
                  : null,
                properties.huur_c_lilek
                  ? ['Huur woningcorporatie', `${properties.huur_c_lilek} %`]
                  : null,
                properties.huur_o_lilek
                  ? ['Huur overig', `${properties.huur_o_lilek} %`]
                  : null,
                properties.onb_lilek
                  ? ['Onbekend bezit', `${properties.onb_lilek} %`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer304',
    tab: 'armoede',
    name: 'Lage energiekwaliteit, weinig investeringsmogelijkheden (LEKWI) ',
    desc: `Deze indicator toont het percentage huishoudens in een woning met lage energetische kwaliteit die niet in staat zijn te verduurzamen, zowel huurders als eigenaar-bewoners met beperkte financiële middelen om zelf te verduurzamen.`,
    sourceDescription: { CBS: metaDataList.CBSArmoede },
    lastUpdate: '2024-03',

    sublayers: [
      {
        id: 'layer304_1',
        name: 'Alle huishoudens',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LEKWI',
            attrName: 'tot_lekwi',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { tot_lekwi } }) => ({
              title: `${tot_lekwi}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met Lage energiekwaliteit, weinig investeringsmogelijkheden`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lekwi
                  ? ['Totaal aantal huishoudens', `${properties.tot_lekwi} %`]
                  : null,
                properties.koop_lekwi
                  ? ['Eigenaar bewoner', `${properties.koop_lekwi} %`]
                  : null,
                properties.huur_c_lekwi
                  ? ['Huur woningcorporatie', `${properties.huur_c_lekwi} %`]
                  : null,
                properties.huur_o_lekwi
                  ? ['Huur overig', `${properties.huur_o_lekwi} %`]
                  : null,
                properties.onb_lekwi
                  ? ['Onbekend bezit', `${properties.onb_lekwi} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer304_2',
        name: 'Eigenaar bewoner',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LEKWI',
            attrName: 'koop_lekwi',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { koop_lekwi } }) => ({
              title: `${koop_lekwi}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met Lage energiekwaliteit, weinig investeringsmogelijkheden`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lekwi
                  ? ['Totaal aantal huishoudens', `${properties.tot_lekwi} %`]
                  : null,
                properties.koop_lekwi
                  ? ['Eigenaar bewoner', `${properties.koop_lekwi} %`]
                  : null,
                properties.huur_c_lekwi
                  ? ['Huur woningcorporatie', `${properties.huur_c_lekwi} %`]
                  : null,
                properties.huur_o_lekwi
                  ? ['Huur overig', `${properties.huur_o_lekwi} %`]
                  : null,
                properties.onb_lekwi
                  ? ['Onbekend bezit', `${properties.onb_lekwi} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer304_3',
        name: 'Huur (woningcorporatie)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LEKWI',
            attrName: 'huur_c_lekwi',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_c_lekwi } }) => ({
              title: `${huur_c_lekwi}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met Lage energiekwaliteit, weinig investeringsmogelijkheden`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lekwi
                  ? ['Totaal aantal huishoudens', `${properties.tot_lekwi} %`]
                  : null,
                properties.koop_lekwi
                  ? ['Eigenaar bewoner', `${properties.koop_lekwi} %`]
                  : null,
                properties.huur_c_lekwi
                  ? ['Huur woningcorporatie', `${properties.huur_c_lekwi} %`]
                  : null,
                properties.huur_o_lekwi
                  ? ['Huur overig', `${properties.huur_o_lekwi} %`]
                  : null,
                properties.onb_lekwi
                  ? ['Onbekend bezit', `${properties.onb_lekwi} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer304_4',
        name: 'Huur (overige verhuurders)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LEKWI',
            attrName: 'huur_o_lekwi',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_o_lekwi } }) => ({
              title: `${huur_o_lekwi}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met Lage energiekwaliteit, weinig investeringsmogelijkheden`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lekwi
                  ? ['Totaal aantal huishoudens', `${properties.tot_lekwi} %`]
                  : null,
                properties.koop_lekwi
                  ? ['Eigenaar bewoner', `${properties.koop_lekwi} %`]
                  : null,
                properties.huur_c_lekwi
                  ? ['Huur woningcorporatie', `${properties.huur_c_lekwi} %`]
                  : null,
                properties.huur_o_lekwi
                  ? ['Huur overig', `${properties.huur_o_lekwi} %`]
                  : null,
                properties.onb_lekwi
                  ? ['Onbekend bezit', `${properties.onb_lekwi} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer304_5',
        name: 'Onbekend bezit',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LEKWI',
            attrName: 'onb_lekwi',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { onb_lekwi } }) => ({
              title: `${onb_lekwi}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met Lage energiekwaliteit, weinig investeringsmogelijkheden`,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lekwi
                  ? ['Totaal aantal huishoudens', `${properties.tot_lekwi} %`]
                  : null,
                properties.koop_lekwi
                  ? ['Eigenaar bewoner', `${properties.koop_lekwi} %`]
                  : null,
                properties.huur_c_lekwi
                  ? ['Huur woningcorporatie', `${properties.huur_c_lekwi} %`]
                  : null,
                properties.huur_o_lekwi
                  ? ['Huur overig', `${properties.huur_o_lekwi} %`]
                  : null,
                properties.onb_lekwi
                  ? ['Onbekend bezit', `${properties.onb_lekwi} %`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
  {
    id: 'layer305',
    tab: 'armoede',
    name: 'Laag inkomen en hoge energierekening en/of lage energiekwaliteit (LIHE en/of LILEK) ',
    desc: `Deze indicator geeft weer hoeveel procent van de huishoudens een laag inkomen heeft, en daarnaast een hoge energierekening en/of een in een woning met een lage energiekwaliteit woont.`,
    sourceDescription: { CBS: metaDataList.CBSArmoede },
    lastUpdate: '2024-03',

    sublayers: [
      {
        id: 'layer305_1',
        name: 'Alle huishoudens',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE en/of LILEK',
            attrName: 'tot_lihelek2',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { tot_lihelek2 } }) => ({
              title: `${tot_lihelek2}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen en hoge energierekening en/of lage energiekwaliteit `,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihelek2
                  ? [
                      'Totaal aantal huishoudens',
                      `${properties.tot_lihelek2} %`,
                    ]
                  : null,
                properties.koop_lihelek2
                  ? ['Eigenaar bewoner', `${properties.koop_lihelek2} %`]
                  : null,
                properties.huur_c_lihelek2
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihelek2} %`]
                  : null,
                properties.huur_o_lihelek2
                  ? ['Huur overig', `${properties.huur_o_lihelek2} %`]
                  : null,
                properties.onb_lihelek2
                  ? ['Onbekend bezit', `${properties.onb_lihelek2} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer305_2',
        name: 'Eigenaar bewoner',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE en/of LILEK',
            attrName: 'koop_lihelek2',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { koop_lihelek2 } }) => ({
              title: `${koop_lihelek2}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen en hoge energierekening en/of lage energiekwaliteit `,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihelek2
                  ? [
                      'Totaal aantal huishoudens',
                      `${properties.tot_lihelek2} %`,
                    ]
                  : null,
                properties.koop_lihelek2
                  ? ['Eigenaar bewoner', `${properties.koop_lihelek2} %`]
                  : null,
                properties.huur_c_lihelek2
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihelek2} %`]
                  : null,
                properties.huur_o_lihelek2
                  ? ['Huur overig', `${properties.huur_o_lihelek2} %`]
                  : null,
                properties.onb_lihelek2
                  ? ['Onbekend bezit', `${properties.onb_lihelek2} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer305_3',
        name: 'Huur (woningcorporatie)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE en/of LILEK',
            attrName: 'huur_c_lihelek2',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_c_lihelek2 } }) => ({
              title: `${huur_c_lihelek2}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen en hoge energierekening en/of lage energiekwaliteit `,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihelek2
                  ? [
                      'Totaal aantal huishoudens',
                      `${properties.tot_lihelek2} %`,
                    ]
                  : null,
                properties.koop_lihelek2
                  ? ['Eigenaar bewoner', `${properties.koop_lihelek2} %`]
                  : null,
                properties.huur_c_lihelek2
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihelek2} %`]
                  : null,
                properties.huur_o_lihelek2
                  ? ['Huur overig', `${properties.huur_o_lihelek2} %`]
                  : null,
                properties.onb_lihelek2
                  ? ['Onbekend bezit', `${properties.onb_lihelek2} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer305_4',
        name: 'Huur (overige verhuurders)',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE en/of LILEK',
            attrName: 'huur_o_lihelek2',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { huur_o_lihelek2 } }) => ({
              title: `${huur_o_lihelek2}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen en hoge energierekening en/of lage energiekwaliteit `,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihelek2
                  ? [
                      'Totaal aantal huishoudens',
                      `${properties.tot_lihelek2} %`,
                    ]
                  : null,
                properties.koop_lihelek2
                  ? ['Eigenaar bewoner', `${properties.koop_lihelek2} %`]
                  : null,
                properties.huur_c_lihelek2
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihelek2} %`]
                  : null,
                properties.huur_o_lihelek2
                  ? ['Huur overig', `${properties.huur_o_lihelek2} %`]
                  : null,
                properties.onb_lihelek2
                  ? ['Onbekend bezit', `${properties.onb_lihelek2} %`]
                  : null,
              ],
            }),
          },
        ],
      },
      {
        id: 'layer305_5',
        name: 'Onbekend bezit',
        styleLayers: [
          {
            scale: 'Gemeente, wijk, buurt',
            unit: '% LIHE en/of LILEK',
            attrName: 'onb_lihelek2',
            tileSource: sourceSettings.cbsarmoede,
            legendStops: [0, 3, 6, 10, 12, 20, 30],
            lastLabel: '30 <',
            expressionType: 'interpolate',
            opacity: 0.9,
            colorScale: createColorScale({
              type: 'sequentional',
              color: 'red',
              domain: [0, 28],
            }),
            popup: ({ properties: { onb_lihelek2 } }) => ({
              title: `${onb_lihelek2}`,
            }),
            detailModal: ({ properties }) => ({
              type: 'armoede',
              title: `Percentage huishoudens met laag inkomen en hoge energierekening en/of lage energiekwaliteit `,
              desc: `${properties.name} ${properties.code}`,
              content: [
                properties.tot_lihelek2
                  ? [
                      'Totaal aantal huishoudens',
                      `${properties.tot_lihelek2} %`,
                    ]
                  : null,
                properties.koop_lihelek2
                  ? ['Eigenaar bewoner', `${properties.koop_lihelek2} %`]
                  : null,
                properties.huur_c_lihelek2
                  ? ['Huur woningcorporatie', `${properties.huur_c_lihelek2} %`]
                  : null,
                properties.huur_o_lihelek2
                  ? ['Huur overig', `${properties.huur_o_lihelek2} %`]
                  : null,
                properties.onb_lihelek2
                  ? ['Onbekend bezit', `${properties.onb_lihelek2} %`]
                  : null,
              ],
            }),
          },
        ],
      },
    ],
  },
]
