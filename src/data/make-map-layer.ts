// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  BackgroundLayerSpecification,
  ExpressionSpecification,
  FilterSpecification,
  LayerSpecification,
} from 'maplibre-gl'
import {
  makeCircleLayer,
  makeFillExtrusionLayer,
  makeFillLayer,
  makeLineLayer,
  makeRasterLayer,
  makeSymbolLayer,
} from '../utils/make-map-layer/get-type-layer'
import { CustomFilter } from '../providers/map-provider'
import { StyleLayer } from './data-layers'

export type CustomLayerSpecification = Exclude<
  LayerSpecification,
  BackgroundLayerSpecification
> & {
  insert: string
  metadata?: unknown
}

export const makeMapLayer = (
  styleLayer: Partial<StyleLayer>,
  threeDimensional: boolean,
  globalFilter: CustomFilter,
  name?: string
): Partial<CustomLayerSpecification> => {
  let layerProps: Partial<CustomLayerSpecification>
  switch (styleLayer.geomType) {
    case 'line':
      layerProps = makeLineLayer(styleLayer)
      break
    case 'raster':
      layerProps = makeRasterLayer(styleLayer)
      break
    case 'circle':
      layerProps = makeCircleLayer(styleLayer, styleLayer.offset || [0, 0])
      break
    case 'symbol':
      layerProps = makeSymbolLayer(name)
      break
    default:
      // geomType == 'fill' (default)
      layerProps =
        threeDimensional && styleLayer.extrusionAttr
          ? makeFillExtrusionLayer(styleLayer)
          : makeFillLayer(styleLayer)
      break
  }

  const newMapLayer = {
    id: styleLayer.id,
    source: styleLayer.tileSource.sourceTitle,
    'source-layer': styleLayer.tileSource['source-layer'] || '',
    minzoom: 0,
    maxzoom: 22,
    ...layerProps,
  }
  newMapLayer.minzoom = styleLayer.tileSource.minzoom
    ? styleLayer.tileSource.minzoom
    : 0
  newMapLayer.maxzoom = styleLayer.tileSource.maxzoom
    ? styleLayer.tileSource.maxzoom
    : 22

  if (styleLayer.geomType !== 'raster') {
    newMapLayer.filter = makeFilter(globalFilter, styleLayer)
  }

  if (styleLayer['source-layer']) {
    newMapLayer['source-layer'] = styleLayer['source-layer']
  }

  if (styleLayer.maxzoom) {
    newMapLayer.maxzoom = styleLayer.maxzoom
  }

  if (styleLayer.customPaint) {
    newMapLayer.paint = styleLayer.customPaint
    newMapLayer.filter = ['all']
  }
  return newMapLayer
}

export const makeInvertLayer = (
  baseLayer: Partial<StyleLayer>
): Partial<LayerSpecification> => {
  let mapLayer = {} as Partial<LayerSpecification>
  const tempMapLayer = {
    id: baseLayer.id,
    minzoom: baseLayer.tileSource.minzoom,
    maxzoom: baseLayer.tileSource.maxzoom,
    source: baseLayer.tileSource.sourceTitle,
    'source-layer': baseLayer.tileSource['source-layer'],
  }

  if (!baseLayer.geomType || baseLayer.geomType === 'fill') {
    mapLayer = { ...tempMapLayer, ...makeFillLayer(baseLayer, true) }
  }

  if (mapLayer.type === 'background') {
    return mapLayer
  }

  mapLayer = {
    ...mapLayer,
    filter: makeInvertFilter(baseLayer),
  }
  return mapLayer
}

const makeFilter = (
  globalFilter: CustomFilter,
  styleLayer: Partial<StyleLayer>
): FilterSpecification => {
  const ignoreGlobalFilter = styleLayer.ignoreGlobalFilter

  // This a filter set by the MenuPanel or the Legend, specific for this layer.
  if (globalFilter && ignoreGlobalFilter !== true) {
    return globalFilter.filter
  }

  const attr = styleLayer.attrName
  const customFilter = styleLayer.customFilter
  let filter: any | FilterSpecification

  if (styleLayer.expressionType === 'match') {
    const allowedValues = styleLayer.colorScale
      .domain()
      .map((value: any) => ['==', ['get', attr], value])
    filter = ['all', ['has', attr], ['any', ...allowedValues]]
  } else {
    const noDataValues = [] // 'NULL', 'null', 'undefined', 'NA', 'NODATA', 'NaN'
    const forbiddenValues = noDataValues
      .concat(styleLayer.noDataValue || [])
      .map((value) => ['!=', ['get', attr], value])
    filter = ['all', ['has', attr], ...forbiddenValues]
  }
  if (customFilter) {
    filter.push(customFilter)
  }

  return filter
}

const makeInvertFilter = (
  styleLayer: Partial<StyleLayer>
): FilterSpecification => {
  const attr = styleLayer.attrName

  if (styleLayer.noDataValue) {
    const noDataValues: StyleLayer['noDataValue'] = ['NA'] // 'NULL', 'null', 'undefined', 'NA', 'NODATA', 'NaN'
    const allowedValues: ExpressionSpecification[] = noDataValues
      .concat(styleLayer.noDataValue || [])
      .map((value) => ['==', ['get', attr], value])
    return ['all', ['has', attr], ['any', ...allowedValues]]
  } else {
    return ['!has', attr]
  }
}
