// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import React, { FC, createContext, useState } from 'react'
import { Toasters } from '../components/toasters/Toasters'

export interface ToasterProps {
  body?: string
  nth?: number
  title: string
  variant: 'success' | 'error'
  visibleTime?: number
}

export interface ToasterContextProps {
  showToast: (toaster: ToasterProps) => void
}

const ToasterContext = createContext<ToasterContextProps>(
  {} as ToasterContextProps
)

const ToasterProvider = ({ children }) => {
  const [toasters, setToasters] = useState([])

  const showToast = (toaster: ToasterProps) => {
    toaster.nth = toasters.reduce(
      (acc, toast) => (toast.nth >= acc ? toast.nth + 1 : acc),
      0
    )

    setToasters([...toasters, toaster])
  }

  return (
    <ToasterContext.Provider
      value={{
        showToast,
      }}
    >
      {children}
      <Toasters toasters={toasters} setToasters={setToasters} />
    </ToasterContext.Provider>
  )
}

export { ToasterContext, ToasterProvider }
