// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { Feature } from 'maplibre-gl'
import { Dispatch, SetStateAction, FC, createContext, useState } from 'react'

export interface DrawContextProps {
  drawMode: 'initPoint' | 'initPolygons' | 'idle' | null
  setDrawMode: Dispatch<SetStateAction<DrawContextProps['drawMode']>>
  featureCollection: Record<string, never>
  setFeatureCollection?: Dispatch<
    SetStateAction<DrawContextProps['featureCollection']>
  >
  startDrawing: boolean | 'polygons'
  setStartDrawing: Dispatch<SetStateAction<DrawContextProps['startDrawing']>>
  deleteSelectedFeature: boolean
  setDeleteSelectedFeature: Dispatch<
    SetStateAction<DrawContextProps['deleteSelectedFeature']>
  >
  selectedPolygonForCount: Feature
  setSelectedPolygonForCount: Dispatch<
    SetStateAction<DrawContextProps['selectedPolygonForCount']>
  >
  customCountFilter: string
  setCustomCountFilter: Dispatch<
    SetStateAction<DrawContextProps['customCountFilter']>
  >
}

const DrawContext = createContext<DrawContextProps>({} as DrawContextProps)

const DrawProvider = ({ children }) => {
  const [drawMode, setDrawMode] = useState(null)
  const [startDrawing, setStartDrawing] = useState(false)
  const [selectedPolygonForCount, setSelectedPolygonForCount] = useState(null)
  const [deleteSelectedFeature, setDeleteSelectedFeature] = useState(false)
  const [featureCollection, setFeatureCollection] = useState()
  const [customCountFilter, setCustomCountFilter] = useState('')

  return (
    <DrawContext.Provider
      value={{
        customCountFilter,
        setCustomCountFilter,
        drawMode,
        setDrawMode,
        startDrawing,
        setStartDrawing,
        deleteSelectedFeature,
        setDeleteSelectedFeature,
        selectedPolygonForCount,
        setSelectedPolygonForCount,
        featureCollection,
        setFeatureCollection,
      }}
    >
      {children}
    </DrawContext.Provider>
  )
}

export { DrawContext, DrawProvider }
