// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import { useRouter } from 'next/router'
import {
  createContext,
  Dispatch,
  FC,
  SetStateAction,
  useEffect,
  useState,
} from 'react'
import { ReactElement } from 'react-markdown/lib/react-markdown'
import { setURLParams } from '../utils/url-params'

export interface LabelContextProps {
  labelState: string
  toggleLabelState: Dispatch<SetStateAction<LabelContextProps['labelState']>>
  borderState: boolean
  toggleBorderState: Dispatch<SetStateAction<LabelContextProps['borderState']>>
  setBorderState: Dispatch<SetStateAction<boolean>>
  aerialPhotosState: boolean
  setAerialPhotosState: Dispatch<SetStateAction<boolean>>
}

export const LabelContext = createContext<LabelContextProps>(
  {} as LabelContextProps
)

type Props = {
  children: ReactElement
}

export const LabelProvider: FC<Props> = ({ children }) => {
  const router = useRouter()
  const { query } = router || {}
  const labelParam: string = query?.label as string
  const [aerialPhotosState, setAerialPhotosState] = useState(false)

  const [labelState, setLabelState] = useState('topo')
  const [borderState, setBorderState] = useState(labelParam === 'admin')

  const toggleBorderState = () => {
    setBorderState(!borderState)
  }

  const toggleLabelState = (value: string) => {
    setURLParams('label', value)
    setLabelState(value)
  }

  useEffect(() => {
    if (labelParam) {
      setBorderState(labelParam === 'admin')
      toggleLabelState(labelParam)
    } else {
      toggleLabelState('topo')
    }
  }, [labelParam])

  return (
    <LabelContext.Provider
      value={{
        labelState,
        borderState,
        aerialPhotosState,
        toggleBorderState,
        toggleLabelState,
        setBorderState,
        setAerialPhotosState,
      }}
    >
      {children}
    </LabelContext.Provider>
  )
}
