// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { useEffect, useState } from 'react'

import breakpoints from '@commonground/design-system/dist/themes/parts/breakpoints'

export const useIsMobile = (): { isMobile: boolean } => {
  const [width, setWidth] = useState(0)

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    setWidth(window.innerWidth)

    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])
  // TODO werkt niet
  // console.log(breakpoints)

  return { isMobile: width <= 900 }
}
