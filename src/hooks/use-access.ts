// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useCallback } from 'react'

import authModels from '../../authorization-model.json'
import { AuthRole, DataLayerProps } from '../data/data-layers'
import { UserSession } from './use-user'

export const useAccess = () => {
  const hasAccess = useCallback(
    (datalayer: Partial<DataLayerProps>, userSession: UserSession): boolean => {
      if (!datalayer) {
        return false
      }

      const userRole: AuthRole | null =
        userSession?.permissions.find(
          (permission) =>
            permission.namespace === authModels.subdivisions.namespace &&
            permission.relation === authModels.subdivisions.relations.access
        )?.roleId || null

      if (
        !datalayer.authRoles ||
        (userRole === null && datalayer.authRoles.includes('no_subdivision'))
      ) {
        return true
      }

      return datalayer.authRoles?.includes(userRole)
    },
    []
  )

  return { hasAccess }
}
