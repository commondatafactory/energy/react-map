// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { useContext } from 'react'
import { MapContext, MapContextProps } from '../providers/map-provider'

export const useMap = (): MapContextProps => {
  return useContext(MapContext)
}
