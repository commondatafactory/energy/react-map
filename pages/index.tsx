// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { GetStaticProps } from 'next'
import { FC } from 'react'
import Head from 'next/head'
import { ThemeProvider } from 'styled-components'
import { DrawProvider } from '../src/providers/draw-provider'
import {
  DataLayerProps,
  degoDataLayers,
  dookDataLayers,
  demoDataLayers,
} from '../src/data/data-layers'
import { GlobalStyle } from '../src/styling/global-styles'
import { MapProvider } from '../src/providers/map-provider'
import { TabLayerProvider } from '../src/providers/tab-layer-provider'
import { TabProps } from '../src/providers/tab-layer-provider'
import { theme } from '../src/theme'
import { ToasterProvider } from '../src/providers/toaster-provider'
import App from '../src/components/App'
import AppContext from '../src/components/AppContext'

export type IndexProps = {
  configuration: {
    product: 'DEGO' | 'DOOK'
    about: string
    head: {
      description: string
      title: string
    }
    title: string
    subtitle: string
    tabs: TabProps[]
    auth?: {
      loginUrl: string
      userEndpoint: string
    }
  }
}

const getAppConfiguration = async () => {
  switch (String(process.env.NEXT_PUBLIC_CONFIG).toLowerCase()) {
    case 'dego':
      return {
        configuration: (await import('../app.dego.json')).default,
      }
    case 'dook':
      return {
        configuration: (await import('../app.dook.json')).default,
      }
    default: {
      // This is demo or e2e, just build it all in...
      const dook = (await import('../app.dook.json')).default
      const dego = (await import('../app.dego.json')).default
      const demo = (await import('../app.json')).default

      const aggregated = {
        ...dook,
        ...dego,
        ...demo,
        tabs: [...dook.tabs, ...dego.tabs, ...demo.tabs].filter(
          (tab, i, tabs) => {
            return i === tabs.findIndex((t) => t.id === tab.id)
          }
        ),
      }

      return { configuration: aggregated }
    }
  }
}

function getDataLayersForProduct(product: string): Partial<DataLayerProps>[] {
  switch (String(product).toLowerCase()) {
    case 'dego':
      return degoDataLayers
    case 'dook':
      return dookDataLayers
    default:
      // This is demo, or e2e. Just build it all in...
      return [...dookDataLayers, ...degoDataLayers, ...demoDataLayers].filter(
        (layer, i, layers) => i === layers.findIndex((o) => o.id === layer.id)
      )
  }
}

export const getStaticProps: GetStaticProps = async () => {
  const { configuration } = await getAppConfiguration()
  return { props: { configuration } }
}

const Index: FC<IndexProps> = ({ configuration }) => {
  const dataLayers = getDataLayersForProduct(configuration.product)

  return (
    <>
      <Head>
        <title>{configuration.head.title}</title>
        <meta name="description" content={configuration.head.description} />
      </Head>

      <AppContext.Provider value={configuration}>
        <TabLayerProvider
          tabs={configuration.tabs}
          availableDataLayers={dataLayers}
        >
          <DrawProvider>
            <ThemeProvider theme={theme}>
              <MapProvider>
                <ToasterProvider>
                  <GlobalStyle />
                  <App />
                </ToasterProvider>
              </MapProvider>
            </ThemeProvider>
          </DrawProvider>
        </TabLayerProvider>
      </AppContext.Provider>
    </>
  )
}

export default Index
