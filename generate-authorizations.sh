#!/bin/bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

url="https://gitlab.com/commondatafactory/rbac-models/-/raw/main/authorization-model-dev.json"
output_file="authorization-model.json"

curl -o "$output_file" "$url"
