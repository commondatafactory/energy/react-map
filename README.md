# Vector Tile Map viewer / analyse tool.

A vector tile map viewer written in React-js, our go-to-tool to create visuals of mapping data which is configurable for multiple topics. Currently running 2 instances:

1. [DEGO Data Energie Gebouwde Omgeving](https://dego.vng.nl)
2. [DOOK Data Onregelmatigheden Op Kaart](https://dook.vng.nl)

More about this project and documentation about the data used on the [Documentation website](https://dook.commondatafactory.nl)

### About DEGO

DEGO is a build for all municipalities in the Netherlands to facilitate in the specific information needs around energy and buildings. Build on behalf of the dutch association of municipalities.[VNG Realisatie](https://vng.nl).

All municipalities are required to make plans and facilitate the transition away from
fossil gas. The Viewer is build on top of carefully collected energy and energy related datasets. To help the people of the municipalities set up their
policy, and make decisions of which neighbourhoods are best to start with.
The viewer is visualizing the most relevant data in an easy and most comprehensive manner.

This way, everyone can use the tool the way they like best or the way they need it most, for example to make decisions, or just to convey their ideas and make it feasible to talk about the upcoming project.

It is simple to experiment with different data on buildings and neighbourhoods by clicking the buttons, and see which neighbourhoods need more attention than others (i.e. which seem more difficult).

### Warning about data quality

We take upmost care in data quality, but our source data does not alway reflect reality. There is no guarantee on completeness, actuality and factuality of the displayed data. We can not show things other than the data that is provided to us. This also means the data shared throughout this project cannot be used for legal grounds, merely as a point of departure.


# For Developers
To run this application in a local development environment for further development or adaptations, you can follow these steps to get the application up and running. 

### Help us further

As with most things, every bit of help is welcome. Therefore, we ask you to give feedback on this documentation and the application!

In case something goes wrong, or doesn't behave as expected, but also when you need/want something that the tool does not provide yet, give us a heads up. Also, for any questions, you can reach out to us:

vip@vng.nl

Let's get started with setting things up:

## Installing the JavaScript runners and application

To run the application we need to install some dependencies. It is running on `node` and `yarn`:
 
 - `node` and `npm`
 - `yarn`

We assume you have some programmings skills and know how to execute the following things from the **command line/terminal**. Follow the steps below:

### 1. Install Node and npm 

You can install these through the Node Version manager `nvm`. See https://github.com/nvm-sh/nvm 

Extra help : https://medium.com/@imvinojanv/how-to-install-node-js-and-npm-using-node-version-manager-nvm-143165b16ce1 

Or install node from the official website and download the LTS-version. https://nodejs.org/.

Follow the instructions for you operating system.

Check the installation through:

    node --version

### 2. Install yarn

When `Node` is installed we can install `yarn`. 

Go to : https://classic.yarnpkg.com/en/docs/install#windows-stable 

Run this in the terminal:

    npm install --global yarn

More info :  https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable 

Check the installation with:

    yarn --version

### 3. Install git

Got to https://git-scm.com/downloads and follow the instructions for your operating system. 

### 4. Install the application 
Now we have everything setup to run this application. 

Clone this repository to your computer. In the terminal:

    git clone https://gitlab.com/commondatafactory/react-map.git

Go into the folder:

    cd react-map 

Run `yarn` to install development environment and all the dependencies needed for the application.

    yarn 

It takes a little while and if all goes well the process ends without errors and displays `success` in green. Orange `warning`s only indicate some missing updates/dependencies but do not obstruct the installing process for now. 

### 5. Run the application 

Now there are 3 options. To run the DOOK, DEGO or Development environment, choose one of these 3 to start the local server:

    yarn dev:dego
    yarn dev:dook
    yarn dev 

The line `ready - started server on 0.0.0.0:3001, url: http://localhost:3001` indicates the server is up and running and the application is available on http://localhost:3001 

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Not working? 

Some tips: 

- When it is not working check either the terminal, or the console/network tabs in the browser for any errors. 

- Try re-installing the application. Remove the `/node_modules` folder and the `yarn.lock` file. Then run `yarn` again for installation. 

- You can also try: [http://127.0.0.1:3001](http://127.0.0.1:3001) instead of localhost. 

## Start developing! 

We have 3 development environments that are automatically published when committing to them:

- acceptation - `develop` branch
- demo - `demo` branch
- production - `main` branch

The acceptation environments are used for testing and debugging. They can be seen at :
- [acc dook](https://acc.dook.commondatafactory.nl)
- [acc dego](https://acc.tvw.commondatafactory.nl)

The demo environment is a combination of all available layers. This should stay a working place which we can use to show features and data that are not necessarily part of the projects dego and dook. For example the AI examples. 

- [demo all](https://demo.commondatafactory.nl/?label=topo#16.5/52.087049/4.308188)

### Download a code editor

To develop code. for example [Visual studio code](https://code.visualstudio.com/). 

The page at `localhost:3001` will reload if you make edits.<br /> 
You will also see any lint errors in the console.

For checking and fixing linting and type script errors the following scripts are available. Execute these in the terminal:

    yarn lint --fix
    yarn type-check
    yarn test
    yarn test-all //this includes all of the above

The output tells you which lines should be corrected.

### Committing and publishing

When changes are made and you want to commit to our gitlab repository ask a maintainer for permission. 

Committing to gitlab:

    git checkout -b feature/new-feature-branch
    git add .
    git commit -m "development of a new feature"

A merge request is made in the browser and needs to be reviewed by a peer developer. 
Never commit to `develop` or `main` directly

### Git naming conventions

We make use of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) with one exception: we write `feature` instead of `feat`.

This means not starting the description with a capital letter and not ending with a period.
The description will be in simple present tense (`add button` instead of `adding button` or `added button`).

The _types_ are being used in our branch names as well, which are structured in the following maner:

```
<type>/<issue number>-<short description>
```

All lower case, dashes for separating issue number and words

**Examples**

- `feature/123-something-new`
- `fix/456-the-problem`
- `chore/789-update-dependencies`


### Contributing to this code

In general we make use of the Standard for Public Code ([https://standard.publiccode.net/](https://standard.publiccode.net/)), and follow the principles of Common Ground ([https://commonground.nl](https://commonground.nl)).


### Guidelines when working with the application

The [README_DEVELOPER.md](/README_DEVELOPER.md) helps you get a base understanding of how the application is structured and how data is moved from A to B within the application. Please continue there to learn how to update or add data in the viewer. 
